<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('PageController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override(function() {
	return view('pages/404');
});
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'PageController::index');
$routes->get('/home', 'PageController::home');
$routes->get('/historia', 'PageController::historia');
$routes->get('/portafolio', 'PageController::portafolio');
$routes->get('/noticias', 'PageController::noticias');
$routes->get('/responsabilidad', 'PageController::responsabilidad');
$routes->get('/contacto', 'PageController::contacto');
$routes->get('/trabajo', 'PageController::trabajo');
$routes->get('/leasing', 'PageController::leasing');
$routes->get('/proyecto/(:segment)', 'PageController::proyecto/$1');
$routes->get('/denuncias', 'PageController::denuncias');

$routes->post('/search', 'PageController::search');
$routes->post('/cvEmail', 'PageController::cvEmail');
$routes->post('/sendEmail', 'PageController::sendEmail');
$routes->post('/sendDenouncement', 'PageController::sendDenouncement');

$routes->get('/switchLang/(:segment)', 'LanguageSwitchController::switchLang/$1');
$routes->get('/switchLangSite/(:segment)', 'LanguageSwitchController::switchLangSite/$1');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
