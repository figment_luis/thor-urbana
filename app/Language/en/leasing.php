<?php

$lang['leasing_description'] = 'Our properties are home to some of the best retailers and restaurants globally. We are committed to offer the best merchandising mix in all our properties. Our best-in-class leasing team works to continually secure cutting-edge brands that are both unique and highly coveted. If you are interested in your brand being part of any of our premium developments, please provide your information in the following form and we will get in touch.';
$lang['leasing_address_phone'] = 'Telephone';
$lang['leasing_form_name'] = 'NAME';
$lang['leasing_form_email'] = 'EMAIL';
$lang['leasing_form_project'] = 'PROJECT OF INTEREST';
$lang['leasing_form_message'] = 'MESSAGE';

$lang['leasing_form_button'] = 'SEND';


$lang['leasing_centros'] = 'Lifestyle Retail Centers';
$lang['leasing_oficinas'] = 'Office';
$lang['leasing_proyectos'] = 'New Developments';


$lang['partner_title'] = 'BRAND PARTNERSHIPS';
$lang['partner_description_p1'] = 'The brands we partner with are some of the best brands in fashion, lifestyle, F&B world-wide. They are recognized national and internationally due to their personalized service and unsurpassed attention as the guests visiting each of our properties.';
$lang['partner_description_p2'] = 'The strategic and unique merchandising mix in all our projects distinguishes us from our competition. It offers our guests a unique buying experience, which turns into the high loyalty of our clients. While also creating a dynamic channel for all our partners.';


$lang['partner_category_moda'] = 'Fashion';
$lang['partner_category_gastronomia'] = 'Restaurants';
$lang['partner_category_entretenimiento'] = 'Entertainment';
$lang['partner_category_estilo-de-vida'] = 'Life Style';

$lang['partner_show_all'] = 'View all';

$lang['partner_message'] = 'Among many others';

return $lang;