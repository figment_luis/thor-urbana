<?php

// QUIENES SOMOS
$lang['about_title'] = 'ABOUT US';
$lang['about_description_p1'] = 'We are a leading real estate investment and development company characterized by the innovative vision that embodies each of our projects. We specialize in sourcing, acquiring, developing, repositioning, leasing, and managing uniquely located real estate projects that include lifestyle centers, luxury hotels and resorts, industrial parks and mixed-used asset properties situated in Mexico´s main urban markets and top tourist destinations, as well as in key locations in Latin America.';
$lang['about_description_p2'] = 'At Thor Urbana, we seek to build spaces with avant-garde designs, capable of generating unique experiences, in order to improve the quality of life in each of the communities that we belong to. The experience of our company has been decisive in the visualization and conceptualization of the highest quality projects in key locations. With this, we seek to promote and develop economic growth in each of the cities where we are present.';

// MISION Y VISION
$lang['mision_title'] = 'MISSION & VISION';
$lang['mision_description_p1'] = 'Our Mission is to develop, lease, operate and acquire innovative real estate assets through a vertically integrated platform. We offer innovative concepts throughout our projects to add significant value for all of our partners.';
$lang['mision_description_p2'] = 'We work every day with a unique vision in mind; to position Thor Urbana as a cutting-edge company recognized for its execution, creating positive social and economic legacies and improving the quality of life for our communities.';

// NUESTRA HISTORIA
$lang['history_title'] = 'OUR HISTORY';
$lang['history_2012'] = '<li>Thor Urbana is founded.</li>
<li>A strategic alliance is created with Thor Equities in New York.</li>
<li>First investment vehicle raised through a Canadian pension fund.</li>
<li>Acquisition to  reposition Calle Corazón (Hotel & Lifestyle Center in Playa del Carmen).</li>
<li>Land acquisition for The Shops at 5ta & 8 (Lifestyle Center in Playa del Carmen) and Thompson Beach House (Hotel & Beach Club in Playa del Carmen).</li>';
$lang['history_2013'] = '<li>Development initiated for Calle Corazón and Thompson Beach House.</li>
<li>Land Purchased for The Landmark Guadalajara.</li>';
$lang['history_2014'] = '<li>Acquisition of Altavista 147 and Masaryk 120 lifestyle centers in Mexico City.</li>
<li>Acquisition of Lomas Verdes (Mixed-use project in the State of Mexico), Town Square Metepec (Lifestyle Center in Metepec, State of Mexico) and The Ritz-Carlton Hotel (Mexico City).</li>
<li>Thor Urbana manages to place $ 500 M USD of capital.</li>';
$lang['history_2015'] = '<li>Second investment vehicle raised with a Canadian pension fund.</li>
<li>Operations commence for Calle Corazón and The Shops at 5ta & 8 (Playa del Carmen).</li>
<li>Acquisition of land for Montage Hotel & Residences (Los Cabos), Caye Chapel (Hotel and residences and residential lots in Belize), The Harbor (Lifestyle Center in Merida).</li>
<li>Thor Urbana reaches $1,billion USD in AUM.</li>';
$lang['history_2016'] = '<li>Thompson Beach House and Masaryk 120 openings.</li>
<li>Acquisition of land for The Park (Mixed-use project in San Luis Potosí).</li>
<li>Development commences for Montage Hotel & Residences.</li>
<li>Thor Urbana reaches $1.5  billion USD in AUM.</li>';
$lang['history_2017'] = '<li>Acquisition of The Landmark Tijuana (Mixed-use project in Tijuana, Baja California).</li>
<li>Thor Urbana reaches +1 million sqm of total area developed.</li>';
$lang['history_2018'] = '<li>Third investment vehicle (CKD) raised via the Mexican Stock Exchange with Afores.</li>
<li>Acquisition of Marina Puerto Cancún (Lifestyle Center in Cancún).</li>
<li>Montage Hotel & Residences, The Harbor Mérida, The Landmark Guadalajara, and Town Square Metepec openings.</li>
<li>Thor Urbana reaches $ 2 billion USD in AUM.</li>';
$lang['history_2019'] = '<li>Acquisition of land for Ritz-Carlton Reserve Costa Canuva (Hotel & Residences in Nayarit) and Casa Magna (Hotel & Residences in Tulum).</li>
<li>Funding round closed for Caye Chapel.</li>';
$lang['history_2020'] = '<li>Thor Urbana launches Thor2GO, a distribution platform created to help all its tenants ship merchandise directly to clients.</li>
<li>Thor Urbana submits winning bid in competition for Aztlán (Entertainment, Urban Park in Mexico City).</li>
<li>Land acquisition in Tulum (Hotel & Beach Club).</li>';
$lang['history_2021'] = '<li>Fourth investment vehicle (CERPI) raised via the institutional stock exchange (BIVA).</li>';

// ESTRATEGIA
$lang['strategy_title'] = 'INVESTMENT STRATEGY';
$lang['strategy_description_p1'] = 'Thor Urbana provides its investors with superior risk-adjusted returns by developing and acquiring lifestyle centers, luxury hotels and resorts, and industrial parks as well as mixed-use properties located in Mexico and Latin America.';
$lang['strategy_description_p2'] = 'How we execute our strategy:';
$lang['strategy_item_1'] = 'Focus on urban markets with high population density and premier tourist destinations.';
$lang['strategy_item_2'] = 'Seek properties well situated for commercial repositioning and rethinking.';
$lang['strategy_item_3'] = 'Capitalize on development & redevelopment opportunities.';
$lang['strategy_item_4'] = 'Implement creative marketing initiatives for each property.';
$lang['strategy_item_5'] = 'Actively and diligently manage assets to maximize cash flow.';
$lang['strategy_item_6'] = 'Utilizes prudent levels of leverage.';

// SOCIOS ESTRATÉGICOS
$lang['partners_title'] = 'STRATEGIC PARTNERS';
$lang['partners_description_p1'] = 'Part of the strength of Thor Urbana is the partnership of two internationally recognized real estate companies: Thor Equities, a leading global real estate development, management, and investment company; and GFA, a leading company in the real estate and development industry in Mexico.';
$lang['partners_description_p2'] = 'The depth and reach of the company´s strategic partners uniquely position Thor Urbana as a leading competitor in Mexico’s growing commercial real estate arena.';
$lang['partners_item_1'] = '<b>Thor Equities:</b> is a leader in the development, leasing and management of commercial, residential, retail, hotel and mixed-use assets in premier urban locations worldwide. The company maximizes returns for investors by recognizing a property’s potential, reducing operating expenses, increasing tenant satisfaction, and leveraging market trends to maintain a long-term competitive edge.<br> <br>
Thor Equities is also the exclusive representative of global retailers through Thor Retail Advisors, a premier leasing agent for marquee properties worldwide.';
$lang['partners_item_2'] = '<b>GFA:</b> is one of the most recognized developers and operators of class A real estate in Mexico. It offers all of its services in a comprehensive way to generate superior results with its real estate developments. The company encompasses real estate promotion and development, financial planning, architectural design, construction oversight, leasing, and asset management.<br><br>
GFA has more than 50 years of experience, during which it has overseen a portfolio of residential, corporate, and hospitality spaces that are both cutting edge and that stand the test of time.';

// NUESTROS PRINCIPIOS
$lang['principals_title'] = 'OPERATING PRINCIPALS';
$lang['principals_item_1'] = 'Create welcoming places people love.';
$lang['principals_item_2'] = 'Respect and reflect each community we join.';
$lang['principals_item_3'] = 'Find a win-win for everyone involved.';
$lang['principals_item_4'] = 'Take joy in delighting people.';
$lang['principals_item_5'] = 'Treat time as a luxury.';
$lang['principals_item_6'] = 'Every detail is important.';
$lang['principals_item_7'] = 'Invest in the future and never stop evolving.';
$lang['principals_item_8'] = 'Make it happen today. Follow up relentlessly.';
$lang['principals_item_9'] = 'Have integrity in every action and every word.';






$lang['menu_item_somos'] = 'ABOUT US';
$lang['menu_item_mision'] = 'MISSION & VISION';
$lang['menu_item_historia'] = 'HISTORY';
$lang['menu_item_estretegia'] = 'STRATEGY';
$lang['menu_item_socios_estrategicos'] = 'STRATEGIC PARTNERS';

$lang['menu_item_portafolio'] = 'PORTAFOLIO';
$lang['menu_item_centros'] = 'LIFESTYLE RETAIL CENTERS';
$lang['menu_item_hoteles'] = 'HOTELS';
$lang['menu_item_oficinas'] = 'OFFICE';
$lang['menu_item_residencial'] = 'RESIDENTIAL';
$lang['menu_item_desarrollo'] = 'NEW DEVELOPMENTS';

$lang['menu_item_leasing'] = 'LEASING';
$lang['menu_item_prensa'] = 'PRENSA';
$lang['menu_item_socios_comericales'] = 'BRAND PARTNERSHIPS';
$lang['menu_item_responsabilidad'] = 'SOCIAL IMPACT';
$lang['menu_item_bolsa'] = 'CAREERS';
$lang['menu_item_contacto'] = 'CONTACT';


return $lang;