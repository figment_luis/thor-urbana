<?php
/*********************************************
 * 
 *  CENTROS COMERCIALES 
 * 
 * *******************************************/
 
// TOWNSQUARE
$lang['townsquare_title'] = 'TOWN SQUARE, METEPEC';
$lang['townsquare_description'] = 'Town Square Metepec is an open-air Lifestyle Retail Center that offers its visitors a great variety of options to shop, dine and enjoy their time with friends and family. Visit our beautiful park and delight your senses with our incredible water-fountain show.  Savor the diverse options of elegant and informal restaurants we have waiting for you. As well as our fashion boutiques, fine jewelry, movies, and more.
<br><br>
This Lifestyle Retail Center its known for its innovative concept and wide variety of commercial offering with brands such as, Zara, Pull & Bear, Oysho, Bershka, Massimo Dutti, Samsung, Desigual, Forever 21, Old Navy, Bimba y Lola, Pandora, Swarovsky, Lacoste, Kipling, MAC Cosmetics, Tous among many others. It has a dynamic culinary offering with restaurants such as Mochomos, Fogo de Chao, Porco Rosso, Cielito Querido Café, Moshi Moshi, Mercado Norte, Cervecería de Barrio, Cassava Roots among many others.
<br><br>
Town Square has positioned itself as one of the most emblematic and innovative Lifestyle Retail Centers in the area, with its variety of retail offering, and unmatched entertainment the project offers an unequaled experience to all its visitors.';
$lang['townsquare_info'] = 'Lifestyle Retail Center | +65,000 m2 of leasable space | +300 linear meters of frontage on Av. Ingacio Comonfort | +160 retail locations | +2,000 parking spaces | Opened: November 2018';

// THE LANDMARK
$lang['the-landmark-gdl_title'] = 'THE LANDMARK, GUADALAJARA';
$lang['the-landmark-gdl_description'] = 'The Landmark Guadalajara is an open-air Lifestyle Retail Center that offers all its visitors the best combination of high-fashion, cuisine, and entertainment in the area. Full of style and energy, it is the meeting place where friends and families spend entire days creating memories.
<br><br>
It brings together exclusive fashion and lifestyle, as well as a select variety of restaurants and entertainment. The Landmark is made up of recognized brands such as Salvatore Ferragamo, Hugo Boss, Longchamp, Miele, Salvador Vergara, Max Mara, Pottery Barn, West Elm, Design within Reach among many others. In addition, it offers restaurants including Mexia, Santo Mar, L’ Osteria de il Duomo, Mochomos, Enrique Tomas, La Tequila, and La No. 20 Cantina among many others.
<br><br>
The Landmark is a unique mixed-use complex, featuring a luxury residential tower and an AAA office tower. We are characterized by having a world-class architectural design providing unforgettable experiences to all our visitors.';
$lang['the-landmark-gdl_info'] = 'Luxury-Lifestyle Retail Center | +25,000 M2 GLA of leasable space | +250 M frontage on Puerta de Hierro and Av. Patria | +65 retail locations | +700 parking spaces | Opened: November 2018';

// THE HARBOR MÉRIDA
$lang['the-harbor_title'] = 'THE HARBOR, MÉRIDA';
$lang['the-harbor_description'] = 'The Harbor Mérida is an open-air Lifestyle Retail Center that offers unique spaces with different entertainment concepts, as well as the best culinary offerings in the area. It is characterized by an iconic lake with a magical show of dancing water-fountains.
<br><br>
The restaurants include Porfirio’s, La No. 20 Cantina, Hamburguesía, Sonora Prime and Maya de Asia, among others. In addition, it houses nationally and internationally recognized brands such as Gran Chapur, Cinépolis, Forever 21, Purificación García, Steve Madden, Innovasport, Bimba y Lola, Miniso, Sephora, Sunglass Hut and Petco.
<br><br>
This Lifestyle Retail Center, with its modern design combined with local influences, is one of the most important retail venues in the entire southeast region of Mexico.';
$lang['the-harbor_info'] = 'Lifestyle Retail Center | +50,000 m2 of leasable space | +300 linear meters of frontage on Av. Paseo Montejo | +120 retail locations | +1,300 parking spaces | Opened: October 2018';

// MARIANA PUERTO CANCÚN
$lang['marina_title'] = 'MARINA PUERTO CANCÚN, CANCÙN';
$lang['marina_description'] = "Marina Puerto Cancun is one of the best Lifestyle Retail Centers in the area, a must-visit destination in Cancun. Explore the unique offer of shops, restaurants and entertainment that awaits you. Accompanied by the best outdoor environment, full of style and vibrant energy. Visit us and live the unbeatable experiences while creating incredible memories with the best view of our marina.
<br><br>
The Lifestyle Center has recognized brands such as H&M, Cinépolis, Zara, Bershka, Hugo Boss, Iló, Innovasport, Lacoste, Pandora, Original Pinguin, Prada, and Sephora among many others. And its great restaurants include La No. 20 Cantina, Cenacolo, Distrito Gourmet, Hiromi and Montemar, among others.
<br><br>
Located in the fastest growing area of the city of Cancun, the complex includes a marina for up to 700 boats, the largest in the Mexican Caribbean.";
$lang['marina_info'] = 'Lifestyle Retail Center | + 45,000 m2 of leasable space | + 150 retail locations | +2,000 parking spaces | Marina for more than 700 boats. | Acquired: August 2018.';

// CALLE CORAZÓN
$lang['calle-corazon_title'] = 'CALLE CORAZÓN';
$lang['calle-corazon_description'] = "Calle Corazon is a lifestyle Retail Center located in the thriving tourist destination of Playa del Carmen, Quintana Roo. This development offers a unique mix of fashion, gastronomy, and entertainment.
<br><br>
It has national and international brands such as, Bath & Body Works, H&M, Lacoste, Steve Maden, Sephora, Puma, Victoria's Secret among others. And its unique culinary offerings inlcude La No. 20 Cantina, Harry’s, and Sushi Roll among others.
<br><br>
The project is characterized by its innovative design that, together with its ideal distribution of commercial spaces and green areas, create a modern and vibrant atmosphere. The development respects the environment and ecosystem of the Rivera Maya, offering a premiere design for the national and international tourist markets.";
$lang['calle-corazon_info'] = 'Lifestyle Retail Center | +15,000 m2 of leasable space | +105 linear meters of front on 5th Avenue | +30 retail locations | Opened: March 2013';

// ALTAVISTA 147
$lang['altavista-147_title'] = 'ALTAVISTA 147';
$lang['altavista-147_description'] = 'Altavista 147 is a Lifestyle Center located in one of the most important and exclusive corridors in the south of Mexico City, in the heart of San Ángel.
<br><br>
It is distinguished by its notable brands such as Carolina Herrera, MaxMara, Elevation, Tane., Siclo among others. Also known for its exclusive restaurant offering such as Tori Tori, Casa Ó, Farina and Ojo de Agua.
<br><br>
Altavista 147 is more than just a retail center, it is a place that showcases the performing arts and cultural activities. The intimate roof garden has hosted numerous exclusive cultural events, exhibitions, concerts, fashion shows, and society events';
$lang['altavista-147_info'] = 'Luxury-Lifestyle Retail Center | +3,000 m2 of leasable | +80 linear meters on Altavista avenue | +17 retail locations | +110 parking spaces | Acquired: January 2013';

/*********************************************
 * 
 *  HOTELES 
 * 
 * *******************************************/

// THOMPSON MAIN HOUSE
$lang['thompson-main-house_title'] = 'THOMPSON MAIN HOUSE';
$lang['thompson-main-house_description'] = 'Enjoy sun-splashed luxury in the heart of Riviera Maya at our dreamscape rooftop oasis on Quinta Avenida, Thompson Main House. Our adults-only urban resort is set within Playa’s vibrant central quarter known as Quinta Avenida, this refined destination is yours to explore.
<br><br>
Rest and relax by day, then swap bathing suits for dancing shoes and take the night to another level at our rooftop pool oasis, where stunning panoramic views and danceable beats will keep you out late into the Riviera Maya night. A rare combination of lively nightlife, exquisite cuisine, and space to still escape...
<br><br>
Thompson Playa del Carmen quite literally sits above the rest on the Riviera Maya. Perfect for adventurous couple’s weekends, unforgettable bachelorette parties or large group special occasions. Encapsulated in the renowned mid-century designs of Mexico City’s Niz + Chauvet, there isn’t a detail to be missed or a moment you’ll forget.';
$lang['thompson-main-house_info'] = '+90 Luxury hotel rooms | +3,000 m2 of rooftop bar | Alessia Dayclub | Catch Restaurant | Opened: November 2015.';

// THOMPSOM BEACH HOUSE
$lang['thompson-beach-house_title'] = 'THOMPSON BEACH HOUSE';
$lang['thompson-beach-house_description'] = 'For a relaxed and intimate bohemian vibe, revel in our exclusive Playa del Carmen beachfront hotel just a short walk from our Thompson Main House address on Quinta Avenida. Surround yourself in mid-century contemporary style, take a dip in the ocean, lounge by the pool or reserve a cabana for personalized service and attention.
<br><br>
An exclusive beachfront experience with pool, private cabanas and day beds reserved for Beach House guests and an oceanfront lounge Wood fire grilled seafood, ceviches and inspired twists on traditional Mexican favorites, along with poolside cocktails at the beachfront C Grill With just 27 rooms and suites and intuitive service, these oceanfront accommodations on the Riviera Maya offer an idyllic oasis for destination weddings, honeymoons and romantic getaways, as well as family vacations, milestone events and corporate retreats.';
$lang['thompson-beach-house_info'] = '+20 Luxury Suites | Private Beach Club | Pool Bar & Restaurant | Opened: February 2016';

// MONTAGE
$lang['montage_title'] = 'MONTAGE LOS CABOS';
$lang['montage_description'] = 'Blending natural beauty with authentic Mexican culture, Montage Los Cabos is an idyllic sanctuary perched upon the premier beachfront in Cabo San Lucas. Spanning 39 acres along the gentle azure waters of Santa Maria Bay, the resort features walk-in access to the finest swimming, snorkeling, and diving in the region.
<br><br>
Montage Los Cabos offers its guests highly personalized and authentic experiences, including wellness programs and beauty treatments at its exclusive Montage Spa. It offers an array of Mexican culinary traditions with fresh and local ingredients with an emphasis on the region’s abundance of seafood.
<br><br>
It also offers a wide range of sporting activities in the Activities Pavilion such as tennis courts, basketball court, mini-golf, and recreational sports. The complex also has a great location adjacent to the famous Twin Dolphin and Cabo del Sol golf courses and the best food and nightlife offerings in the region.';
$lang['montage_info'] = '+122 Luxury hotel rooms | +52 Luxury Residences | +230 meters of frontage on the Bay of Santa Maria. | Spa & Restaurant | Opened: April 2019.';

/*********************************************
 * 
 *  OFICINAS 
 * 
 * *******************************************/

 // THE LANDMARK OFICINAS
$lang['the-landmark-gdl-oficinas_title'] = 'THE LANDMARK OFFICES';
$lang['the-landmark-gdl-oficinas_description'] = 'The Landmark Offices boast over 20,000 m² of AAA office space on 16 levels, ranging from floor 4 to PH4, offering unrivaled 180-degree views of the city. The structural design allows the spaces to enjoy great flexibility and versatility, ideal for achieving efficient distributions. The glass façade Duovent ®️ allows the entry of abundant natural light to the offices.
The project integrates two iconic towers that take advantage of its unbeatable location to offer spectacular views and expand the level of first-class office space in Jalisco.
With a privileged location in Puerta de Hierro, the most exclusive and prestigious area in Jalisco, The Landmark Offices is integrated with a spectacular commercial space with top national and international brands and a luxury residential tower.';
$lang['the-landmark-gdl-oficinas_info'] = '+20,000 m2 of AAA Office Space | +250 m of frontage in Puerta de Hierro & Av. Patria | We Work Corporate Headquarters | Sky-lobby | +600 bicycle ports | 6 exclusive elevevators | Opened: November 2018';

/*********************************************
 * 
 *  RESIDENCIAS 
 * 
 * *******************************************/

 // THE LANDMARK RESIDENCIA
$lang['the-landmark-gdl-residencial_title'] = 'THE LANDMARK RESIDENCES';
$lang['the-landmark-gdl-residencial_description'] = 'Located on the corner of Blvd. Puerta de Hierro and Av. Patria, The Landmark Residences has a privileged location. The project counts with upscale amenities, luxury finishes, and direct access to our exclusive Lifestyle Retail Center, residents truly have access to the best of both worlds.
<br><br>
The Landmark Residences have taken the responsibility of designing such a prominent development in Guadalajara´s Metropolitan Area. We have created a building that is innovative, bold, and refined.
<br><br>
The Landmark Residences has it all: Exclusivity, style, avant-garde design, comfort, security, and functionality.';
$lang['the-landmark-gdl-residencial_info'] = '150 Luxury Residences | Event Hall | Gym, Pool & Spa | Playroom | Sky garden | Opened: November 2018';

// MONTAGE RESIDENCIA
$lang['montage-residencial_title'] = 'MONTAGE RESIDENCES';
$lang['montage-residencial_description'] = 'Montage Residences offer a sophisticated yet comfortable indoor-outdoor living experience in a seaside desert paradise. The artfully crafted two and three-bedroom luxury residences embrace a contemporary design while paying the utmost respect to Cabo’s desert beauty.
<br><br>
Explore the private residences of Montage Los Cabos, a Forbes Five-Star and AAA Five-Diamond resort, and enjoy extraordinary amenities and experiences, including hiking, swimming, and dining.
<br><br>
Montage Residences offer a sophisticated yet comfortable indoor-outdoor living experience. Imagine living in a seaside desert paradise. Sun-splashed adventure by day, gourmet dining by night. Here at Montage Residences Los Cabos, your oasis awaits.';
$lang['montage-residencial_info'] = '52 Luxury Residences | +230 m frontage along Santa María Bay | Spa & Resturants | Full Access to the Montage Hotel Amenities | Opened: April 2019';

/*********************************************
 * 
 *  PROYECTOS EN DESARROLLO
 * 
 * *******************************************/

 // THE PARK SAN LUIS POTOSÍ
$lang['the-park_title'] = 'THE PARK SAN LUIS POTOSÍ';
$lang['the-park_description'] = "Located in San Luis Potosi, The Park is a mixed-use project developed with innovative architecture by renowned firm Elkus Manfredi Architects.
<br><br>
The project will include class AAA office space, a luxury hotel, luxury residential towers, and a world-class lifestyle retail center. The project will bring together the best mix of fashion, cuisine and entertainment to offer a unique experience to all of its visitors.
<br><br>
The Park San Luis Potosí will have + 65,000 m² of commercial area, it is a project that will surely become the city´s center of attraction.  This project fulfills Thor Urbana's promise and strategy of developing iconic projects in the best locations of the most important cities in the country.";
$lang['the-park_info'] = 'Mixed-use Asset | +65,000 m2 of leasable space | +2,000 parking spaces | Opening: summer 2022';

// THE LANDMARK TIJUANA
$lang['landmark-tijuana_title'] = 'THE LANDMARK TIJUANA';
$lang['landmark-tijuana_description'] = 'The Landmark Tijuana is a mix-used project with innovative architecture and design by acclaimed firm Elkus Manfredi Architects.
<br><br>
The project has +28,000 m² of commercial area, +14,000 m² of AAA-class offices, +190 condominiums and a hotel with +200 rooms.
<br><br>
The project is in the so-called “Golden Zone”, which is the area with the highest real estate growth in the city, along with vertical residential constructions of premier quality. This will be the most innovative Lifestyle Retail center in the region.';
$lang['landmark-tijuana_info'] = 'Lifestyle Center | +28,000 m2 of leasable area | +14,000 m2 of AAA office space | +190 Residences | Hotel with +200 Rooms | +2,000 Parking Spaces | Opening: fall 2022';

// THE RITZ-CARLTON MEXICO CITY
$lang['the-ritz_title'] = 'THE RITZ-CARLTON MEXICO CITY';
$lang['the-ritz_description'] = "A luxury hotel arrives in the financial center of Mexico City, offering astonishing views The Ritz Carlton is one of the tallest buildings of the city.
<br><br>
Rising 58 stories in the heart of the financial center, the home of The Ritz-Carlton, Mexico City adds a striking structure to the city’s skyline. Located on floors 36 to 47, the hotel offers sweeping views of Chapultepec Park from each of its 153 accommodations and its private residences. Luxury amenities include a Club Lounge, a Mediterranean-inspired bar and restaurant, a spa, pool, fitness center and meeting and event spaces.
<br><br>
The Ritz-Carlton, Mexico City is situated in Mexico City in the Mexico DF region, 800 meters from Chapultepec Castle and less than 1 km from The Angel of Independence.";
$lang['the-ritz_info'] = '+150 Hotel Rooms | Private Residences | Club Lounge | Bar & Restaurant | Spa & Fitness Center | Helipad and sky lounge | Opening: summer 2021';

// FOUR SEASONS RESORT & RESIDENCES CAYE CHAPEL, BELIZE
$lang['caye-chapel_title'] = 'FOUR SEASONS PRIVATE ISLAND & RESORT CAYE CHAPEL';
$lang['caye-chapel_description'] = 'Perched on the edge of the UNESCO World Heritage designated Belize Barrier Reef, Caye Chapel Belize is a sustainably focused, 300-acre private island and luxury resort community that balances the vast adventures of the open sea with exacting service and amenities of Four Seasons. Located just 8 minutes by plane from Belize City, the resort offers guests 114 hectares of private island, bungalows, overwater bungalows, and recreational experiences.
<br><br>
The luxury resort has a private airstrip and heliport, a spectacular 20 slip marina, a select variety of shops, restaurants, bars, spa, The Fabien Cousteau Nature and Conservation Center and a White Shark 9-hole course with reversible routing and Pitch and Putt Course.
<br><br>
In addition, the Great Blue Hole, which has become a national heritage, is only a few kilometers away. Inspiring design, curated adventure, and world class amenities; all things in their right place and at the right time.';
$lang['caye-chapel_info'] = '100 Luxury Hotel Rooms | 18 Over Water Bungalows | +100 Residential oceanfront lots | 48 Four Seasons Branded Private Residences | 10 on island Food & Beverage offerings | White Shark & Lorena Ochoa reversible 9-hole golf course | Fabien Cousteau Nature and Conservation Center / Adventure Center | Sunrise Sanctuary & Fitness Center | Private Airstrip and Helipad | Opening: 4Q 2023';

// TULUM LIFESTYLE HOTEL & BEACH CLUB
$lang['tulum-beach-club_title'] = 'TULUM HOTEL & BEACH CLUB';
$lang['tulum-beach-club_description'] = 'Tulum is the perfect mix of the charming, iconic, and modern. Enter this wonderful luxury lifestyle hotel and beach club that will connect your body with the spirit of the iconic Mayan jungle.
<br><br>
Its eco-chic atmosphere makes it the ideal spot for those who prefer to spend a few days of peace and quiet, but at the same time want to live unique experiences. Take advantage of the astonishing sites in a once of a lifetime location on a stunning world-renowned hotel in a tropical setting.
<br><br>
Your adventure starts here, this magnificent development is the perfect place to really get away and simply enjoy at its purest form.';
$lang['tulum-beach-club_info'] = '+60 Luxury Suites | +7 Hotel Villas | 100 meters of frontage on the beach | Restaurant & Beach Club | Opening: fall 2022';

// RITZ-CARLTON RESERVE COSTA CANUVA
$lang['riviera-nayarit_title'] = 'THE RITZ-CARLTON RESERVE HOTEL & RESIDENCIAL';
$lang['riviera-nayarit_description'] = "In the heart of the Riviera de Nayarit, The Ritz Carlton Reserve will have access to pristine coastal waters which will complement an exclusive and private beach club as well as a signature 18-hole golf course.
<br><br>
The complex will have an access at each end of the coast and will have over 90 luxury rooms, 14 are suites plus 20 luxury residences. The hotel has a Spa, three exclusive restaurants and a beach club for all its guests who want to enjoy a luxurious stay surrounded by incomparable nature.
<br><br>
A mix of the charming, modern, and tried and true away from home. Soak up year-round sunshine and seamless indoor-outdoor living. With panoramic Pacific views, the toughest decision you'll make is whether to dine outside or in.";
$lang['riviera-nayarit_info'] = '+90 Hotel Rooms | 14 Luxury Suites | 20 Luxury Residences | Luxury Hotel | Spa & Beach Club | Opening: fall 2023';

// THE LANDMARK RESERVE
$lang['landmark-reserve_title'] = 'THE LANDMARK RESERVE';
$lang['landmark-reserve_description'] = 'The Landmark Reserve offers breathtaking views and will become part of The Landmark community. Here you can find your new home. Designed by Sordo Madaleno Architects the development will have a residential tower divided into 2 wings, each with independent access to exclusive residences.
<br><br>
Exceptional footprint allows for the creation of the most gracious residences in the City. Spaces ranging from 87 m2 to 199 m2, with one, two or three bedrooms and spectacular terraces that adapt perfectly to your lifestyle.
<br><br>
The Landmark Reserve will offer the most gracious residences in the city. These expansive spaces feature custom details, the finest luxury finishes, and the most dynamic views in Guadalajara. Each home offers grand living spaces ideal for gatherings both large and small.';
$lang['landmark-reserve_info'] = '+230 Residences | Gym, Pool & Spa | Sky garden | Teen lounge | Yoga lounge | Sports Bar | Library | Opening: fall 2024';





$lang['menu_item_somos'] = 'ABOUT US';
$lang['menu_item_mision'] = 'MISSION & VISION';
$lang['menu_item_historia'] = 'HISTORY';
$lang['menu_item_estretegia'] = 'STRATEGY';
$lang['menu_item_socios_estrategicos'] = 'STRATEGIC PARTNERS';

$lang['menu_item_portafolio'] = 'PORTAFOLIO';
$lang['menu_item_centros'] = 'LIFESTYLE RETAIL CENTERS';
$lang['menu_item_hoteles'] = 'HOTELS';
$lang['menu_item_oficinas'] = 'OFFICE';
$lang['menu_item_residencial'] = 'RESIDENTIAL';
$lang['menu_item_desarrollo'] = 'NEW DEVELOPMENTS';

$lang['menu_item_leasing'] = 'LEASING';
$lang['menu_item_prensa'] = 'PRENSA';
$lang['menu_item_socios_comericales'] = 'BRAND PARTNERSHIPS';
$lang['menu_item_responsabilidad'] = 'SOCIAL IMPACT';
$lang['menu_item_bolsa'] = 'CAREERS';
$lang['menu_item_contacto'] = 'CONTACT';

return $lang;