<?php

// PORTAFOLIO
$lang['portafolio_title'] = 'OUR PORTFOLIO';
$lang['portafolio_description'] = '<p class="mb-3">Thor Urbana has a unique real estate portfolio in Mexico and Latin America, seeking to be recognized for its innovative design, construction and unmatched experiences.</p>
<p>We develop and operate innovative and profitable projects that meet the present and future needs of our society. This translates into benefits for investors and an important boost to the economic and social development of the communities where the company operates.</p>';

$lang['portafolio_filters'] = 'FILTERS';
$lang['portafolio_location'] = 'Location';
$lang['portafolio_show_all'] = 'View all';

// CENTROS COMERCIALES
$lang['centers_title'] = 'LIFESTYLE RETAIL CENTERS';

// HOTELES
$lang['hotels_title'] = 'HOTELS';

// OFICINAS
$lang['office_title'] = 'OFFICE';

// RESIDENCIAL
$lang['residential_title'] = 'RESIDENTIAL (MIXED-USED)';

// PROYECTOS EN DESARROLLO
$lang['developments_title'] = 'NEW DEVELOPMENTS';

$lang['operation'] = 'IN OPERATION';

return $lang;