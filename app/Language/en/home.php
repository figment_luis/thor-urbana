<?php

// Sección de Bienvenida
$lang['welcome_title'] = 'THOR URBANA';
$lang['welcome_description_p1'] = 'Thor Urbana is a leading real estate investment and development company based in Mexico. Through a vertically integrated platform, it specializes in the sourcing, development, acquisition, repositioning, leasing and administration of real estate projects including lifestyle retail centers, luxury hotels and resorts, industrial parks, as well as mixed-used assets in Mexico and Latin America.';
$lang['welcome_description_p2'] = 'The company has developments that total more than 1.5 million square meters in locations including Mexico City, Guadalajara, Playa del Carmen, Cancun, Mérida, Metepec, Los Cabos, Tulum, San Luis Potosí, Tijuana, Riviera Nayarit, Belize among many others.';

// Sección Mapa
$lang['locations_title'] = 'OUR LOCATIONS';

// Sección Carrusel ultimas noticias
$lang['news_title'] = 'LATEST NEWS';







$lang['menu_item_somos'] = 'ABOUT US';
$lang['menu_item_mision'] = 'MISSION & VISION';
$lang['menu_item_historia'] = 'HISTORY';
$lang['menu_item_estretegia'] = 'STRATEGY';
$lang['menu_item_socios_estrategicos'] = 'STRATEGIC PARTNERS';

$lang['menu_item_portafolio'] = 'PORTAFOLIO';
$lang['menu_item_centros'] = 'LIFESTYLE RETAIL CENTERS';
$lang['menu_item_hoteles'] = 'HOTELS';
$lang['menu_item_oficinas'] = 'OFFICE';
$lang['menu_item_residencial'] = 'RESIDENTIAL';
$lang['menu_item_desarrollo'] = 'NEW DEVELOPMENTS';

$lang['menu_item_leasing'] = 'LEASING';
$lang['menu_item_prensa'] = 'PRENSA';
$lang['menu_item_socios_comericales'] = 'BRAND PARTNERSHIPS';
$lang['menu_item_responsabilidad'] = 'SOCIAL IMPACT';
$lang['menu_item_bolsa'] = 'CAREERS';
$lang['menu_item_contacto'] = 'CONTACT';

return $lang;