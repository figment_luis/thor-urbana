<?php

$lang['carrers_title'] = 'JOIN OUR TEAM';
$lang['carrers_description'] = 'Thor Urbana believes that its human capital is the most valuable asset and the most critical factor to its success. We continuously seek candidates driven to excel and to thrive in a team environment. We thank you in advance for your interest.';
$lang['carrers_subtitle'] = 'CAREERS';

$lang['carrers_form_name'] = 'NAME';
$lang['carrers_form_email'] = 'EMAIL';
$lang['carrers_form_jobs'] = 'JOB OF INTEREST';
$lang['carrers_form_resume'] = 'RESUME';
$lang['carrers_form_button'] = 'SEND';

$lang['carrers_item_1'] = '
<h2 class="secondary-title">Legal Assistant</h2>
<div class="contact-list">
    <p><b>Location: </b>Reforma 2620, Mexico City</p>
    <p><b>Email: </b><a href="mailto:smacip@thorurbana.com">smacip@thorurbana.com</a></p>
</div>';
$lang['carrers_item_2'] = '
<h2 class="secondary-title">Recruitment Analyst</h2>
<div class="contact-list">
    <p><b>Location: </b>Reforma 2620, Mexico City</p>
    <p><b>Email: </b><a href="mailto:smacip@thorurbana.com">smacip@thorurbana.com</a></p>
</div>';
$lang['carrers_item_3'] = '
<h2 class="secondary-title">Associate Jr. Capital Markets</h2>
<div class="contact-list">
    <p><b>Location: </b>Reforma 2620, Mexico City</p>
    <p><b>Email: </b><a href="mailto:lmedina@thorurbana.com">lmedina@thorurbana.com</a></p>
</div>';

return $lang;