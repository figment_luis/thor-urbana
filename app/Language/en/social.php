<?php

$lang['social_title'] = 'SOCIAL IMPACT';
$lang['social_title_1'] = 'Home Construction: PROGRAMA DE FUNDACIÓN TECHO';
$lang['social_description_1'] = 'Thor Urbana has partnered with Fundacion Techo to assist in the construction of houses in low-income rural areas around Mexico. With the support from our collaborators, we helped to build emergency housing for communities in need.';
$lang['social_title_2'] = 'Building and Growing Program:';
$lang['social_description_2'] = 'An educational model that emerged as a result of the COVID 19 Pandemic, the program continues to innovate by providing education to our collaborators. The learning process is carried out by a teacher (educational advisor), with the aim of guiding, orienting and facilitating the use of digital learning tools and materials. The program has the goal of promoting interaction between students through technological means, and to motivate them to achieve their educational goals. ';




$lang['menu_item_somos'] = 'ABOUT US';
$lang['menu_item_mision'] = 'MISSION & VISION';
$lang['menu_item_historia'] = 'HISTORY';
$lang['menu_item_estretegia'] = 'STRATEGY';
$lang['menu_item_socios_estrategicos'] = 'STRATEGIC PARTNERS';

$lang['menu_item_portafolio'] = 'PORTAFOLIO';
$lang['menu_item_centros'] = 'LIFESTYLE RETAIL CENTERS';
$lang['menu_item_hoteles'] = 'HOTELS';
$lang['menu_item_oficinas'] = 'OFFICE';
$lang['menu_item_residencial'] = 'RESIDENTIAL';
$lang['menu_item_desarrollo'] = 'NEW DEVELOPMENTS';

$lang['menu_item_leasing'] = 'LEASING';
$lang['menu_item_prensa'] = 'PRENSA';
$lang['menu_item_socios_comericales'] = 'BRAND PARTNERSHIPS';
$lang['menu_item_responsabilidad'] = 'SOCIAL IMPACT';
$lang['menu_item_bolsa'] = 'CAREERS';
$lang['menu_item_contacto'] = 'CONTACT';

return $lang;