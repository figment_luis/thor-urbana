<?php

$lang['contact_title_1'] = 'CORPORATE OFFICES';
$lang['contact_areas_legal'] = 'Legal';
$lang['contact_areas_it'] = 'Human Resources & IT';
$lang['contact_areas_finance'] = 'Capital Markets & Finance';
$lang['contact_areas_investment'] = 'Investment';
$lang['contact_areas_development'] = 'Development';
$lang['contact_areas_marketing'] = 'Marketing';
$lang['contact_areas_operations'] = 'Operations';
$lang['contact_areas_leasing'] = 'Leasing';


$lang['contact_phone'] = 'Telephone';

$lang['contact_title_3'] = 'REGIONAL OFFICES';
$lang['contact_title_areas'] = 'OUR AREAS';




$lang['menu_item_somos'] = 'ABOUT US';
$lang['menu_item_mision'] = 'MISSION & VISION';
$lang['menu_item_historia'] = 'HISTORY';
$lang['menu_item_estretegia'] = 'STRATEGY';
$lang['menu_item_socios_estrategicos'] = 'STRATEGIC PARTNERS';

$lang['menu_item_portafolio'] = 'PORTAFOLIO';
$lang['menu_item_centros'] = 'LIFESTYLE RETAIL CENTERS';
$lang['menu_item_hoteles'] = 'HOTELS';
$lang['menu_item_oficinas'] = 'OFFICE';
$lang['menu_item_residencial'] = 'RESIDENTIAL';
$lang['menu_item_desarrollo'] = 'NEW DEVELOPMENTS';

$lang['menu_item_leasing'] = 'LEASING';
$lang['menu_item_prensa'] = 'PRENSA';
$lang['menu_item_socios_comericales'] = 'BRAND PARTNERSHIPS';
$lang['menu_item_responsabilidad'] = 'SOCIAL IMPACT';
$lang['menu_item_bolsa'] = 'CAREERS';
$lang['menu_item_contacto'] = 'CONTACT';

return $lang;