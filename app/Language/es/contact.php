<?php

$lang['contact_title_1'] = 'OFICINAS CORPORATIVAS';
$lang['contact_areas_legal'] = 'Legal';
$lang['contact_areas_it'] = 'Recursos Humanos';
$lang['contact_areas_finance'] = 'Finanzas y Capital Markets';
$lang['contact_areas_investment'] = 'Inversiones';
$lang['contact_areas_development'] = 'Desarrollo';
$lang['contact_areas_marketing'] = 'Marketing';
$lang['contact_areas_operations'] = 'Operaciones';
$lang['contact_areas_leasing'] = 'Leasing';


$lang['contact_phone'] = 'Teléfono';

$lang['contact_title_3'] = 'OFICINAS REGIONALES';
$lang['contact_title_areas'] = 'NUESTRAS ÁREAS';



$lang['menu_item_somos'] = 'QUIÉNES SOMOS';
$lang['menu_item_mision'] = 'MISIÓN/VISIÓN';
$lang['menu_item_historia'] = 'HISTORIA';
$lang['menu_item_estretegia'] = 'ESTRATEGIA';
$lang['menu_item_socios_estrategicos'] = 'SOCIOS ESTRATÉGICOS';

$lang['menu_item_portafolio'] = 'PORTAFOLIO';
$lang['menu_item_centros'] = 'CENTROS COMERCIALES';
$lang['menu_item_hoteles'] = 'HOTELES';
$lang['menu_item_oficinas'] = 'OFICINAS';
$lang['menu_item_residencial'] = 'RESIDENCIAL';
$lang['menu_item_desarrollo'] = 'PROYECTOS EN DESARROLLO';

$lang['menu_item_leasing'] = 'LEASING';
$lang['menu_item_prensa'] = 'PRENSA';
$lang['menu_item_socios_comericales'] = 'SOCIOS COMERCIALES';
$lang['menu_item_responsabilidad'] = 'RESPONSABILIDAD SOCIAL';
$lang['menu_item_bolsa'] = 'BOLSA DE TRABAJO';
$lang['menu_item_contacto'] = 'CONTACTO';

return $lang;