<?php

$lang['carrers_title'] = 'BOLSA DE TRABAJO';
$lang['carrers_description'] = 'En Thor Urbana queremos ser los mejores en lo que hacemos, es por eso que detrás de nuestros proyectos contamos con un gran equipo comprometido y visionario que diariamente comparten su visión y pasión para crear y operar increíbles espacios de convivencia. Hoy somos más de 185 colaboradores y continuamos creciendo, forma parte de nuestro equipo.';
$lang['carrers_subtitle'] = 'VACANTES';

$lang['carrers_form_name'] = 'NOMBRE';
$lang['carrers_form_email'] = 'CORREO ELECTRÓNICO';
$lang['carrers_form_jobs'] = 'VACANTE DE INTERÉS';
$lang['carrers_form_resume'] = 'CURRICULUM VITAE';
$lang['carrers_form_button'] = 'ENVIAR';

$lang['carrers_item_1'] = '
<h2 class="secondary-title">Asistente Legal</h2>
<div class="contact-list">
    <p><b>Ubicación: </b>Reforma 2620, CDMX</p>
    <p><b>Email: </b><a href="mailto:smacip@thorurbana.com">smacip@thorurbana.com</a></p>
</div>';
$lang['carrers_item_2'] = '
<h2 class="secondary-title">Analista de Selección</h2>
<div class="contact-list">
    <p><b>Ubicación: </b>Reforma 2620, CDMX</p>
    <p><b>Email: </b><a href="mailto:smacip@thorurbana.com">smacip@thorurbana.com</a></p>
</div>';
$lang['carrers_item_3'] = '
<h2 class="secondary-title">Asociado Jr Capital Markets</h2>
<div class="contact-list">
    <p><b>Ubicación: </b>Reforma 2620, CDMX</p>
    <p><b>Email: </b><a href="mailto:lmedina@thorurbana.com">lmedina@thorurbana.com</a></p>
</div>';

return $lang;