<?php

// Sección de Bienvenida
$lang['welcome_title'] = 'THOR URBANA';
$lang['welcome_description_p1'] = "Thor Urbana es una de las empresas de desarrollo e inversión inmobiliaria líderes en México. A través de una plataforma verticalmente integrada, se especializa en la búsqueda, adquisición, desarrollo, reposicionamiento, comercialización, operación, administración y disposición de múltiples proyectos inmobiliarios, incluyendo lifestyle centers, hoteles de lujo, proyectos de usos mixtos y parques industriales en las principales ciudades y destinos turísticos.";
$lang['welcome_description_p2'] = 'La empresa tiene desarrollos que suman más de 1.5 millones de metros cuadrados en distintos puntos del país y el exterior, como la Ciudad de México, Guadalajara, Playa del Carmen, Cancún, Mérida, Metepec, Los Cabos, Tulum, San Luis Potosí, Tijuana, Riviera Nayarit y Belice, entre otros.';

// Sección Mapa
$lang['locations_title'] = 'NUESTRAS UBICACIONES';

// Sección Carrusel ultimas noticias
$lang['news_title'] = 'ÚLTIMAS NOTICIAS';



$lang['menu_item_somos'] = 'QUIÉNES SOMOS';
$lang['menu_item_mision'] = 'MISIÓN/VISIÓN';
$lang['menu_item_historia'] = 'HISTORIA';
$lang['menu_item_estretegia'] = 'ESTRATEGIA';
$lang['menu_item_socios_estrategicos'] = 'SOCIOS ESTRATÉGICOS';

$lang['menu_item_portafolio'] = 'PORTAFOLIO';
$lang['menu_item_centros'] = 'CENTROS COMERCIALES';
$lang['menu_item_hoteles'] = 'HOTELES';
$lang['menu_item_oficinas'] = 'OFICINAS';
$lang['menu_item_residencial'] = 'RESIDENCIAL';
$lang['menu_item_desarrollo'] = 'PROYECTOS EN DESARROLLO';

$lang['menu_item_leasing'] = 'LEASING';
$lang['menu_item_prensa'] = 'PRENSA';
$lang['menu_item_socios_comericales'] = 'SOCIOS COMERCIALES';
$lang['menu_item_responsabilidad'] = 'RESPONSABILIDAD SOCIAL';
$lang['menu_item_bolsa'] = 'BOLSA DE TRABAJO';
$lang['menu_item_contacto'] = 'CONTACTO';

return $lang;