<?php

$lang['leasing_description'] = 'Nuestras centros comerciales cuentan con las mejores boutiques y restaurantes a nivel mundial. Estamos comprometidos a ofrecer el mejor mix de marcas nacionales e internacionales en cada uno de ellos. Contamos con el mejor equipo de leasing, el cual se asegura que continuamente nuestros proyectos tengan marcas únicas y de vanguardia, únicas siendo éstas las más codiciadas en el mercado. Si estás interesado en formar parte de alguno de nuestros proyectos, por favor proporciona tus datos en el siguiente formulario y nos pondremos en contacto contigo o escríbenos al siguiente número vía WhatsApp 55 8106 0290';

$lang['leasing_address_phone'] = 'Teléfono';
$lang['leasing_form_name'] = 'NOMBRE';
$lang['leasing_form_email'] = 'CORREO ELECTRÓNICO';
$lang['leasing_form_project'] = 'DESARROLLO DE INTERÉS';
$lang['leasing_form_message'] = 'Mensaje';

$lang['leasing_form_button'] = 'ENVIAR';


$lang['leasing_centros'] = 'Centros comerciales';
$lang['leasing_oficinas'] = 'Oficinas';
$lang['leasing_proyectos'] = 'Proyectos en Desarrollo';


$lang['partner_title'] = 'SOCIOS COMERCIALES';
$lang['partner_description_p1'] = 'Nuestros socios comerciales son algunas de las mejores marcas de moda, estilo de vida, entretenimiento y gastronomía a nivel mundial. Son reconocidos nacional e internacionalmente por su servicio personalizado y atención insuperable como los clientes que visitan cada una de nuestras propiedades.';
$lang['partner_description_p2'] = 'Todos nuestros proyectos cuentan con un mix de marcas único el cual nos distingue de nuestra competencia convirtiéndonos en la mejor opción tanto para inquilinos como para nuestros clientes. Ofrecemos una experiencia de compra única a todos nuestros clientes, lo cual crea alta fidelidad, al igual que un canal dinámico para todos nuestros socios.';



$lang['partner_category_moda'] = 'MODA';
$lang['partner_category_gastronomia'] = 'GASTRONOMÍA';
$lang['partner_category_entretenimiento'] = 'ENTRETENIMIENTO';
$lang['partner_category_estilo-de-vida'] = 'ESTILO DE VIDA';

$lang['partner_show_all'] = 'Ver todos';

$lang['partner_message'] = 'Entre muchas otras.';

return $lang;
