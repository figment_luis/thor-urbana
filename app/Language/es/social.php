<?php

$lang['social_title'] = 'RESPONSABILIDAD SOCIAL';
$lang['social_title_1'] = 'Construcción de Viviendas: Programa de Fundación Techo';
$lang['social_description_1'] = 'Apoyo de colaboradores a fin de construir viviendas de emergencia en zonas rurales. El voluntariado corporativo es una herramienta de triple impacto que genera valor al entorno, a la empresa y a sus colaboradores. A través de una experiencia trabajo en equipo, con impacto tangible y a muy corto plazo, que establece fuertes lazos entre los participantes, hacia Thor Urbana y con la superación de la pobreza.';
$lang['social_title_2'] = 'Programa Construyendo y Creciendo';
$lang['social_description_2'] = 'Modelo educativo que a consecuencia de la Pandemia COVID 19, Construyendo y Creciendo innova para continuar brindando educación a personas. El proceso de aprendizaje lo realiza un docente (asesor educativo), con el objetivo de guiar el aprendizaje, orientar y facilitar la utilización de recursos y materiales didácticos digitales, promoviendo la interacción entre los estudiantes a través de medios tecnológicos, para motivarlos al logro de los objetivos educativos.';





$lang['menu_item_somos'] = 'QUIÉNES SOMOS';
$lang['menu_item_mision'] = 'MISIÓN/VISIÓN';
$lang['menu_item_historia'] = 'HISTORIA';
$lang['menu_item_estretegia'] = 'ESTRATEGIA';
$lang['menu_item_socios_estrategicos'] = 'SOCIOS ESTRATÉGICOS';

$lang['menu_item_portafolio'] = 'PORTAFOLIO';
$lang['menu_item_centros'] = 'CENTROS COMERCIALES';
$lang['menu_item_hoteles'] = 'HOTELES';
$lang['menu_item_oficinas'] = 'OFICINAS';
$lang['menu_item_residencial'] = 'RESIDENCIAL';
$lang['menu_item_desarrollo'] = 'PROYECTOS EN DESARROLLO';

$lang['menu_item_leasing'] = 'LEASING';
$lang['menu_item_prensa'] = 'PRENSA';
$lang['menu_item_socios_comericales'] = 'SOCIOS COMERCIALES';
$lang['menu_item_responsabilidad'] = 'RESPONSABILIDAD SOCIAL';
$lang['menu_item_bolsa'] = 'BOLSA DE TRABAJO';
$lang['menu_item_contacto'] = 'CONTACTO';

return $lang;