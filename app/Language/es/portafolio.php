<?php

// PORTAFOLIO
$lang['portafolio_title'] = 'PORTAFOLIO';
$lang['portafolio_description'] = '<p class="mb-3">Thor Urbana cuenta con un portafolio inmobiliario de primer nivel en México y en Latinoamérica, buscando ser reconocido por su innovador diseño, construcción, operación y experiencias inigualables.</p>
<p>Desarrollamos y operamos proyectos innovadores y rentables que satisfacen las necesidades presentes y futuras de las personas. Esto se traduce en beneficio para los inversionistas y en un importante impulso al desarrollo económico y social de las comunidades donde opera la compañía, mediante la inversión y la creación de empleos que posibilitan una mejor calidad de vida para las presentes y futuras comunidades.</p>';

$lang['portafolio_filters'] = 'FILTROS';
$lang['portafolio_location'] = 'Ubicación';
$lang['portafolio_show_all'] = 'Ver todos';

// CENTROS COMERCIALES
$lang['centers_title'] = 'LIFESTYLE CENTERS';

// HOTELES
$lang['hotels_title'] = 'HOTELES';

// OFICINAS
$lang['office_title'] = 'OFICINAS';

// RESIDENCIAL
$lang['residential_title'] = 'RESIDENCIAL';

// PROYECTOS EN DESARROLLO
$lang['developments_title'] = 'PROYECTOS EN DESARROLLO';

$lang['operation'] = 'EN OPERACIÓN';

return $lang;