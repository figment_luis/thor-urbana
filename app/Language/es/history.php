<?php

// QUIENES SOMOS
$lang['about_title'] = 'QUIÉNES SOMOS';
$lang['about_description_p1'] = 'Somos una empresa mexicana líder en desarrollo e inversión inmobiliaria y nos caracterizamos por una visión fresca e innovadora que plasmamos en cada uno de nuestros proyectos. Nos especializamos en la búsqueda, adquisición, desarrollo, reposicionamiento, comercialización, administración de múltiples proyectos inmobiliarios que incluyen lifestyle centers, hoteles de lujo, proyectos de usos mixtos y parques industriales en las principales ciudades y destinos turísticos de México y Latinoamérica.';
$lang['about_description_p2'] = 'Contamos con una sólida trayectoria en la industria inmobiliaria y un experimentado equipo que nos ha permitido conformar, a lo largo de la última década, un portafolio de propiedades de calidad institucional que en su conjunto suman más de 1.5 millones de metros cuadrados construidos y una inversión superior a los $2,000 millones de dólares, con presencia en la Ciudad de México, Guadalajara, Playa del Carmen, Cancún, Mérida, Metepec, Los Cabos, Tulum, San Luis Potosí, Tijuana, Riviera Nayarit y Belice entre otros.';

// MISION Y VISION
$lang['mision_title'] = 'MISIÓN / VISIÓN';
$lang['mision_description_p1'] = 'Nuestra misión es desarrollar, comercializar, reposicionar, operar y adquirir desarrollos inmobiliarios de primer nivel, integrales e innovadores, a través de una plataforma verticalmente integrada.';
$lang['mision_description_p2'] = 'Trabajamos diariamente con una visión en mente, posicionar a Thor Urbana como líder en el sector inmobiliario de cada uno de nuestros segmentos de mercado, creando nuevas fuentes de trabajo y un legado socioeconómico; manteniendo un ambiente dinámico y mejorando siempre la calidad de vida del entorno de nuestras comunidades.';

// NUESTRA HISTORIA
$lang['history_title'] = 'NUESTRA HISTORIA';
$lang['history_2012'] = '<li>Se funda Thor Urbana.</li>
<li>Se consolida una alianza estratégica con Thor Equities en Nueva York.</li>
<li>Primer vehículo de inversión levantado con fondo de pensiones canadienses.</li>
<li>Adquisición para reposicionamiento de Calle Corazón (Hotel & Centro Comercial en Playa del Carmen).</li>
<li>Adquisición de tierra: The Shops en 5ta y 8 (Centro Comercial en Playa del Carmen) y Thompson Beach House (Hotel en Playa del Carmen).</li>';
$lang['history_2013'] = '<li>Inicio del proyecto Calle Corazón y Hotel Thompson en Playa del Carmen.</li>
<li>Compra de propiedad para The Landmark Guadalajara.</li>
<li>Adquisición de tierra: The Landmark (Proyecto de usos mixtos en Guadalajara).</li>';
$lang['history_2014'] = '<li>Adquisición de los proyectos Altavista 147 y Masaryk 120 (Centros Comerciales en la Ciudad de México).</li>
<li>Adquisición de tierra: Lomas Verdes (Proyecto de usos mixtos en Estado de México), Town Square (Centro Comercial en Metepec) y The Ritz-Carlton Hotel (Ciudad de México).</li>
<li>Thor Urbana logra colocar $500 M USD de capital.</li>';
$lang['history_2015'] = '<li>Levantamiento del segundo vehículo de inversión levantado con fondo de pensiones canadienses.</li>
<li>Inicio de operaciones: Calle Corazón y The Shops en 5ta y 8.</li>
<li>Compra de la propiedad Montage Hotel en Los Cabos, BCS.</li>
<li>Adquisición de tierra: Montage Hotel & Residencias (Los Cabos), Caye Chapel (Hotel y residencias y lotes de residencias en Belice), The Harbor (Centro Comercial en Mérida).</li>
<li>Thor Urbana logra colocar $1,000 M USD de capital.</li>';
$lang['history_2016'] = '<li>Inicio de operaciones: Thompson Beach House y Masaryk 120.</li>
<li>Adquisición de tierra: The Park (Proyecto de usos mixtos en San Luis Potosí).</li>
<li>Inicio de desarrollo: Montage Hotel & Residencias.</li>
<li>Thor Urbana logra colocar $1,500 M USD de capital.</li>';
$lang['history_2017'] = '<li>Adquisición de activo: The Landmark Tijuana (Proyecto de Usos mixtos en Tijuana).</li>
<li>Thor Urbana logra desarrollar +1 millón de m2 en proyectos.</li>';
$lang['history_2018'] = '<li>Levantamiento del tercer vehículo de inversión (CKD) levantado en la Bolsa Mexicana de Valores con Afores.</li>
<li>Adquisición de activo: Marina Puerto Cancún (Centro Comercial en Cancún).</li>   
<li>Inicio de operaciones: Montage Hotel & Residencias, The Harbor Mérida, The Landmark Guadalajara y Town Square Metepec.</li>
<li>Thor Urbana logra colocar $2,000 M USD de capital.</li>';
$lang['history_2019'] = '<li>Adquisición de tierra: Ritz-Carlton Reserve Costa Canuva (Hotel & Residencias en Nayarit), Casa Magna (Hotel & Residencias en Tulum).</li>
<li>Ronda de Inversión: Caye Chapel (Belice).</li>';
$lang['history_2020'] = '<li>Thor Urbana lanza en sus centros comerciales la plataforma Thor2Go para todos sus locatarios.</li>
<li>Thor Urbana gana licitación del proyecto Aztlán (Parque Urbano en la Ciudad de México).</li>
<li>Adquisición de Tierra en Tulum.</li>';
$lang['history_2021'] = '<li>Emisión de la primera ronda de Certificados Bursátiles Fiduciarios de Proyectos de Inversión (CERPI) en la Bolsa Institucional de Valores (BIVA).</li>';


// ESTRATEGIA
$lang['strategy_title'] = 'ESTRATEGIA';
$lang['strategy_description_p1'] = 'Thor Urbana se encarga de proveer a sus inversionistas importantes rendimientos a través del desarrollo, adquisición y operación de propiedades comerciales, hoteles de lujo, proyectos de usos mixtos y parques industriales, ubicados en las principales ciudades y destinos turísticos de México y Latinoamérica.';
$lang['strategy_description_p2'] = 'Thor Urbana ejecuta su estrategia de la siguiente forma:';
$lang['strategy_item_1'] = 'Busca mercados urbanos con alta densidad de población.';
$lang['strategy_item_2'] = 'Busca propiedades susceptibles de reposicionamiento y replanteamiento comercial.';
$lang['strategy_item_3'] = 'Capitaliza el desarrollo y replanteamiento de oportunidades.';
$lang['strategy_item_4'] = 'Desarrolla e implementa nuevas estrategias de mercadotecnia.';
$lang['strategy_item_5'] = 'Administra sus bienes de forma agresiva para maximizar el flujo de efectivo.';
$lang['strategy_item_6'] = 'Utiliza prudentes niveles de deuda.';

// SOCIOS ESTRATÉGICOS
$lang['partners_title'] = 'SOCIOS ESTRATÉGICOS';
$lang['partners_description_p1'] = 'Parte del éxito de Thor Urbana ha sido la alianza estratégica con dos firmas internacionalmente reconocidas, ambas empresas líderes en el mercado de bienes raíces.';
$lang['partners_description_p2'] = 'Thor Equities, compañía líder en desarrollo, administración e inversión de bienes raíces a nivel global; y GFA Grupo Inmobiliario, empresa mexicana líder en el sector de desarrollo inmobiliario.';
$lang['partners_item_1'] = '<b>THOR EQUITIES</b>: es una empresa líder global en inversión, desarrollo y administración de propiedades comerciales y de usos mixtos de primer nivel, provee valor añadido a través de años de experiencia y de su amplia red de relaciones comerciales de alcance internacional.<br> <br>
Se especializa en proyectos comerciales, residenciales, hoteleros y de usos mixtos en ciudades de primer nivel alrededor del mundo, reconociendo el potencial de cada propiedad, reduciendo gastos operativos, aumentando la satisfacción del inquilino y aprovechando las tendencias del mercado para mantener una ventaja competitiva a largo plazo.';
$lang['partners_item_2'] = '<b>GFA GRUPO INMOBILIARIO</b>: es una de las firmas mexicanas más importantes y sólidas del pais. Ofrece todos los servicios de manera integral para garantizar el mayor éxito de los desarrollos inmobiliarios. <br> <br> La empresa interviene en la promoción y desarrollo inmobiliario, planeación financiera, el diseño y coordinación arquitectónica de obra, comercialización, así como administración de activos. <br><br> El grupo tiene más de 50 años de experiencia, durante los cuales ha sabido interpretar las necesidades y esencia de cada época con una propuesta de espacios residenciales, corporativos, turísticos e institucionales que trascienden estilos y tendencias.';

// NUESTROS PRINCIPIOS
$lang['principals_title'] = 'NUESTROS PRINCIPIOS';
$lang['principals_item_1'] = 'Crear espacios acogedores que la gente ame.';
$lang['principals_item_2'] = 'Respetar y reflejar cada comunidad a la que nos unimos.';
$lang['principals_item_3'] = 'Encontrar un ganar-ganar para todos los involucrados.';
$lang['principals_item_4'] = 'Disfrutamos hacer feliz a la gente en nuestros espacios.';
$lang['principals_item_5'] = 'Tratamos el tiempo como un lujo.';
$lang['principals_item_6'] = 'Perfeccionamos cada detalle, todo es importante.';
$lang['principals_item_7'] = 'Invertimos en el futuro y nunca dejamos de evolucionar.';
$lang['principals_item_8'] = 'Creamos experiencias inigualables.';
$lang['principals_item_9'] = 'Buscamos la integridad en cada acción que realizamos.';




$lang['menu_item_somos'] = 'QUIÉNES SOMOS';
$lang['menu_item_mision'] = 'MISIÓN/VISIÓN';
$lang['menu_item_historia'] = 'HISTORIA';
$lang['menu_item_estretegia'] = 'ESTRATEGIA';
$lang['menu_item_socios_estrategicos'] = 'SOCIOS ESTRATÉGICOS';

$lang['menu_item_portafolio'] = 'PORTAFOLIO';
$lang['menu_item_centros'] = 'CENTROS COMERCIALES';
$lang['menu_item_hoteles'] = 'HOTELES';
$lang['menu_item_oficinas'] = 'OFICINAS';
$lang['menu_item_residencial'] = 'RESIDENCIAL';
$lang['menu_item_desarrollo'] = 'PROYECTOS EN DESARROLLO';

$lang['menu_item_leasing'] = 'LEASING';
$lang['menu_item_prensa'] = 'PRENSA';
$lang['menu_item_socios_comericales'] = 'SOCIOS COMERCIALES';
$lang['menu_item_responsabilidad'] = 'RESPONSABILIDAD SOCIAL';
$lang['menu_item_bolsa'] = 'BOLSA DE TRABAJO';
$lang['menu_item_contacto'] = 'CONTACTO';

return $lang;