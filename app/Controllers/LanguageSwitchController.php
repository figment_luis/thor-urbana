<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class LanguageSwitchController extends BaseController
{

	function switchLang($language = "") {
        $session = session();
        
        $language = ($language != "") ? $language : "english";
        $locale = ($language == "english") ? 'en' : 'es';
       
        $session->set('site_lang', $language);
        $session->set('lang', $locale);

        return redirect('home');
    }

    function switchLangSite($language = "") {
        $session = session();
        
        $language = ($language != "") ? $language : "english";
        $locale = ($language == "english") ? 'en' : 'es';
        
        $session->set('site_lang', $language);
        $session->set('lang', $locale);
        
        return redirect()->back();
    }

}
