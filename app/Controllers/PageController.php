<?php

namespace App\Controllers;

use App\Models\HeaderModel;
use App\Models\NewsModel;
use App\Models\CategoryModel;
use App\Models\ProjectModel;
use App\Models\LocationModel;
use App\Models\PartnerModel;
use App\Models\SearchModel;
use App\Models\PopupModel;
use App\Models\EmailModel;
use App\Models\DenouncementModel;
use App\Models\SeoModel;

class PageController extends BaseController
{
	private $seo;

	public function __construct() 
	{
		$this->seo = new SeoModel('website');
	}

	public function index()
	{
		$metatags = $this->seo->render(
			'Thor Urbana',
			'Thor Urbana es una de las empresas de desarrollo e inversión inmobiliaria líderes en México.',
			base_url(),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/index', compact('metatags'));
	}

	public function home() 
	{
		$session = session();

		$headerModel 	= new HeaderModel();
		$newsModel 		= new NewsModel();
		$categoryModel 	= new CategoryModel();
		$popupModel 	= new PopupModel();
		
		$headers 		= $headerModel->getHeaders();
		$news 			= $newsModel->getLastNews();
		$categories 	= $categoryModel->getCategories();
		$popup 			= $popupModel->getActivePopup();
		$typeBrowser 	= $this->typeBrowser();

		$metatags = $this->seo->render(
			'Thor Urbana - Home',
			'Thor Urbana se especializa en la búsqueda, adquisición, desarrollo, comercialización, operacióny  administración de múltiples proyectos inmobiliarios.',
			base_url('home'),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/home', compact('headers', 'news', 'categories', 'popup', 'typeBrowser', 'metatags'));
	}

	public function historia() 
	{
		$session = session();

		$headerModel = new HeaderModel();
		$categoryModel = new CategoryModel();

		$headers = $headerModel->getHeaders();
		$categories = $categoryModel->getCategories();
		$typeBrowser = $this->typeBrowser();

		$metatags = $this->seo->render(
			'Thor Urbana - Historia',
			'Empresa mexicana líder en desarrollo e inversión inmobiliario, se caracteriza por una visión fresca e innovadora en cada uno de sus proyectos.',
			base_url('historia'),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/history', compact('headers', 'categories', 'typeBrowser', 'metatags'));
	}

	public function portafolio() 
	{
		$session = session();

		$headerModel 	= new HeaderModel();
		$categoryModel 	= new CategoryModel();
		$projectModel 	= new projectModel();
		$locationModel 	= new LocationModel();

		$headers 		= $headerModel->getHeaders();
		$categories 	= $categoryModel->getCategories();
		$projects 		= $projectModel->getProjectsByCategory('centros-comerciales');
		$hotels 		= $projectModel->getProjectsByCategoryHotels();
		$offices 		= $projectModel->getProjectsByCategory('oficinas');
		$residentials 	= $projectModel->getProjectsByCategory('residencial');
		$desarrollos 	= $projectModel->getProjectsByCategory('proyectos-desarrollo');
		$locations 		= $locationModel->getLocations();
		$typeBrowser 	= $this->typeBrowser();

		$metatags = $this->seo->render(
			'Thor Urbana - Portafolio',
			'Thor Urbana cuenta con un portafolio inmobiliario de primer nivel en México y en Latinoamérica, reconocido por su innovador diseño y construcción.',
			base_url('portafolio'),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/portafolio', compact('headers', 'categories', 'projects', 'hotels', 'offices', 'residentials', 'desarrollos', 'locations', 'typeBrowser', 'metatags'));
	}

	public function noticias() 
	{
		$session = session();

		$headerModel 	= new HeaderModel();
		$categoryModel 	= new CategoryModel();
		$newsModel 		= new NewsModel();

		$headers 		= $headerModel->getHeaders();
		$categories 	= $categoryModel->getCategories();
		$news 			= $newsModel->getAllNews();
		$years 			= $newsModel->getYears();
		$typeBrowser 	= $this->typeBrowser();

		$metatags = $this->seo->render(
			'Thor Urbana - Noticias',
			'Comunicados de prensa Thor Urbana.',
			base_url('noticias'),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/noticias', compact('headers', 'categories', 'news', 'years', 'typeBrowser', 'metatags'));
	}

	public function responsabilidad() 
	{
		$session = session();

		$headerModel 	= new HeaderModel();
		$categoryModel 	= new CategoryModel();

		$headers 		= $headerModel->getHeaders();
		$categories 	= $categoryModel->getCategories();
		$typeBrowser 	= $this->typeBrowser();

		$metatags = $this->seo->render(
			'Thor Urbana - Responsabilidad Social',
			'Programa de Fundación Techo - Programa Construyendo y Creciendo.',
			base_url('responsabilidad'),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/responsabilidad', compact('headers', 'categories', 'typeBrowser', 'metatags'));
	}

	public function contacto() 
	{
		$session = session();

		$headerModel 	= new HeaderModel();
		$categoryModel 	= new CategoryModel();

		$headers 		= $headerModel->getHeaders();
		$categories 	= $categoryModel->getCategories();
		$typeBrowser 	= $this->typeBrowser();

		$metatags = $this->seo->render(
			'Thor Urbana - Contacto',
			'Visítanos directamente en nuestras oficinas corporativas ubicadas en la Ciudad de México o a través de nuestras distintas oficinas regionales.',
			base_url('contacto'),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/contacto', compact('headers', 'categories', 'typeBrowser', 'metatags'));
	}

	public function trabajo() 
	{
		$session = session();

		$headerModel 	= new HeaderModel();
		$categoryModel 	= new CategoryModel();

		$headers 		= $headerModel->getHeaders();
		$categories 	= $categoryModel->getCategories();
		$typeBrowser 	= $this->typeBrowser();

		$metatags = $this->seo->render(
			'Thor Urbana - Bolsa de Trabajo',
			'En Thor Urbana ya somos más de 185 colaboradores y continuamos creciendo, forma parte de nuestro equipo.',
			base_url('trabajo'),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/trabajo', compact('headers', 'categories', 'typeBrowser', 'metatags'));
	}

	public function leasing() 
	{
		$session = session();

		$headerModel 		= new HeaderModel();
		$categoryModel 		= new CategoryModel();
		$partnerModel 		= new PartnerModel();		

		$headers 			= $headerModel->getHeaders();
		$categories 		= $categoryModel->getCategories();
		$partners 			= $partnerModel->getPartners();
		$partner_categories = $partnerModel->getPartnersCategories();
		$typeBrowser 		= $this->typeBrowser();

		$metatags = $this->seo->render(
			'Thor Urbana - Leasing',
			'Nuestras centros comerciales cuentan con las mejores boutiques y restaurantes a nivel mundial. ',
			base_url('leasing'),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/leasing', compact('headers', 'categories', 'partners', 'partner_categories', 'typeBrowser', 'metatags'));
	}

	public function proyecto($nameProject = null) 
	{
		$session = session();
		helper('text');

		$headerModel 		= new HeaderModel();
		$categoryModel 		= new CategoryModel();
		$projectModel 		= new ProjectModel();

		$headers 			= $headerModel->getProjectHeaders($nameProject);
		$categories 		= $categoryModel->getCategories();
		$project 			= $projectModel->getProjects($nameProject);
		$typeBrowser 		= $this->typeBrowser();

		// Tomar información del proyecto desde los archivos de idioma, si el idioma seleccionado es inglés
		if ($session->get('site_lang') == 'english') {
			// Reemplazar información de base de datos (español) por la del archivo de traducción
			$project[0]->alt_name = lang('projects.' . $project[0]->slug . '_title');
			$project[0]->description = lang('projects.' . $project[0]->slug . '_description');
			$project[0]->info = lang('projects.' . $project[0]->slug . '_info');
		}
		$metatags = $this->seo->render(
			'Thor Urbana - Proyecto ' . ucwords(mb_strtolower($project[0]->alt_name)),
			character_limiter($project[0]->short_info, 130),
			base_url('proyecto/' . $project[0]->slug),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/proyecto', compact('headers', 'categories', 'project', 'typeBrowser', 'metatags'));
	}

	public function denuncias() 
	{
		$session = session();

		$headerModel 	= new HeaderModel();
		$categoryModel 	= new CategoryModel();

		$headers 		= $headerModel->getHeaders();
		$categories 	= $categoryModel->getCategories();
		$typeBrowser 	= $this->typeBrowser();

		$metatags = $this->seo->render(
			'Thor Urbana - Canal de Denuncias',
			'Canal de Denuncias Thor Urbana',
			base_url('denuncias'),
			base_url() . '/public/images/thor_equities.jpg'
		);

		return view('pages/denuncias', compact('headers', 'categories', 'typeBrowser', 'metatags'));
	}
	
	public function sendEmail() 
	{
		$name 	  = filter_var($this->request->getPost('name'), FILTER_SANITIZE_STRING);
		$email 	  = filter_var($this->request->getPost('email'), FILTER_SANITIZE_EMAIL);
		$message  = filter_var($this->request->getPost('message'), FILTER_SANITIZE_STRING);
		$interest = filter_var($this->request->getPost('interest'),FILTER_SANITIZE_STRING);
		
		$subject  = "[COMERCIALIZACIÓN] - " . ucwords(mb_strtolower($interest));

		$emailModel = new EmailModel();
		$result    	= $emailModel->send('no-reply@thorurbana.com', 'no-reply@thorurbana.com', $email, $name, $subject, $message);

		if ($result)
			$respuesta = ['status' => 'send'];	
		else
			$respuesta = ['status' => 'error'];	

		return json_encode($respuesta);
	}

	public function cvEmail() 
	{
		try {
			$rules = [
				'cv' => [
					'label'  => 'Currículum Vitae',
					'rules'  => 'uploaded[cv]|max_size[cv,2048]|mime_in[cv,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document]',
					'errors' => [
						'uploaded' => 'El archivo cargado no es válido',
						'max_size' => 'El tamaño del archivo no debe exceder los 2MB',
						'mime_in'  => 'Solo se permiten archivos PDF o DOCX'
					]
				]
			];

			$input = $this->validate($rules);

			$respuesta = [];

			if (!$input) {
				$respuesta['status'] = 'error';
				$respuesta['error']  = $this->validator->getErrors();
				return json_encode($respuesta);
			}

			$name = filter_var($this->request->getPost('name'), FILTER_SANITIZE_STRING);
			$email = filter_var($this->request->getPost('email'), FILTER_SANITIZE_EMAIL);
			$vacant = filter_var($this->request->getPost('vacant'), FILTER_SANITIZE_EMAIL);
			$cv = $this->request->getFile('cv');
			
			$uploadedCV = $this->uploadFile($cv);

			$message = "Hola mi nombre es " . ucwords(mb_strtolower($name)) . ", adjunto mi curriculum vitae, saludos.";
			$subject = "[BOLSA DE TRABAJO] - " . ucwords(mb_strtolower($name)) . " - " . ucwords(mb_strtolower($vacant));

			if ($uploadedCV) {
				$emailModel = new EmailModel();
				$result = $emailModel->send('no-reply@thorurbana.com', 'no-reply@thorurbana.com', $email, $name, $subject, $message, $uploadedCV);

				if ($result)
					$respuesta = ['status' => 'send'];	
				else
					$respuesta = ['status' => 'error', 'error' => 'Envío de correo electrónico'];	
			} else {
				$respuesta = ['status' => 'error', 'error' => 'Subir archivo al servidor'];
			}
			return json_encode($respuesta);
		} catch (\Exception $e) {
			$respuesta = ['status' => 'error', 'error' => $e->getMessage()];
			return json_encode($respuesta);
		}
	}

	private function uploadFile($file) 
	{
		try {
			if ($file->isValid() && !$file->hasMoved()) {
				$fileName = $file->getRandomName();
				$response = $file->move(WRITEPATH . 'uploads', $fileName);
				if ($response)
					return WRITEPATH . 'uploads/' . $fileName;
				return false;
			} else {
				return false;
			}
		} catch (\Exception $e) {
			throw new \Exception($e->getMessage());
		}
	}

	public function sendDenouncement() 
	{
		try {
			$rules = [
				'evidenceFiles' => [
					'label'  => 'Archivos de evidencia',
					'rules'  => 'uploaded[evidenceFiles]|max_size[evidenceFiles,16384]|mime_in[evidenceFiles,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/zip]',
					'errors' => [
						'uploaded' => 'Los archivos cargados no son válidos',
						'max_size' => 'El tamaño de los archivo no debe exceder los 16MB',
						'mime_in'  => 'Solo se permiten archivos PDF, DOCX o ZIP'
					]
				]
			];

			$input = $this->validate($rules);

			$respuesta = [];

			if (!$input) {
				$respuesta['status'] = 'error';
				$respuesta['error']  = $this->validator->getErrors();
				return json_encode($respuesta);
			}

			$typeDenouncement 		= filter_var($this->request->getPost('typeDenouncement'), FILTER_SANITIZE_NUMBER_INT);
			$name 					= filter_var($this->request->getPost('name'), FILTER_SANITIZE_STRING);
			$gender 				= filter_var($this->request->getPost('gender'), FILTER_SANITIZE_STRING);
			$email 					= filter_var($this->request->getPost('email'), FILTER_SANITIZE_EMAIL);
			$phone 					= filter_var($this->request->getPost('phone'), FILTER_SANITIZE_STRING);
			$relationDenouncement 	= filter_var($this->request->getPost('relationDenouncement'), FILTER_SANITIZE_STRING);
			$place 					= filter_var($this->request->getPost('place'), FILTER_SANITIZE_STRING);
			$specificPlace 			= filter_var($this->request->getPost('specificPlace'), FILTER_SANITIZE_STRING);
			$eventDate 				= filter_var($this->request->getPost('eventDate'), FILTER_SANITIZE_STRING);
			$eventTime 				= filter_var($this->request->getPost('eventTime'), FILTER_SANITIZE_STRING);
			$short_description 		= filter_var($this->request->getPost('short_description'), FILTER_SANITIZE_STRING);
			$specificAmbient 		= filter_var($this->request->getPost('specificAmbient'), FILTER_SANITIZE_STRING);
			$description 			= filter_var($this->request->getPost('description'), FILTER_SANITIZE_STRING);
			$aditionalPersons 		= filter_var($this->request->getPost('aditionalPersons'), FILTER_SANITIZE_STRING);
			$evidenceSources 		= filter_var($this->request->getPost('evidenceSources'), FILTER_SANITIZE_STRING);
			$involucratedPersons 	= filter_var($this->request->getPost('involucratedPersons'), FILTER_SANITIZE_STRING);
			$tracing 				= filter_var($this->request->getPost('tracing'), FILTER_SANITIZE_NUMBER_INT);
			$evidences 				= $this->request->getFiles()['evidenceFiles'];
			$message 				= '';
			$emailTracing 			= '';
			$seguimiento			= '';
			$genderCode 			= '';

			if ($specificAmbient == 'other')
				$specificAmbient = filter_var($this->request->getPost('specificAmbientOther'), FILTER_SANITIZE_STRING);
			if ($specificPlace == 'other')
				$specificPlace = filter_var($this->request->getPost('specificPlaceOther'), FILTER_SANITIZE_STRING);
			if ($relationDenouncement == 'other')
				$relationDenouncement = filter_var($this->request->getPost('relationDenouncementOther'), FILTER_SANITIZE_STRING);
			if ($typeDenouncement == 1) {
				$typeDenouncement = 'identificado';
				$message 		  = 'msg_seguimiento';
			} else {
				$typeDenouncement = 'anonimo';
				$name 	 		  = 'Anónimo';
				$message 		  = 'msg_anonimo'; 
			}
			if ($tracing == 1){
				$seguimiento = 'Si';
				if ($typeDenouncement == 0)
					$emailTracing = filter_var($this->request->getPost('anonimeEmail'), FILTER_SANITIZE_EMAIL);
			} else {
				$seguimiento = 'No';
			}

			if ($gender == 'Masculino')
				$genderCode = 'M';
			else if ($gender == 'Femenino')
				$genderCode = 'F';
			else
				$genderCode = 'X';

			$uploadedEvidences = $this->uploadEvidences($evidences);

			if (!count($uploadedEvidences)) {
				$respuesta = ['status' => 'error', 'error' => 'Subir archivos al servidor'];
				return json_encode($respuesta); 
			}

			$dataEmail = [
				'name' 			=> $name,
				'gender'   		=> $gender,
				'email'			=> $email,
				'phone'			=> $phone,
				'relation'		=> $relationDenouncement,
				'place'			=> $place,
				'specific'		=> $specificPlace,
				'date'			=> $eventDate,
				'time'			=> $eventTime,
				'short'			=> $short_description,
				'ambient'		=> $specificAmbient,
				'description' 	=> $description,
				'persons'	  	=> $aditionalPersons,
				'evidence'		=> $evidenceSources,
				'involucrated'	=> $involucratedPersons,
				'tracing'		=> $seguimiento,
				'emailtracing'	=> $emailTracing
			];

			$emailModel = new EmailModel();
			$result = $emailModel->send('no-reply@thorurbana.com', 'denuncia_anonima@thorurbana.com', $email, $name, "[DENUNCIA]", $message, $uploadedEvidences, $dataEmail);

			if (!$result) {
				$respuesta = ['status' => 'error', 'error' => 'Envío de correo electrónico'];
				return json_encode($respuesta); 
			}

			$data = [
				'type' => $typeDenouncement,
				'name' => $name,
				'gender' => $genderCode,
				'email' => ($email) ? $email : $emailTracing,
				'phone' => $phone,
				'relation' => $relationDenouncement,
				'place' => $place,
				'specific_place' => $specificPlace,
				'event_date' => $eventDate,
				'event_time' => $eventTime,
				'short_description' => $short_description,
				'ambient' => $specificAmbient,
				'description' => $description,
				'aditional_persons' => $aditionalPersons,
				'involucrated_persons' => $involucratedPersons,
				'tracing' => $tracing,
			];
			
			$denouncementModel = new DenouncementModel();
			$denouncementInserted = $denouncementModel->addDenouncement($data);

			if ($denouncementInserted)
				$respuesta = ['status' => 'success'];
			else
				$respuesta = ['status' => 'error', 'error' => 'Registro en base de datos'];
				
			return json_encode($respuesta);
		} catch (\Exception $e) {
			$respuesta = [
				'status' => 'error',
				'error'  => $e->getMessage()
			];
			return json_encode($respuesta);
		}
	}

	private function uploadEvidences($files) 
	{
		try {
			$filePaths = array();
			foreach ($files as $file) {
				if ($file->isValid() && !$file->hasMoved()) {
					$fileName = $file->getRandomName();
					$response = $file->move(WRITEPATH . 'uploads/evidences', $fileName);
					if ($response)
						$filePaths[] = WRITEPATH . 'uploads/evidences/' . $fileName;
					else
						return false;
				} else {
					return false;
				}
			}
			return $filePaths;
		} catch (\Exception $e) {
			throw new \Exception($e->getMessage());
		}
	}

	public function search() 
	{
		$output = '';
		$query = '';
		$query = filter_var ($this->request->getPost('query'), FILTER_SANITIZE_STRING);

		$searchModel = new SearchModel();
		$data = $searchModel->getProjectsInfo($query);

		$response = [
			'projects' => $data
		];

		return json_encode($response);
	}

	public function typeBrowser() 
	{
		$agent = $this->request->getUserAgent();
		if ($agent->isMobile()) 
			return 0;
		else
			return 1;
	}
}
