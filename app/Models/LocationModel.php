<?php

namespace App\Models;

use CodeIgniter\Model;

class LocationModel extends Model
{
	public function __construct()
	{
		$this->db = db_connect();
	}

	public function getLocations() 
	{
        return $this->db->query("SELECT * FROM locations WHERE deleted_at = 0 ORDER BY name")->getResult(); 
    }
}
