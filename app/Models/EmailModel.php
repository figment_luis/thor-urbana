<?php

namespace App\Models;

use CodeIgniter\Model;

class EmailModel extends Model
{
	private $email = null;

    public function __construct() 
    {
        $this->email = \Config\Services::email();
    }

    public function send($from, $to, $email, $name, $subject, $message, $attach = null, $data = null) 
    {
        $this->prepare();

        $this->email->setFrom($from, $name);
		$this->email->setTo($to);
		$this->email->setReplyTo($email, $name);
        $this->email->setSubject($subject);
        if ($message == 'msg_seguimiento')
            $this->email->setMessage(view('email/seguimiento', $data));
        else if ($message == 'msg_anonimo')
            $this->email->setMessage(view('email/anonimo', $data));
        else
            $this->email->setMessage($message);
		if ($attach !== null) {
            if (!is_array($attach)) {
			    $this->email->attach($attach);
            } else {
                foreach ($attach as $currentAttach) {
                    $this->email->attach($currentAttach);
                }
            }
        }
        
        $emailSended = $this->email->send();
        return $emailSended;
        // dd($this->email->printDebugger());
    }

    private function prepare() 
    {
        // Protocol
		$config['protocol'] = 'smtp';
		// Host
		$config['SMTPHost'] = 'smtp.mailtrap.io'; //'smtp.office365.com';
		// Port
		$config['SMTPPort'] = 2525;
		// User
		$config['SMTPUser'] = '7033bc6fb0e59c'; //'no-reply@thorurbana.com';
		// Pass
		$config['SMTPPass'] = 'e32a2476319d67'; //'Qoj96350';
        // Support HTML
        $config['mailType'] = 'html';

        $this->email->initialize($config);
    }
}
