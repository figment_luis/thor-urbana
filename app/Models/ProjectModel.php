<?php

namespace App\Models;

use CodeIgniter\Model;

class ProjectModel extends Model
{
	public function __construct()
	{
		$this->db = db_connect();
	}

	public function getProjects($proyecto=null)
	{
        if($proyecto){
            return $this->db->query("SELECT p.*, c.cat_id, sc.url_facebook, sc.url_instagram, sc.url_twitter FROM projects as p LEFT JOIN social_media as sc ON p.id = sc.project_id LEFT JOIN projects_has_categories as c ON p.id = c.project_id WHERE p.slug = '$proyecto' AND p.deleted_at >= 0")->getResult();
        } else {
            return $this->db->query("SELECT * FROM projects where deleted_at >= 0")->getResult();
        }
        
    }

    public function getProjectsByCategory($category)
	{
        if (session()->get('site_lang') == 'english'):
            return $this->db->query("SELECT p.* FROM projects_en as p LEFT JOIN projects_has_categories as phc on p.id = phc.project_id LEFT JOIN categories as c ON c.id = phc.cat_id WHERE p.deleted_at = 0 and c.slug = '$category'")->getResult();  
        else:
            return $this->db->query("SELECT p.* FROM projects as p LEFT JOIN projects_has_categories as phc on p.id = phc.project_id LEFT JOIN categories as c ON c.id = phc.cat_id WHERE p.deleted_at = 0 and c.slug = '$category'")->getResult(); 
        endif;
    }

    public function getProjectsByCategoryHotels() 
	{
        if (session()->get('site_lang') == 'english'):
            return $this->db->query("SELECT p.* FROM projects_en as p LEFT JOIN projects_has_categories as phc on p.id = phc.project_id LEFT JOIN categories as c ON c.id = phc.cat_id WHERE p.deleted_at = 0 and c.slug = 'hoteles' ORDER BY FIELD(p.id, 15, 8, 9, 7)")->getResult();  
        else:
            return $this->db->query("SELECT p.* FROM projects as p LEFT JOIN projects_has_categories as phc on p.id = phc.project_id LEFT JOIN categories as c ON c.id = phc.cat_id WHERE p.deleted_at = 0 and c.slug = 'hoteles' ORDER BY FIELD(p.id, 15, 8, 9, 7)")->getResult(); 
        endif;
    }
}
