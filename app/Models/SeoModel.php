<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Libraries\CoffeeCode\Optimizer\Optimizer;

class SeoModel extends Model
{
	private $optimizer;

	public function __construct(string $schema = 'article') 
	{
		$this->optimizer = new Optimizer();
		$this->optimizer->openGraph('Thor Urbana', 'es-MX', $schema)
						->publisher('thorurbana', '1424705011157963')
						->twitterCard('@ThorUrbana', '@ThorUrbana', 'thorurbana.com');
	}

	public function render(string $title, string $description, string $url, string $image, bool $follow = true): string
	{
		$seo = $this->optimizer->optimize($title, $description, $url, $image, $follow);
		return $seo->render();
	}
}
