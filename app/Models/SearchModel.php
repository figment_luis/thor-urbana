<?php

namespace App\Models;

use CodeIgniter\Model;

class SearchModel extends Model
{
	public function __construct() 
	{
		$this->db = db_connect();
	}

	public function getProjectsInfo($search=null) 
	{
        // Seleccionar la tabla de proyectos correspondiente al idioma seleccionado
        $tableProjects = 'projects';

        if (session()->get('site_lang') == 'english')
            $tableProjects = 'projects_en';

        if ($search) {
            return $this->db->query("SELECT p.name, p.slug, p.short_info, p.location, c.slug as category FROM $tableProjects as p LEFT JOIN projects_has_categories as phc on p.id=phc.project_id LEFT JOIN categories as c on phc.cat_id=c.id  WHERE p.deleted_at = 0 and (p.name LIKE '%$search%' OR p.keywords LIKE '%$search%') 
                                     UNION 
                                     SELECT date, url, title, type, type FROM news WHERE deleted_at = 0 and (date LIKE '%$search%' OR title LIKE '%$search%')
            ")->getResult();
        } else {
            return $this->db->query("SELECT p.name, p.slug, p.short_info, p.location, c.slug as category FROM $tableProjects as p LEFT JOIN projects_has_categories as phc on p.id=phc.project_id LEFT JOIN categories as c on phc.cat_id=c.id WHERE p.deleted_at = 0 and c.deleted_at = 0
                                     UNION 
                                     SELECT date, url, title, type, type FROM news WHERE deleted_at = 0
            ")->getResult();
        }
    }
}
