<?php

namespace App\Models;

use CodeIgniter\Model;

class PopupModel extends Model
{
	public function __construct() 
	{
		$this->db = db_connect();
	}

	public function getActivePopup() 
	{
        return $this->db->query("SELECT * FROM popups WHERE enabled = 1")->getRow(); 
    }
}
