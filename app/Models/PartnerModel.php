<?php

namespace App\Models;

use CodeIgniter\Model;

class PartnerModel extends Model
{
	public function __construct() 
	{
		$this->db = db_connect();
	}

	public function getPartnersCategories() 
	{
        return $this->db->query("SELECT * FROM partners_categories WHERE deleted_at = 0")->getResult(); 
    }

    public function getPartners() 
	{
        return $this->db->query("SELECT p.*, c.slug FROM partners as p LEFT JOIN partners_has_categories as phc ON phc.partner_id = p.id LEFT JOIN partners_categories as c on c.id=phc.cat_id WHERE p.deleted_at = 0 ORDER BY p.name")->getResult(); 
    }
}
