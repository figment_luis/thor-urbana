<?php

namespace App\Models;

use CodeIgniter\Model;

class NewsModel extends Model
{
    public function __construct() {
        $this->db = db_connect();
    }

	public function getLastNews(){
        return $this->db->query("SELECT * FROM news WHERE deleted_at = 0 ORDER BY id DESC LIMIT 16")->getResult(); 
    }

    public function getAllNews(){
        return $this->db->query("SELECT * FROM news WHERE deleted_at = 0 ORDER BY id DESC")->getResult(); 
    }

    public function getYears(){
        return $this->db->query("SELECT DISTINCT year FROM news WHERE deleted_at = 0 order by year")->getResult(); 
    }
}
