<?php

namespace App\Models;

use CodeIgniter\Model;

class CategoryModel extends Model
{
    public function __construct() {
        $this->db = db_connect();
    }

	public function getCategories(){
        return $this->db->query("SELECT * FROM categories WHERE deleted_at = 0")->getResult(); 
    }
}
