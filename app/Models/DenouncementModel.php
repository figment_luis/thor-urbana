<?php

namespace App\Models;

use CodeIgniter\Model;

class DenouncementModel extends Model
{
	public function __construct() 
	{
		$this->db = db_connect();
	}

	public function addDenouncement($data) 
	{
        return $this->db->table('denouncements')->insert($data);
    }
}
