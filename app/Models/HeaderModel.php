<?php

namespace App\Models;

use CodeIgniter\Model;

class HeaderModel extends Model
{
    public function __construct() {
        $this->db = db_connect();
    }

	public function getHeaders(){
        
        return $this->db->query("SELECT * FROM headers where section = 1 ORDER BY RAND ( ) LIMIT 10")->getResult(); 
    }

    public function getProjectHeaders($project){
        return $this->db->query("SELECT * FROM headers WHERE slug = '$project' AND section = 4 ORDER BY RAND ( ) LIMIT 8")->getResult(); 
    }

    public function getHeadersBySection($section){
        return $this->db->query("SELECT * FROM headers where section = $section ORDER BY RAND ( ) LIMIT 10")->getResult(); 
    }
}
