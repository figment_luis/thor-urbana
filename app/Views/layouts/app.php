<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MS73NGV');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= $this->renderSection('seo') ?>
    <meta name="keywords" content="Thor, Urbana, thorurbana, inmobiliaria, desarrollo, inversión, lifestyle centers, proyectos de usos mixtos, hoteles de lujo, reposicionamiento, parques industriales, proyectos inmobiliarios, bienes raices, plazas comerciales, centros comerciales, Altavista 147, Town Square Metepec, Calle Corazón, The Harbor Mérida, Marina Puerto Cancún, Landmark Guadalajara">
    <!-- No Cache -->
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 2001 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>/public/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>/public/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>/public/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/public/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>/public/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>/public/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>/public/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>/public/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>/public/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url() ?>/public/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>/public/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>/public/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>/public/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?= base_url() ?>/public/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url() ?>/public/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="author" type="text/plain" href="<?= base_url("humans.txt");?>" />
    <link rel="sitemap" type="application/xml" title="Sitemap" href="<?= base_url('sitemap.xml') ?>">
    <link rel="image_src" href="<?= base_url("public/images/thor_equities.jpg")?>">
    <link rel="stylesheet" href="<?= base_url("public/css/normalize.min.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/bootstrap.min.css");?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url("public/libs/slick/slick.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/libs/slick/slick-theme.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/animate.min.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/lunar.min.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/style.min.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/responsive.min.css");?>">
    <?= $this->renderSection('css') ?>
  </head>
  <body id="page-content">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS73NGV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?= $this->include('partials/navbar') ?>
    <?= $this->include('partials/hero') ?>
    <?= $this->renderSection('content') ?>
    <?php if (session()->get('site_lang') == 'english'): ?>
      <div class="footer-var mt-5 lazy" style="background-image: url(/public/images/bar2_en.jpg)"></div>
    <?php else: ?>
      <div class="footer-var mt-5 lazy" style="background-image: url(/public/images/bar.jpg)"></div>
    <?php endif; ?>
    <footer>    
      <div class="flex-container">
        <div>
          <?php if (session()->get('site_lang') == 'english'): ?>
            <p><a href="<?=base_url('denuncias') ?>" title="<?=base_url('denuncias') ?>">Ethics Hotline</a></p>
            <p><a href="mailto:denuncia_anonima@thorurbana.com" title="denuncia_anonima@thorurbana.com">denuncia_anonima@thorurbana.com</a></p>
          <?php else: ?>
            <p><a href="<?=base_url('denuncias') ?>" title="<?=base_url('denuncias') ?>">Canal de denuncias</a></p>
            <p><a href="mailto:denuncia_anonima@thorurbana.com" title="denuncia_anonima@thorurbana.com">denuncia_anonima@thorurbana.com</a></p>
          <?php endif; ?>
        </div>
        <div>
          <p>REFORMA 2620 P16, CDMX</p>
          <p><a class="link-grey" href="tel:+525541701400" title="5541701400">+52 55 4170 14 00</a></p>
          <p><a class="link-grey" href="mailto:info@thorurbana.com" title="info@thorurbana.com">info@thorurbana.com</a></p>
          <p><a href="#" data-toggle="modal" data-target="#PopupUtilidades" title="Reparto de utilidades 2020">Reparto de utilidades 2020</a></p>
        </div>
        <div>
          <?php if (session()->get('site_lang') == 'english'): ?>
            <p><a target="_blank" href="#" data-toggle="modal" data-target="#footerVideoModal" title="Corporate video">Corporate video <i class="fa fa-play" aria-hidden="true"></i></a></p>
            <p><a target="_blank" href="<?=base_url('public/files/brochure_en.pdf') ?>" title="Digital brochure">Digital brochure   <i class="fa fa-download" aria-hidden="true"></i></a></p> 
          <?php else: ?>
            <p><a target="_blank" href="#" data-toggle="modal" data-target="#footerVideoModal" title="Video Corporativo">Video Corporativo <i class="fa fa-play" aria-hidden="true"></i></a></p>
            <p><a target="_blank" href="<?=base_url('public/files/brochure_es.pdf') ?>" title="Brochure digital">Brochure digital   <i class="fa fa-download" aria-hidden="true"></i></a></p>
          <?php endif; ?>  
        </div>
      </div>
      <div class = "footer">
        <div class="d-md-flex">
          <?php if (session()->get('site_lang') == 'english'): ?>
            <a target="_blank" href="<?=base_url('public/files/aviso-de-privacidad.pdf') ?>" title="<?=base_url('public/files/aviso-de-privacidad.pdf') ?>"><h3 class="align-self-center text-center">PRIVACY POLICY 2021</h3></a>
          <?php else: ?>
            <a target="_blank" href="<?=base_url('public/files/aviso-de-privacidad.pdf') ?>" title="<?=base_url('public/files/aviso-de-privacidad.pdf') ?>"><h3 class="align-self-center text-center" >POLÍTICA DE PRIVACIDAD 2021</h3></a>
          <?php endif; ?>
        </div>
        <div class="footer-social">
          <?php if (session()->get('site_lang') == 'english'): ?>
            <h3 class="mb-2 text-center">SOCIAL MEDIA</h3>
          <?php else: ?>
            <h3 class="mb-2 text-center">SÍGUENOS EN NUESTRAS REDES SOCIALES</h3>
          <?php endif; ?>
          <div class="footer-icons">
            <a href="https://www.facebook.com/thorurbana" target="_blank" title="https://www.facebook.com/thorurbana" rel="noreferrer noopener" rel="noreferrer noopener"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="https://twitter.com/thorurbana" target="_blank" title="https://twitter.com/thorurbana" rel="noreferrer noopener" rel="noreferrer noopener"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="https://www.instagram.com/thorurbana/" target="_blank" title="ttps://www.instagram.com/thorurbana/" rel="noreferrer noopener" rel="noreferrer noopener"><i class="fa fa-instagram" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
    </footer>
    <div class="modal fade" id="footerVideoModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog  modal-dialog-centered " role="document">
        <div class="modal-content">
          <iframe src="https://www.youtube.com/embed/fCjNtQetQHw" loading="lazy" srcdoc="<style>*{padding:0;margin:0;overflow:hidden}html,body{height:100%}img,span{position:absolute;width:100%;top:0;bottom:0;margin:auto}span{height:1.5em;text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}</style><a href=https://www.youtube.com/embed/fCjNtQetQHw?autoplay=1><img src=https://img.youtube.com/vi/fCjNtQetQHw/hqdefault.jpg alt='Thor Urbana Video Corporativo 2018'><span>▶</span></a>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
    <!-- MODAL -->
    <div class="modal-popup modal fade" id="PopupUtilidades" tabindex="-1" role="dialog" aria-labelledby="PopupUtilidadesTitle"
    aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <picture>
              <source srcset="<?= base_url('public/images/popups/mobile-popup.jpg') ?>" media="(max-width: 599px)">
              <source srcset="<?= base_url('public/images/popups/desktop-popup.jpg') ?>" media="(min-width: 600px)">
              <img src="<?= base_url('public/images/popups/desktop-popup.jpg') ?>" alt="Thor Urbana - Reparto de Utilidades 2020" title="Reparto de Utilidades 2020" loading="lazy">
            </picture>
          </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url("public/js/jquery-3.5.1.min.js");?>"></script>
    <script src="<?= base_url("public/js/bootstrap.bundle.min.js");?>"></script>
    <script src="<?= base_url("public/js/jquery.slidereveal.min.js");?>"></script>
    <script src="<?= base_url("public/js/fullclip.min.js");?>"></script>
    <script src="<?= base_url("public/js/lunar.min.js");?>"></script>
    <script src="<?= base_url("public/libs/slick/slick.min.js");?>"></script>
    <script src="<?= base_url("public/js/app.min.js");?>"></script>
    <script>
    const URL = '<?= base_url() ?>'
    </script>
    <?= $this->renderSection('js') ?>
  </body>
</html>
