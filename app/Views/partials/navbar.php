<nav class="navbar sticky-top navbar-expand-lg navbar-light border-bottom">
  <div class="toggle-menu" id='trigger'>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <header style="display: none;">
    <h1>Thor Urbana</h1>
  </header>
  <a href="<?= base_url('home');?>" title="<?= base_url('home');?>"><img src="<?= base_url("public/images/logo.png");?>" alt="Logotipo Thor Urbana" title="Thor Urbana" loading="lazy"></a>
  <div class="language">
    <a href="<?= base_url('switchLangSite/spanish') ?>" class="language__link <?= (session()->get('site_lang') == 'spanish') ? 'active' : '' ?>" title="<?= base_url('switchLangSite/spanish') ?>">ES</a> / <a href="<?= base_url('switchLangSite/english') ?>" class="language__link <?= (session()->get('site_lang') == 'english') ? 'active' : '' ?>" title="<?= base_url('switchLangSite/english') ?>">EN
    </a>
  </div>
  <i class="fa fa-search" aria-hidden="true" style="font-size: 20px" data-toggle="modal" data-target="#fullscreen-search"></i>
</nav>


<!-- Modal -->
<div class="modal fade" id="fullscreen-search" tabindex="-1" role="dialog" aria-labelledby="fullscreen-searchLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <i class="close fa fa-times" data-dismiss="modal" aria-label="Close" aria-hidden="true"></i>
        </div>
      <div class="modal-body">
        <div class="container">
          <h2 class="search-title section-title text-center mb-3">Thor Urbana Proyectos</h2>
          <div class="input-group">
              <input class="form-control py-2 border-right-0 border" type="search" value="" id="search-input">
              <span class="input-group-append">
                  <div class="input-group-text bg-transparent"><i class="fa fa-search"></i></div>
              </span>
          </div>

          <div class="filters mx-auto text-center my-3">
                <?php if (session()->get('site_lang') == 'english'): ?>
                  <span id="filter-button">FILTERS <i class="fa fa-arrow-down" aria-hidden="true"></i></span>
                <?php else: ?>
                  <span id="filter-button">FILTROS <i class="fa fa-arrow-down" aria-hidden="true"></i></span>
                <?php endif; ?>

                <div id="filter-list" class="initiallyHidden">
                  <div class="row mt-3">
                    <?php if (session()->get('site_lang') == 'english'): ?>
                      <?php
                        $count = 0;
                        // El orden de los elementos en el arreglo debe coincidir con el orden de los registros en la tabla de la base de datos
                        $myCategories = ['LIFESTYLE RETAIL CENTERS', 'OFFICE', 'RESIDENTIAL', 'HOTELS', 'NEWS', 'NEW DEVELOPMENTS'];
                        foreach ($categories as $category): 
                      ?>
                        <div category="<?= $category->slug;?>" class="filter-item col-sm-12 col-md-4 my-3 section-title">
                          <h4><?= strtoupper($myCategories[$count++]);?></h4>
                        </div>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <?php foreach ($categories as $category): ?>
                        <div category="<?= $category->slug;?>" class="filter-item col-sm-12 col-md-4 my-3 section-title">
                          <h4><?= strtoupper($category->name);?></h4>
                        </div>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </div>
                  <span id="clean-filter" class="text-center">Limpiar Filtros</span> 
                </div>
          </div>
          <h2 class="mt-5 mb-3">Resultados de búsqueda</h2>
          <div id="search_result" class="result-search container mt-3"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php if (session()->get('site_lang') == 'english'): ?>
  <nav id='sidebar'> 
    <div class="sidebar-container">
        <span id="sidebar-close"><i class="fa fa-times" aria-hidden="true"></i></span>
        <img class="img-fluid mb-4" src="<?= base_url("public/images/logo-tagline.jpg");?>" alt="Logotipo Thor Urbana" title="Thor Urbana" loading="lazy">
        <a id="nav-history" href="<?= base_url("/historia");?>" class="dropdown-sidebar <?=(strpos( current_url(), '/historia') !== false ) ? 'active': '' ?>" title="<?= base_url("/historia");?>">ABOUT US</a>
          <div class="mb-3 ml-4 d-none animate__animated animate__fadeInDown">
            <a class="menu-option" data-scroll="mision" href="<?= base_url("/historia#mision");?>" title="<?= base_url("/historia#mision");?>">MISSION & VISION</a>
            <a class="menu-option" data-scroll="historia" href="<?= base_url("/historia#historia");?>" title="<?= base_url("/historia#historia");?>">HISTORY</a>
            <a class="menu-option" data-scroll="estrategia" href="<?= base_url("/historia#estrategia");?>" title="<?= base_url("/historia#estrategia");?>">STRATEGY</a>
            <a class="menu-option" data-scroll="socios-estrategicos" href="<?= base_url("/historia#socios-estrategicos");?>" title="<?= base_url("/historia#socios-estrategicos");?>">STRATEGIC PARTNERS</a>
          </div>
        <a id="nav-briefcase" href="<?= base_url("/portafolio");?>" class="dropdown-sidebar <?=(strpos( current_url(), '/portafolio') !== false ) ? 'active': ''?>" title="<?= base_url("/portafolio");?>">OUR PORTFOLIO</a>
          <div class="mb-3 ml-4 d-none animate__animated animate__fadeInDown">
            <a class="menu-option" data-scroll="centros-comerciales" href="<?= base_url("/portafolio#centros-comerciales");?>" title="<?= base_url("/portafolio#centros-comerciales");?>">LIFESTYLE RETAIL CENTERS</a>
            <a class="menu-option" data-scroll="hoteles" href="<?= base_url("/portafolio#hoteles");?>" title="<?= base_url("/portafolio#hoteles");?>">HOTELS</a>
            <a class="menu-option" data-scroll="oficinas" href="<?= base_url("/portafolio#oficinas");?>" title="<?= base_url("/portafolio#oficinas");?>">OFFICE</a>
            <a class="menu-option" data-scroll="residencial" href="<?= base_url("/portafolio#residencial");?>" title="<?= base_url("/portafolio#residencial");?>">RESIDENTIAL (MIXED-USED)</a>
            <a class="menu-option" data-scroll="proyectos-desarrollo" href="<?= base_url("/portafolio#proyectos-desarrollo");?>" title="<?= base_url("/portafolio#proyectos-desarrollo");?>">NEW DEVELOPMENTS</a>
          </div>
          <a id="nav-leasing" href="<?= base_url("/leasing");?>" class="dropdown-sidebar <?=(strpos( current_url(), '/leasing') !== false ) ? 'active': '' ?>" title="<?= base_url("/leasing");?>">LEASING</a>
          <div class="mb-3 ml-4 d-none animate__animated animate__fadeInDown">
            <a class="menu-option" data-scroll="comerciales" href="<?= base_url("/leasing#comerciales");?>" title="<?= base_url("/leasing#comerciales");?>">BRAND PARTNERSHIPS</a>
          </div>  
          <a href="<?= base_url("/responsabilidad");?>" class="<?=(current_url()==base_url('/responsabilidad')) ? 'active':''?>" title="<?= base_url("/responsabilidad");?>">SOCIAL IMPACT</a>
          <a href="<?= base_url("/trabajo");?>" class="<?=(current_url()==base_url('/trabajo')) ? 'active':''?>" title="<?= base_url("/trabajo");?>">CAREERS</a>
          <a href="<?= base_url("/contacto");?>" class="<?=(current_url()==base_url('/contacto')) ? 'active':''?>" title="<?= base_url("/contacto");?>">CONTACT</a>
    </div>
  </nav>
<?php else: ?>
  <nav id='sidebar'> 
    <div class="sidebar-container">
        <span id="sidebar-close"><i class="fa fa-times" aria-hidden="true"></i></span>
        <img class="img-fluid mb-4" src="<?= base_url("public/images/logo-tagline.jpg");?>" alt="Logotipo Thor Urbana" title="Thor Urbana" loading="lazy">
        <a id="nav-history" href="<?= base_url("/historia");?>" class="dropdown-sidebar <?=(strpos( current_url(), '/historia') !== false ) ? 'active': '' ?>" title="<?= base_url("/historia");?>">QUIÉNES SOMOS</a>
          <div class="mb-3 ml-4 d-none animate__animated animate__fadeInDown">
            <a class="menu-option" data-scroll="mision" href="<?= base_url("/historia#mision");?>" title="<?= base_url("/historia#mision");?>">MISIÓN/VISIÓN</a>
            <a class="menu-option" data-scroll="historia" href="<?= base_url("/historia#historia");?>" title="<?= base_url("/historia#historia");?>">HISTORIA</a>
            <a class="menu-option" data-scroll="estrategia" href="<?= base_url("/historia#estrategia");?>" title="<?= base_url("/historia#estrategia");?>">ESTRATEGIA</a>
            <a class="menu-option" data-scroll="socios-estrategicos" href="<?= base_url("/historia#socios-estrategicos");?>" title="<?= base_url("/historia#socios-estrategicos");?>">SOCIOS ESTRATÉGICOS</a>
          </div>
        <a id="nav-briefcase" href="<?= base_url("/portafolio");?>" class="dropdown-sidebar <?=(strpos( current_url(), '/portafolio') !== false ) ? 'active': ''?>" title="<?= base_url("/portafolio");?>">PORTAFOLIO</a>
          <div class="mb-3 ml-4 d-none animate__animated animate__fadeInDown">
            <a class="menu-option" data-scroll="centros-comerciales" href="<?= base_url("/portafolio#centros-comerciales");?>" title="<?= base_url("/portafolio#centros-comerciales");?>">LIFESTYLE CENTERS</a>
            <a class="menu-option" data-scroll="hoteles" href="<?= base_url("/portafolio#hoteles");?>" title="<?= base_url("/portafolio#hoteles");?>">HOTELES</a>
            <a class="menu-option" data-scroll="oficinas" href="<?= base_url("/portafolio#oficinas");?>" title="<?= base_url("/portafolio#oficinas");?>">OFICINAS</a>
            <a class="menu-option" data-scroll="residencial" href="<?= base_url("/portafolio#residencial");?>" title="<?= base_url("/portafolio#residencial");?>">RESIDENCIAL</a>
            <a class="menu-option" data-scroll="proyectos-desarrollo" href="<?= base_url("/portafolio#proyectos-desarrollo");?>" title="<?= base_url("/portafolio#proyectos-desarrollo");?>">PROYECTOS EN DESARROLLO</a>
          </div>
        <a id="nav-leasing" href="<?= base_url("/leasing");?>" class="dropdown-sidebar <?=(strpos( current_url(), '/leasing') !== false ) ? 'active': '' ?>" title="<?= base_url("/leasing");?>">LEASING</a>
          <div class="mb-3 ml-4 d-none animate__animated animate__fadeInDown">
            <a class="menu-option" data-scroll="comerciales" href="<?= base_url("/leasing#comerciales");?>" title="<?= base_url("/leasing#comerciales");?>">SOCIOS COMERCIALES</a>
          </div>
          <a href="<?= base_url("/noticias");?>" class="<?=(current_url()==base_url('/noticias')) ? 'active':''?>" title="<?= base_url("/noticias");?>">PRENSA</a>
          <a href="<?= base_url("/responsabilidad");?>" class="<?=(current_url()==base_url('/responsabilidad')) ? 'active':''?>" title="<?= base_url("/responsabilidad");?>">RESPONSABILIDAD SOCIAL</a>
          <a href="<?= base_url("/trabajo");?>" class="<?=(current_url()==base_url('/trabajo')) ? 'active':''?>" title="<?= base_url("/trabajo");?>">BOLSA DE TRABAJO</a>
          <a href="<?= base_url("/contacto");?>" class="<?=(current_url()==base_url('/contacto')) ? 'active':''?>" title="<?= base_url("/contacto");?>">CONTACTO</a>
    </div>
  </nav>
<?php endif; ?>