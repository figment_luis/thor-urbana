<?php if (isset($popup) && $popup != null && is_object($popup)): ?>
    <div id="popup_desktop" class="popup" style="display: none;">
      <?php if($popup->link): ?>
        <a href="<?php echo $popup->link; ?>" class="popup__link" target="<?php echo $popup->target ?>" title="<?php echo $popup->link; ?>">
          <img src="<?php echo base_url($popup->image_desktop)."?v=".rand(); ?>" class="popup__image" alt="Thor Urbana - <?= $popup->title ?>" title="<?= $popup->title ?>" loading="lazy" />
        </a>
      <?php else: ?>
        <img src="<?php echo base_url($popup->image_desktop)."?v=".rand(); ?>" class="popup__image" alt="Thor Urbana - <?= $popup->title ?>" title="<?= $popup->title ?>" loading="lazy" />
      <?php endif; ?>
    </div>

    <div id="popup_mobile" class="popup--mobile" style="display: none;">
      <?php if($popup->link): ?>
        <a href="<?php echo $popup->link; ?>" class="popup__link--mobile" target="<?php echo $popup->target; ?>" title="<?php echo $popup->link; ?>">
          <img src="<?php echo base_url($popup->image_mobile)."?v=".rand(); ?>" class="popup__image--mobile" alt="Thor Urbana - <?= $popup->title?>" title="<?= $popup->title ?>" loading="lazy" />
        </a>
      <?php else: ?>
        <img src="<?php echo base_url($popup->image_mobile)."?v=".rand(); ?>" class="popup__image--mobile" alt="Thor Urbana - <?= $popup->title?>" title="<?= $popup->title ?>" loading="lazy" />
      <?php endif; ?>
    </div>
<?php endif; ?>