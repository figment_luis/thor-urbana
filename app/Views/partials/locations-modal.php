<div class="map-modal modal fade" id="LandmarkTijuanaModal" tabindex="-1" role="dialog"
  aria-labelledby="LandmarkTijuanaModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">THE LANDMARK, TIJUANA</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>Lifestyle Center</p>
          <p>+28,000 m2 of leasable area</p>
          <p>+14,000 m2 of AAA office space</p>
          <p>+190 Residences</p>
          <p>Hotel with +200 Rooms</p>
          <p>+2,000 Parking Spaces</p>
          <p>Opening: fall 2022</p>
          <?php else: ?>
          <p>Proyecto de usos mixtos</p>
          <p>+28,000 m2 de área rentable</p>
          <p>+14,000 m2 de oficinas clase tipo AAA</p>
          <p>+190 condominios</p>
          <p>Hotel con +200 habitaciones</p>
          <p>+2,000 cajones de estacionamiento</p>
          <p>Fecha de apertura: otoño 2022</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/landmark-tijuana" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="TheParkModal" tabindex="-1" role="dialog" aria-labelledby="TheParkModalTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">THE PARK, SAN LUIS POTOSÍ</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>Mixed-use Asset</p>
          <p>+65,000 m2 of leasable space</p>
          <p>+2,000 parking spaces</p>
          <p>Opening: summer 2022</p>
          <?php else: ?>
          <p>Proyecto de usos mixtos</p>
          <p>+65,000 m2 de área rentable</p>
          <p>+2,000 cajones de estacionamiento</p>
          <p>Fecha de apertura: verano 2022</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/the-park" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="MontageModal" tabindex="-1" role="dialog" aria-labelledby="MontageModalTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">MONTAGE, LOS CABOS</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>+122 Luxury hotel rooms</p>
          <p>+52 Luxury Residences</p>
          <p>+230 meters of frontage on the Bay of Santa Maria.</p>
          <p>Spa & Restaurant</p>
          <p>Opened: April 2019.</p>
          <?php else: ?>
          <p>+120 Habitaciones</p>
          <p>+50 Residencias de lujo</p>
          <p>+230 m lineales sobre la Bahía de Santa María</p>
          <p>Spa y restaurantes</p>
          <p>Fecha de apertura: Abril 2019</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/montage" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="canuvaModal" tabindex="-1" role="dialog" aria-labelledby="canuvaModalTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">COSTA CANUVA, NAYARIT</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>+90 Hotel Rooms</p>
          <p>14 Luxury Suites</p>
          <p>20 Luxury Residences</p>
          <p>Luxury Hotel</p>
          <p>Spa & Beach Club</p>
          <p>Opening: fall 2023</p>
          <?php else: ?>
          <p>+90 habitaciones</p>
          <p>14 suites</p>
          <p>20 residencias</p>
          <p>Hotel de Lujo</p>
          <p>Spa & Beach Club</p>
          <p>Fecha de apertura: otoño 2023</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/costa-canuva" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="LandmarkReserveModal" tabindex="-1" role="dialog"
  aria-labelledby="LandmarkReserveModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">THE LANDMARK RESERVE</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>+230 Residences</p>
          <p>Gym, Pool & Spa</p>
          <p>Sky garden</p>
          <p>Teen lounge</p>
          <p>Yoga lounge</p>
          <p>Sports Bar</p>
          <p>Library</p>
          <p>Opening: fall 2024</p>
          <?php else: ?>
          <p>+230 condominios</p>
          <p>Alberca, Gimnasio, Spa</p>
          <p>Sky garden</p>
          <p>Teen lounge</p>
          <p>Yoga lounge</p>
          <p>Sports Bar</p>
          <p>Biblioteca</p>
          <p>Fecha de apertura: otoño 2024</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/landmark-reserve" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="TheLandmarkGdl" tabindex="-1" role="dialog" aria-labelledby="TheLandmarkGdlTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">THE LANDMARK, GUADALAJARA</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>Luxury-Lifestyle Retail Center</p>
          <p>+25,000 M2 GLA of leasable space</p>
          <p>+250 M frontage on Puerta de Hierro and Av. Patria</p>
          <p>+65 retail locations</p>
          <p>+700 parking spaces</p>
          <p>Opened: November 2018</p>
          <?php else: ?>
          <p>Proyecto de usos mixtos</p>
          <p>+25,000 m2 de área rentable</p>
          <p>+250 m de frente sobre Puerta de Hierro y Av. Patria</p>
          <p>+65 locales comerciales</p>
          <p>+700 cajones de estacionamiento</p>
          <p>Fecha de apertura: Noviembre 2018</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/the-landmark-gdl" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="townSquareModal" tabindex="-1" role="dialog"
  aria-labelledby="townSquareModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">TOWN SQUARE, METEPEC</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>Lifestyle Retail Center</p>
          <p>+65,000 m2 of leasable space</p>
          <p>+300 linear meters of frontage on Av. Ingacio Comonfort</p>
          <p>+160 retail locations</p>
          <p>+2,000 parking spaces</p>
          <p>Opened: November 2018</p>
          <?php else: ?>
          <p>Lifestyle Center</p>
          <p>+65,000 m2 de área rentable</p>
          <p>+300 m lineales de frente sobre Av. Comonfort</p>
          <p>+160 locales comerciales</p>
          <p>+2,000 cajones de estacionamiento</p>
          <p>Fecha de apertura: Noviembre 2018</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/townsquare" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="altavista147Modal" tabindex="-1" role="dialog"
  aria-labelledby="altavista147ModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">ALTAVISTA 147, CIUDAD DE MÉXICO</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>Luxury-Lifestyle Retail Center</p>
          <p>+3,000 m2 of leasable</p>
          <p>+80 linear meters on Altavista avenue</p>
          <p>+17 retail locations</p>
          <p>+110 parking spaces</p>
          <p>Acquired: January 2013</p>
          <?php else: ?>
          <p>Lifestyle Center</p>
          <p>+3,000 m2 de área rentable</p>
          <p>+80 m lineales de frente sobre Av. Altavista</p>
          <p>+ 17 locales comerciales</p>
          <p>+110 cajones de estacionamiento</p>
          <p>Fecha de adquisición: Enero 2013</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/altavista-147" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="theHarborModal" tabindex="-1" role="dialog" aria-labelledby="theHarborModalTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">THE HARBOR, MÉRIDA</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>Lifestyle Retail Center</p>
          <p>+50,000 m2 of leasable space</p>
          <p>+300 linear meters of frontage on Av. Paseo Montejo</p>
          <p>+120 retail locations</p>
          <p>+1,300 parking spaces</p>
          <p>Opened: October 2018</p>
          <?php else: ?>
          <p>Lifestyle Center</p>
          <p>+50,000 m2 de área rentable</p>
          <p>+300 m lineales sobre Av. Paseo Montejo</p>
          <p>+120 locales comerciales</p>
          <p>+1,300 cajones de estacionamiento</p>
          <p>Fecha de apertura: Octubre 2018</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/the-harbor" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="marinaModal" tabindex="-1" role="dialog" aria-labelledby="marinaModalTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">MARINA PUERTO CANCÚN, CANCÚN</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>Lifestyle Retail Center</p>
          <p>+ 45,000 m2 of leasable space</p>
          <p>+ 150 retail locations</p>
          <p>+2,000 parking spaces</p>
          <p>Marina for more than 700 boats.</p>
          <p>Acquired: August 2018.</p>
          <?php else: ?>
          <p>Lifestyle Center</p>
          <p>+45,000 m2 de área rentable</p>
          <p>Marina para +700 embarcaciones</p>
          <p>+150 locales comerciales</p>
          <p>+2,000 cajones de estacionamiento</p>
          <p>Fecha de adquisición: Agosto 2018</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/marina" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="calleCorazonModal" tabindex="-1" role="dialog"
  aria-labelledby="calleCorazonModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">CALLE CORAZÓN</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>Lifestyle Retail Center</p>
          <p>+15,000 m2 of leasable space</p>
          <p>+105 linear meters of front on 5th Avenue</p>
          <p>+30 retail locations</p>
          <p>Opened: March 2013</p>
          <?php else: ?>
          <p>Lifestyle Center</p>
          <p>+15,000 m2 de área rentable</p>
          <p>+105 m lineales de frente a la 5ta Avenida</p>
          <p>+30 locales comerciales</p>
          <p>Fecha de apertura: Septiembre 2015</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/calle-corazon" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="thompsonMainHouseModal" tabindex="-1" role="dialog"
  aria-labelledby="ThompsonMainHouseModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">THOMPSON BEACH HOUSE</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>+20 Luxury Suites</p>
          <p>Private Beach Club</p>
          <p>Pool Bar & Restaurant</p>
          <p>Opened: February 2016</p>
          <?php else: ?>
          <p>+20 suites con vista al mar</p>
          <p>Club de playa privado</p>
          <p>Pool Bar & Restaurantes</p>
          <p>Fecha de apertura: Febrero 2016</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/thompson-beach-house"
              class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="thompsonModal" tabindex="-1" role="dialog" aria-labelledby="thompsonModalTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">THOMPSON MAIN HOUSE</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>+90 Luxury hotel rooms</p>
          <p>+3,000 m2 of rooftop bar</p>
          <p>Alessia Dayclub</p>
          <p>Catch Restaurant</p>
          <p>Opened: November 2015.</p>
          <?php else: ?>
          <p>+90 habitaciones de lujo</p>
          <p>+3,000 m2 de rooftop bar</p>
          <p>Alessia Dayclub</p>
          <p>Restaurante Catch NYC</p>
          <p>Fecha de apertura: Noviembre 2015</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/thompson-main-house" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="tulumModal" tabindex="-1" role="dialog" aria-labelledby="tulumModalTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">TULUM LIFESTYLE HOTEL & BEACH CLUB</h2>
        </div>
        <div class="map-modal-text">
          <?php if (session()->get('site_lang') == 'english'): ?>
          <p>+60 Luxury Suites</p>
          <p>+7 Hotel Villas</p>
          <p>100 meters of frontage on the beach</p>
          <p>Restaurant, Spa & Wellness Center</p>
          <p>Opening: fall 2022</p>
          <?php else: ?>
          <p>+60 suites de lujo</p>
          <p>+7 villas hoteleras</p>
          <p>100 metros lineales de frente a playa</p>
          <p>Restaurant, Spa & Wellness Center</p>
          <p>Fecha de apertura: otoño 2022</p>
          <?php endif; ?>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/tulum-beach-club" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="cayeChapelModal" tabindex="-1" role="dialog"
  aria-labelledby="cayeChapelModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <?php if (session()->get('site_lang') == 'english'): ?>
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">FOUR SEASONS PRIVATE ISLAND & RESORT CAYE CHAPEL</h2>
        </div>
        <div class="map-modal-text">
          <p>A Four Seasons Private Island</p>
          <p>Luxury Hotel & Resort</p>
          <p>100 Hotel Rooms</p>
          <p>18 Over Water Bungalows</p>
          <p>48 Four Seasons Branded Private Residences</p>
          <p>108 Residential oceanfront lots</p>
          <p>10 on island Food & Beverage offerings</p>
          <p>9-hole golf course</p>
          <p>Fabien Cousteau Nature and Conservation Center / Adventure Center</p>
          <p>Sunrise Sanctuary and Fitness Center</p>
          <p>Private Airstrip and Helipad</p>
          <p>Opening: fall 2023</p>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/caye-chapel" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
      <?php else: ?>
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">CAYE CHAPEL, BELICE</h2>
        </div>
        <div class="map-modal-text">
          <p>+100 Habitaciones de lujo</p>
          <p>+100 lotes residenciales</p>
          <p>18 Over Water Bungalows</p>
          <p>108 residencias privadas frente al mar</p>
          <p>48 residencias Four Seasons</p>
          <p>Campo de golf diseñado por Greg Norman y Lorena Ochoa</p>
          <p>Centro de Conservación y Naturaleza Fabien Casuteu</p>
          <p>Pista de aterrizaje</p>
          <p>Fecha de apertura: otoño 2023</p>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/caye-chapel" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
      <?php endif; ?>
    </div>
  </div>
</div>
<div class="map-modal modal fade" id="theRitzModal" tabindex="-1" role="dialog" aria-labelledby="theRitzModalTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <?php if (session()->get('site_lang') == 'english'): ?>
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">THE RITZ-CARLTON, MEXICO CITY</h2>
        </div>
        <div class="map-modal-text">
          <p>+150 Hotel Rooms</p>
          <p>Private Residences</p>
          <p>Club Lounge</p>
          <p>Bar & Restaurant</p>
          <p>Spa & Fitness Center</p>
          <p>Helipad and sky lounge</p>
          <p>Opening: summer 2021</p>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/the-ritz" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
      <?php else: ?>
      <div class="modal-body">
        <div class="map-modal-title">
          <h2 class="secondary-title">THE RITZ-CARLTON, CIUDAD DE MÉXICO</h2>
        </div>
        <div class="map-modal-text">
          <p>+150 Habitaciones de lujo</p>
          <p>Residencias Privadas</p>
          <p>Club-Lounge</p>
          <p>Restaurante</p>
          <p>Spa & Fitness Center</p>
          <p>Helipuerto</p>
          <p>Fecha de apertura: verano 2021</p>
          <p class="text-right mt-4">
            <a class="text-white" href="<?= base_url('proyecto') ?>/the-ritz" class="text-right" title="Más información">Más
              información</a>
          </p>
        </div>
      </div>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php if (session()->get('site_lang') != 'english'): ?>
<section class="background-gray py-5 info mt-5 pb-2 text-center last-news-section">
  <div class="container">
    <div>
      <h2 class="mb-5 section-title tituloNoticias"><?= lang('home.news_title'); ?></h2>
    </div>
    <div class="row news-slider">
      <?php foreach ($news as $new): ?>
      <div class="col-sm-12 col-md-6 col-lg-3 last-news home-news-slider">
        <a target="_blank" href="<?= $new->url;?>" title="<?= $new->title; ?>" rel="noreferrer noopener">
          <div class="news-box">
            <?php if (session()->get('site_lang') == 'english'): ?>
                <h2><?= $new->date_en;?></h2>
            <?php else: ?>
                <h2><?= $new->date;?></h2>
            <?php endif; ?>
            <p class="section-title"><?= $new->title;?></p>
            <div class="box-spaces grey down"></div>
            <div class="box-spaces grey up"></div>
          </div>
        </a>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
<?php endif; ?>