<section class="main-slider">
    <?php foreach ($headers as $header): ?>
        <?php if ($typeBrowser == 0 && $header->has_mobile==1): ?>
            <?php if (session()->get('site_lang') == 'english'): ?>
                <div class="hero lazy" style="background-image: url('<?= base_url($header->mobile_img_en); ?>'">
            <?php else: ?>
                <div class="hero lazy" style="background-image: url('<?= base_url($header->mobile_img); ?>'">
            <?php endif; ?>
        <?php else: ?>
            <?php if (session()->get('site_lang') == 'english'): ?>
                <div class="hero lazy" style="background-image: url('<?= base_url($header->image_en); ?>'">
            <?php else: ?>
                <div class="hero lazy" style="background-image: url('<?= base_url($header->image); ?>'">
            <?php endif; ?>
        <?php endif; ?>
            <div class="hero-content d-flex">
                <div class="bg-multipler"></div>
                <div class="hero-info d-flex justify-content-center align-items-end justify-content-md-between">
                    <div>
                        <?php if (session()->get('site_lang') == 'english'): ?>
                            <h2 class="text-uppercase"><?= $header->title_en; ?></h2>
                            <p class="mb-0"><?= $header->subtitle_en; ?></p>
                        <?php else: ?>
                            <h2 class="text-uppercase"><?= $header->title; ?></h2>
                            <p class="mb-0"><?= $header->subtitle; ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</section>