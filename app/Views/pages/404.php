<?php 
  header("Expires: Tue, 03 Jul 2001 01:00:00 GMT");
  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="es-MX">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MS73NGV');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Thor Urbana - Error 404</title>
    <!-- No Cache -->
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 2001 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>/public/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>/public/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>/public/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/public/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>/public/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>/public/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>/public/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>/public/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>/public/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>/public/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>/public/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>/public/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>/public/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?= base_url() ?>/public/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url() ?>/public/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="author" type="text/plain" href="<?= base_url("humans.txt");?>" />
    <link rel="sitemap" type="application/xml" title="Sitemap" href="<?= base_url('sitemap.xml') ?>">
    <link rel="image_src" href="<?= base_url("public/images/thor_equities.jpg")?>">
    <link rel="stylesheet" href="<?= base_url("public/css/bootstrap.min.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/style.css?v=".rand());?>">
    <style>
        * {
            border: 0;
            padding: 0;
            margin: 0;
            text-decoration: none;
            box-sizing: border-box;
        }
        .container {
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center
        }
        .image {
            display: block;
            margin-bottom: 1rem;
            max-width: 100%;
        }
        .message {
            color: #505050;
            font-family: var(--main-font);
            text-align: center;
            margin: 2.5rem 0 1.5rem;
        }
        .button {
            background-color: #FF8217;
            padding: 10px 30px;
            color: white;
            display: inline-block;
            text-transform: uppercase;
            width: 190px;
            height: 50px;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .button:hover {
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS73NGV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <section class="container">
        <img src="<?= base_url('public/images/favicon.png') ?>" alt="Logotipo Thor Urbana" title="Thor Urbana" class="image" loading="lazy">
        <img src="<?= base_url('public/images/logo.png') ?>" alt="Logotipo Thor Urbana" title="Thor Urbana"class="image" loading="lazy">
        <p class="message">Lo sentimos, el recurso solicitado ya no existe o posiblemente se ha movido a otra sección del sitio.</p>
        <a href="<?= base_url('home') ?>" title="<?= base_url('home') ?>" class="button">Ir al Home</a>
    </section>
</body>

</html>