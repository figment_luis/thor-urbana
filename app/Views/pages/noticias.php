<?= $this->extend('layouts/app') ?>

<?= $this->section('seo') ?>
<?= $metatags ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="py-5 info my-5 pb-2 text-center last-news-section">
    <div class="container">
        <div>
            <?php if (session()->get('site_lang') == 'english'): ?>
                <h2 class="mb-5 section-title tituloNoticias">NEWSROOM</h2>
            <?php else: ?>
                <h2 class="mb-5 section-title tituloNoticias">PRENSA</h2>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <?php if (session()->get('site_lang') == 'english'): ?>
                <label for="year-filter">Date</label>    
            <?php else: ?>
                <label for="year-filter">Año</label>    
            <?php endif; ?>                    
            <select id="year-filter">
                <?php if (session()->get('site_lang') == 'english'): ?>
                    <option value="all" selected="true">View all</option>
                <?php else: ?>
                    <option value="all" selected="true">Todos</option> 
                <?php endif; ?>  
                <?php foreach ($years as $year): ?>
                    <option value="<?= $year->year;?>"><?= $year->year;?></option>
                <?php endforeach; ?> 
            </select>  
        </div>
        <div class="row">
            <?php foreach ($news as $new): ?>
                <div class="col-sm-12 col-md-4 last-news mb-5 news-list-item" year='<?= $new->year;?>'>
                    <a target="_blank" href="<?= $new->url;?>" title="<?= $new->title; ?>" rel="noreferrer noopener">
                        <div class="news-box">
                            <?php if (session()->get('site_lang') == 'english'): ?>
                                <h2><?= $new->date_en; ?></h2>
                            <?php else: ?>
                                <h2><?= $new->date; ?></h2>
                            <?php endif; ?>
                            <p class="section-title"><?= $new->title; ?></p>
                            <div class="box-spaces white down"></div>
                            <div class="box-spaces white up"></div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?> 
        </div>
    </div>            
</section>
<?= $this->endSection() ?>