<?= $this->extend('layouts/app') ?>

<?= $this->section('seo') ?>
<?= $metatags ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="py-5 container">
    <?php
        if ($project[0]->location == 'CDMX' OR $project[0]->location == 'Los Cabos' OR $project[0]->location == 'Belice' )
            $project[0]->location = '';
    ?>
    <h2 class="section-title text-center text-uppercase">
        <?= strtoupper( ($project[0]->alt_name ? $project[0]->alt_name : $project[0]->name )); ?>
    </h2>
    <?php if (session()->get('site_lang') == 'english'): ?>
        <div class="backbutton text-right">
            <a href="<?= base_url('portafolio'); ?>" title="<?= base_url('portafolio'); ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
        </div>
    <?php else: ?>
        <div class="backbutton text-right">
            <a href="<?= base_url('portafolio'); ?>" title="<?= base_url('portafolio'); ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</a>
        </div>
    <?php endif; ?>
    <div class="my-5">
        <div class="row">
            <div class="project-sidebar col-sm-12 col-md-4">
                <?php $info = explode("|", $project[0]->info); ?>
                <?php foreach($info as $item): ?>
                    <p><?= $item ?></p>
                <?php endforeach; ?>
                <div class="project-map mb-3">
                    <a target="_blank" href="<?= $project[0]->map_url ?>" title="URL mapa <?= ucwords(mb_strtolower($project[0]->alt_name)); ?>" rel="noreferrer noopener">
                        <img class="img-fluid" src="<?= base_url('public/images/mapas/'. $project[0]->slug . '.jpg');?>" alt="Thor Urbana - Mapa <?= ucwords(mb_strtolower($project[0]->alt_name)); ?>" title="Ubicación <?= ucwords(mb_strtolower($project[0]->alt_name)); ?>" loading="lazy">
                    </a>
                </div>    
            </div>
            <div class="project-info col-sm-12 col-md-8">
                <p class="text-justify"><?=$project[0]->description; ?></p>
                <br><br><br>
                <a href="<?=$project[0]->website; ?>" target="_blank" title="<?=$project[0]->website; ?>" rel="noreferrer noopener"><?= $project[0]->display_website ? $project[0]->display_website : $project[0]->website; ?></a>
                <br><br>
                <a href="mailto:<?=$project[0]->email; ?>" target="_blank" title="mailto:<?=$project[0]->email; ?>"><?=$project[0]->email; ?></a>
                <br>
                <a href="mailto:<?=$project[0]->email_2; ?>" target="_blank" title="mailto:<?=$project[0]->email_2; ?>"><?=$project[0]->email_2; ?></a>
                <?php if ($project[0]->cat_id != 3 and $project[0]->cat_id != 7 and $project[0]->cat_id != 2): ?>
                    <?php if ($project[0]->url_facebook || $project[0]->url_instagram || $project[0]->url_twitter): ?>
                        <div class="mt-5">
                            <?php if (session()->get('site_lang') == 'english'): ?>
                                <h3 class="mb-3">Social Media</h3>
                            <?php else: ?>
                                <h3 class="mb-3">Síguenos en nuestras redes sociales</h3>
                            <?php endif; ?>
                            <div class="store-icons d-flex justify-content-start">
                                <?php if($project[0]->url_facebook):?>
                                    <a class="mr-4" href="<?= $project[0]->url_facebook?>" target="_blank" title="<?= $project[0]->url_facebook?>" rel="noreferrer noopener"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <?php endif; ?>
                                <?php if($project[0]->url_instagram):?>
                                    <a class="mr-4" href="<?= $project[0]->url_instagram?>" target="_blank" title="<?= $project[0]->url_instagram?>" rel="noreferrer noopener"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <?php endif; ?>
                                <?php if($project[0]->url_twitter):?>
                                    <a class="mr-4" href="<?= $project[0]->url_twitter?>" target="_blank" title="<?= $project[0]->url_twitter?>" rel="noreferrer noopener"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div> 
    </div>
</section>
<?= $this->endSection() ?>