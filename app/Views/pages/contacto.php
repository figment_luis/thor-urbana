<?= $this->extend('layouts/app') ?>

<?= $this->section('seo') ?>
<?= $metatags ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="container py-5">
    <h2 class="section-title text-center mb-5" style="text-transform: uppercase;"><?= lang('contact.contact_title_1'); ?></h2>
    <div class="row">
        <div class="col-md-4 offset-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <?php if (session()->get('site_lang') == 'english'): ?>
                    <h2>Mexico City</h2>
                <?php else: ?>
                    <h2>Ciudad de México</h2>
                <?php endif; ?>
                <div class="contact-list">
                    <p>Paseo de la Reforma 2620, Piso 16</p>
                    <p>11950, México D.F.</p>
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+525541701400" title="tel:+52 (55) 4170 1400">+52 (55) 4170 1400</a></p>
                    <p><b>Email: </b><br><a href="mailto:info@thorurbana.com" title="info@thorurbana.com">info@thorurbana.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <h2 class="section-title text-center my-5"><?= lang('contact.contact_title_areas'); ?></h2>
    <div class="row">
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title"><?= lang('contact.contact_areas_legal'); ?></h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+525541701400</a></p>
                    <p><b>Diego Noriega: </b><br><a href="mailto:dnogueira@thorurbana.com" title="dnogueira@thorurbana.com">dnogueira@thorurbana.com</a></p>
                </div>
            </div> 
        </div>
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title"><?= lang('contact.contact_areas_it'); ?></h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+525541701400</a></p>
                    <p><b>Ana Canedo: </b><br><a href="mailto:acanedo@thorurbana.com" title="acanedo@thorurbana.com">acanedo@thorurbana.com</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title"><?= lang('contact.contact_areas_finance'); ?></h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+525541701400</a></p>
                    <p><b>Federico Malo: </b><br><a href="mailto:fmalo@thorurbana.com" title="fmalo@thorurbana.com">fmalo@thorurbana.com</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title"><?= lang('contact.contact_areas_investment'); ?></h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+52 (55) 4170 1400</a></p>
                    <p><b>Sebastian González: </b><br><a href="mailto:sgonzalez@thorurbana.com" title="sgonzalez@thorurbana.com">sgonzalez@thorurbana.com</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title"><?= lang('contact.contact_areas_development'); ?></h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+52 (55) 4170 1400</a></p>
                    <p><b>Jorge Vargas: </b><br><a href="mailto:jvargas@thorurbana.com" title="jvargas@thorurbana.com">jvargas@thorurbana.com</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title"><?= lang('contact.contact_areas_marketing'); ?></h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+52 (55) 4170 1400</a></p>
                    <p><b>Alexis Ellstein: </b><br><a href="mailto:aellstein@thorurbana.com" title="aellstein@thorurbana.com">aellstein@thorurbana.com</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title"><?= lang('contact.contact_areas_operations'); ?></h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+52 (55) 4170 1400</a></p>
                    <p><b>Juliana Román: </b><br><a href="mailto:jroman@thorurbana.com" title="jroman@thorurbana.com">jroman@thorurbana.com</a></p>
                </div>
            </div> 
        </div>
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title"><?= lang('contact.contact_areas_leasing'); ?></h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+52 (55) 4170 1400</a></p>
                    <p><b>Victor Rivera: </b><br><a href="mailto:vrivera@thorurbana.com" title="vrivera@thorurbana.com">vrivera@thorurbana.com</a></p>
                </div>
            </div> 
        </div>
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title">Asset Management</h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+52 (55) 4170 1400</a></p>
                    <p><b>Mauricio Durante - Retail & Mixed-Use: </b><br><a href="mailto:mdurante@thorurbana.com" title="mdurante@thorurbana.com">mdurante@thorurbana.com</a></p>
                    <p><b>Antonio Gómez - Hotels: </b><br><a href="mailto:antonio.gomez@thorurbana.com" title="antonio.gomez@thorurbana.com">antonio.gomez@thorurbana.com</a></p>
                </div>
            </div> 
        </div>
    </div>
    <h2 class="section-title text-center my-5"><?= lang('contact.contact_title_3'); ?></h2>
    <div class="row">
        <div class="col-md-4 contact-info info-lg mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title">Metepec</h2>
                <div class="contact-list">
                    <p>Av. Ignacio Comonfort 1100, frente Parque Providencia, 52177.</p>
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+ (72) 2924 1220" title="tel:+7229241220">+ (72) 2924 1220</a></p>
                    <p><b>Email: </b>
                        <br><a href="mailto:contacto@townsquare.com" title="contacto@townsquare.com">contacto@townsquare.com</a>
                        <br><a href="mailto:leasing@townsquare.com" title="leasing@townsquare.com">leasing@townsquare.com</a>
                    </p>
                    <p><a href="tel:5581060290" title="tel:5581060290"><i class="fa fa-whatsapp" aria-hidden="true"></i> 55 8106 0290</a></p>
                </div>
            </div>  
        </div>
        <div class="col-md-4 contact-info info-lg mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title">Guadalajara</h2>
                <div class="contact-list">
                    <p>Av. Paseo de los Virreyes No.45 Col. Plaza Corporativa</p>
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+ (33) 9690 7780" title="tel:+3396907780">+ (33) 9690 7780</a></p>
                    <p><b>Email: </b>
                        <br><a href="mailto:contacto@landmark.com" title="contacto@landmark.com">contacto@landmark.com</a>
                        <br><a href="mailto:leasing@landmark.com" title="easing@landmark.com">leasing@landmark.com</a>
                    </p>
                    <p><a href="tel:5581060290" title="tel:5581060290"><i class="fa fa-whatsapp" aria-hidden="true"></i> 55 8106 0290</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 contact-info info-lg mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title">Mérida</h2>
                <div class="contact-list">
                    <p>97204, Prol. Paseo Montejo, 97204 Mérida, Yuc.</p>
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+ (99) 9921 1252" title="tel:+9999211252">+ (99) 9921 1252</a></p>
                    <p><b>Email: </b>
                        <br><a href="mailto:contacto@theharbormerida.com" title="contacto@theharbormerida.com">contacto@theharbormerida.com</a>
                        <br><a href="mailto:leasing@theharbormerida.com" title="leasing@theharbormerida.com">leasing@theharbormerida.com</a>
                    </p>
                    <p><a href="tel:5581060290" title="tel:5581060290"><i class="fa fa-whatsapp" aria-hidden="true"></i> 55 8106 0290</a></p>
                </div>
            </div>       
        </div>
        <div class="col-md-4 contact-info info-lg mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title">Cancún</h2>
                <div class="contact-list">
                    <p>Blvd. Kulkulcán km. 1.5, Puerto Juárez, Zona Hotelera, 77500 Cancún, Q.R.</p>
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+ (99)8313 3128" title="tel:+ 9983133128">+ (99)8313 3128</a></p>
                    <p><b>Email: </b>
                        <br><a href="mailto:contacto@marinapuertocancun.com" title="contacto@marinapuertocancun.com">contacto@marinapuertocancun.com</a>
                        <br><a href="mailto:leasing@marinapuertocancun.com" title="leasing@marinapuertocancun.com">leasing@marinapuertocancun.com</a>
                    </p>
                    <p><a href="tel:5581060290" title="tel:5581060290"><i class="fa fa-whatsapp" aria-hidden="true"></i> 55 8106 0290</a></p>
                </div>
            </div>            
        </div>
        <div class="col-md-4 contact-info info-lg mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title">Playa del Carmen</h2>
                <div class="contact-list">
                    <p>5ta Avenida entre las calles 12 y 14, Col. Centro , 77710, Playa del Carmen, Q. Roo.</p>
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+(98) 4206 4900" title="tel:+9842064900">+(98) 4206 4900</a></p>
                    <p><b>Email: </b>
                        <br><a href="mailto:contacto@callecorazon.com" title="contacto@callecorazon.com">contacto@callecorazon.com</a>
                        <br><a href="mailto:leasing@callecorazon.com" title="leasing@callecorazon.com">leasing@callecorazon.com</a>
                    </p>
                    <p><a href="tel:5581060290" title="tel:5581060290"><i class="fa fa-whatsapp" aria-hidden="true"></i> 55 8106 0290</a></p>
                </div>
            </div>     
        </div>
        <div class="col-md-4 contact-info info-lg mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title">San Luis Potosí</h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+52 (55) 4170 1400</a></p>
                    <p><b>Email: </b><br><a href="mailto:info@thorurbana.com" title="info@thorurbana.com">info@thorurbana.com</a></p>
                    <p><a href="tel:5581060290" title="tel:5581060290"><i class="fa fa-whatsapp" aria-hidden="true"></i> 55 8106 0290</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 contact-info info-lg mx-auto p-3">
            <div class="background-gray p-3">
                <h2 class="secondary-title">Tijuana</h2>
                <div class="contact-list">
                    <p><b><?= lang('contact.contact_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+52 (55) 4170 1400</a></p>
                    <p><b>Email: </b><br><a href="mailto:info@thorurbana.com" title="info@thorurbana.com">info@thorurbana.com</a></p>
                    <p><a href="tel:5581060290" title="tel:5581060290"><i class="fa fa-whatsapp" aria-hidden="true"></i> 55 8106 0290</a></p>
                </div>
            </div>     
        </div>
    </div>
</section>
<?= $this->endSection() ?>