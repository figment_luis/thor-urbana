<?= $this->extend('layouts/app') ?>

<?= $this->section('seo') ?>
<?= $metatags ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container filters mx-auto text-center my-5">
    <h2 class="section-title"><?= lang('portafolio.portafolio_title'); ?></h2>
    <div class="text-container"><?= lang('portafolio.portafolio_description'); ?></div>
    <br>
    <div id="briefcase-filter">
        <h2 class="section-title"><?= lang('portafolio.portafolio_filters'); ?></h2>
        <div class="row mt-3">
            <?php
                if (session()->get('site_lang') == 'english'):
                    $count = 0;
                    $myCategories = ['LIFESTYLE RETAIL CENTERS', 'OFFICE', 'RESIDENTIAL', 'HOTELS', 'NEW DEVELOPMENTS'];
                    foreach ($categories as $category):
                        if ($category->name!="Noticias"): ?>
                            <div category="<?= $category->slug;?>" class="briefcase-filter-item col-sm-6 col-md-4 mb-1 section-title">
                                <h4 class="text-uppercase"><?= strtoupper($myCategories[$count++]);?></h4>
                            </div>
            <?php           
                        endif;
                    endforeach;
                else:
                    foreach ($categories as $category):
                        if ($category->name!="Noticias"): ?>
                            <div category="<?= $category->slug;?>" class="briefcase-filter-item col-sm-6 col-md-4 mb-1 section-title">
                                <h4 class="text-uppercase"><?= strtoupper($category->name);?></h4>
                            </div>
            <?php
                        endif;
                    endforeach;
                endif;
            ?>
        </div>
    </div>
    <div class="form-group">
        <label for="location-filter"><?= lang('portafolio.portafolio_location'); ?></label>
        <select class="form-control" id="location-filter">
            <option class="location-default" selected="true" disabled="disabled"></option>
            <?php foreach ($locations as $location): ?>
                <?php if (session()->get('site_lang') == 'english'): ?>
                    <option class="location-option">
                        <?= ($location->name_en === "CDMX" ? "Ciudad de México" : ucfirst($location->name_en)) ?>
                    </option>
                <?php else: ?>
                    <option class="location-option">
                        <?= ($location->name === "CDMX" ? "Ciudad de México" : ucfirst($location->name)) ?>
                    </option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
    </div>
    <span id="briefcase-clean-filter" class="text-center"><?= lang('portafolio.portafolio_show_all'); ?></span>
</div>
<section class="container py-5 my-5 briefcase-section" id="centros-comerciales">
    <h2 class="section-title text-center"><?= lang('portafolio.centers_title'); ?>
        <span class="section-subtitle-portafolio"><?= lang('portafolio.operation'); ?></span>
    </h2>
    <div class="flex-wrap  my-5 pd-5 briefcase-grid">
        <?php foreach ($projects as $project): ?>
        <div class="mb-5 text-center project-card briefcase-flex-grid">
            <div class="image-briefcase lazy"
                style="background-image:url('<?= base_url("public/images/portafolio/".$project->slug.".jpg")?>')">
                <a href="<?= base_url("proyecto/".$project->slug)?>" title="<?= base_url("proyecto/".$project->slug)?>">
                    <div class="info">
                        <img class="img-fluid" src="<?= base_url("public/images/portafolio/logos/".$project->slug.".png")?>" alt="Thor Urbana - Logotipo <?= ucwords(mb_strtolower($project->name)); ?>" title="<?= ucwords(mb_strtolower($project->name)); ?>" loading="lazy">
                    </div>
                </a>
            </div>
            <h2 class="mt-3 text-uppercase"><?= strtoupper($project->name);?></h2>
            <span class="mt-2"><?= ($project->location === "CDMX" ? "Ciudad de México" : ucfirst($project->location)) ?></span>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="orange-bar"></div>
</section>
<section class="container py-5 my-5  briefcase-section" id="hoteles">
    <h2 class="section-title text-center"><?= lang('portafolio.hotels_title'); ?>
        <span class="section-subtitle-portafolio"><?= lang('portafolio.operation'); ?></span>
    </h2>
    <div class="flex-wrap my-5 pd-5 briefcase-grid">
        <?php foreach ($hotels as $hotel): ?>
        <div class="mb-5 text-center project-card briefcase-flex-grid">
            <div class="image-briefcase lazy"
                style="background-image:url('<?= base_url("public/images/portafolio/".$hotel->slug.".jpg")?>')">
                <a href="<?= base_url("proyecto/".$hotel->slug)?>" title="<?= base_url("proyecto/".$hotel->slug)?>">
                    <div class="info">
                        <img class="img-fluid" src="<?= base_url("public/images/portafolio/logos/".$hotel->slug.".png")?>" alt="Thor Urbana - Logotipo <?= ucwords(mb_strtolower($hotel->name)); ?>" title="<?= ucwords(mb_strtolower($hotel->name)); ?>" loading="lazy">
                    </div>
                </a>
            </div>
            <h2 class="mt-3 text-uppercase"><?= strtoupper($hotel->name);?></h2>
            <span class="mt-2"><?= ($hotel->location === "CDMX" ? "Ciudad de México" : ucfirst($hotel->location)) ?></span>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="orange-bar"></div>
</section>
<section class="container py-5 my-5  briefcase-section" id="oficinas">
    <h2 class="section-title text-center"><?= lang('portafolio.office_title'); ?>
        <span class="section-subtitle-portafolio"><?= lang('portafolio.operation'); ?></span>
    </h2>
    <div class="flex-wrap my-5 pd-5 briefcase-grid">
        <?php foreach ($offices as $Office): ?>
        <div class="mb-5 text-center project-card briefcase-flex-grid">
            <div class="image-briefcase lazy"
                style="background-image:url('<?= base_url("public/images/portafolio/".$Office->slug.".jpg")?>')">
                <a href="<?= base_url("proyecto/".$Office->slug)?>" title="<?= base_url("proyecto/".$Office->slug)?>">
                    <div class="info">
                        <img class="img-fluid" src="<?= base_url("public/images/portafolio/logos/".$Office->slug.".png")?>" alt="Thor Urbana - Logotipo <?= ucwords(mb_strtolower($Office->name)); ?>" title="<?= ucwords(mb_strtolower($Office->name)); ?>" loading="lazy">
                    </div>
                </a>
            </div>
            <h2 class="mt-3"><?= strtoupper($Office->name);?></h2>
            <span class="mt-2"><?= ($Office->location === "CDMX" ? "Ciudad de México" : ucfirst($Office->location)) ?></span>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="orange-bar"></div>
</section>
<section class="container py-5 my-5  briefcase-section" id="residencial">
    <h2 class="section-title text-center"><?= lang('portafolio.residential_title'); ?>
        <span class="section-subtitle-portafolio"><?= lang('portafolio.operation'); ?></span>
    </h2>
    <div class="flex-wrap my-5 pd-5 briefcase-grid">
        <?php foreach ($residentials as $residential): ?>
        <div class="mb-5 text-center project-card briefcase-flex-grid">
            <div class="image-briefcase lazy"
                style="background-image:url('<?= base_url("public/images/portafolio/".$residential->slug.".jpg")?>')">
                <a href="<?= base_url("proyecto/".$residential->slug)?>" title="<?= base_url("proyecto/".$residential->slug)?>">
                    <div class="info">
                        <img class="img-fluid" src="<?= base_url("public/images/portafolio/logos/".$residential->slug.".png")?>" alt="Thor Urbana - Logotipo <?= ucwords(mb_strtolower($residential->name)); ?>" title="<?= ucwords(mb_strtolower($residential->name)); ?>" loading="lazy">
                    </div>
                </a>
            </div>
            <h2 class="mt-3 text-uppercase"><?= strtoupper($residential->name);?></h2>
            <span class="mt-2"><?= ($residential->location === "CDMX" ? "Ciudad de México" : ucfirst($residential->location)) ?></span>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="orange-bar"></div>
</section>
<section class="container py-5 my-5  briefcase-section" id="proyectos-desarrollo">
    <h2 class="section-title text-center"><?= lang('portafolio.developments_title'); ?></h2>
    <div class="flex-wrap  my-5 pd-5 briefcase-grid">
        <?php foreach ($desarrollos as $desarrollo): ?>
        <div class="mb-5 text-center project-card briefcase-flex-grid">
            <div class="image-briefcase lazy"
                style="background-image:url('<?= base_url("public/images/portafolio/".$desarrollo->slug.".jpg")?>')">
                <a href="<?= base_url("proyecto/".$desarrollo->slug)?>" title="<?= base_url("proyecto/".$desarrollo->slug)?>">
                    <div class="info">
                        <img class="img-fluid" src="<?= base_url("public/images/portafolio/logos/".$desarrollo->slug.".png")?>" alt="Thor Urbana - Logotipo <?= ucwords(mb_strtolower($desarrollo->name)); ?>" title="<?= ucwords(mb_strtolower($desarrollo->name)); ?>" loading="lazy">
                    </div>
                </a>
            </div>
            <h2 class="mt-3 text-uppercase"><?= strtoupper($desarrollo->name);?></h2>
            <?php if ($desarrollo->type): ?>
                <span class="mt-2"><?= ucfirst($desarrollo->type) ?></span>
            <?php endif; ?>
                <span class="mt-2"><?= ($desarrollo->location === "CDMX" ? "Ciudad de México" : ucfirst($desarrollo->location))  ?></span>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="orange-bar"></div>
</section>
<section id="not-found-text" class="initiallyHidden">
    <h4 class="text-center">No se han encontrado resultados con su criterio de búsqueda</h4>
</section>
<?= $this->endSection() ?>