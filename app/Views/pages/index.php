<!DOCTYPE html>
<html lang="es-MX">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MS73NGV');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= $metatags ?>
    <meta name="keywords" content="Thor, Urbana, thorurbana, inmobiliaria, desarrollo, inversión, lifestyle centers, proyectos de usos mixtos, hoteles de lujo, reposicionamiento, parques industriales, proyectos inmobiliarios, bienes raices, plazas comerciales, centros comerciales, Altavista 147, Town Square Metepec, Calle Corazón, The Harbor Mérida, Marina Puerto Cancún, Landmark Guadalajara">
    <!-- No Cache -->
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 2001 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>/public/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>/public/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>/public/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/public/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>/public/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>/public/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>/public/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>/public/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>/public/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>/public/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>/public/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>/public/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>/public/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?= base_url() ?>/public/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url() ?>/public/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="author" type="text/plain" href="<?= base_url("humans.txt");?>" />
    <link rel="sitemap" type="application/xml" title="Sitemap" href="<?= base_url('sitemap.xml') ?>">
    <link rel="image_src" href="<?= base_url("public/images/thor_equities.jpg")?>">
    <link rel="stylesheet" href="<?= base_url("public/css/normalize.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/bootstrap.min.css");?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url("public/css/animate.min.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/libs/slick/slick.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/libs/slick/slick-theme.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/lunar.min.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/style.min.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/responsive.min.css");?>">
    <link rel="stylesheet" href="<?= base_url("public/css/popup.min.css");?>">
</head>
<body id="page-content">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS73NGV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <header style="display: none;">
        <h1>Thor Urbana</h1>
        <h2>La visión distinta</h2>
    </header>
    <div class="intro-video d-flex align-content-center">
        <video autoplay muted playsinline id="mainVideo" redirectTo="<?= base_url('home'); ?>">
            <source src="<?= base_url('public/videos/video.mp4'); ?>" type="video/mp4">
        </video>
    </div>
    <section class="lenguage-section d-flex">
        <div class="language-content text-center">
            <div class="d-flex justify-content-center">
                <a class="mr-3 btn-language" data-language="spanish" href="<?= base_url('home'); ?>" title="<?= base_url('home'); ?>">Español</a>
                <a class="mr-3 btn-language" data-language="english" href="<?= base_url('home'); ?>" title="<?= base_url('home'); ?>">English</a>
            </div>
        </div>
    </section>
</body>
<script src="<?= base_url("public/js/jquery-3.5.1.min.js");?>"></script>
<script src="<?= base_url("public/js/bootstrap.bundle.min.js");?>"></script>
<script src="<?= base_url("public/js/sweetalert2.min.js");?>"></script>
<script src="<?= base_url("public/js/jquery.slidereveal.min.js");?>"></script>
<script src="<?= base_url("public/js/fullclip.min.js");?>"></script>
<script src="<?= base_url("public/js/lunar.min.js");?>"></script>
<script src="<?= base_url("public/libs/slick/slick.min.js");?>"></script>
<script src="<?= base_url("public/js/jquery.maphilight.min.js");?>"></script>
<script src="<?= base_url("public/js/app.min.js");?>"></script>
<script>
    const URL = '<?= base_url() ?>';
</script>
</html>