<?= $this->extend('layouts/app') ?>

<?= $this->section('seo') ?>
<?= $metatags ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="my-5 pt-5 text-center container" id="thor-urbana">
    <h2 class="section-title"><?= lang('history.about_title'); ?></h2>
    <p class="text-container"><?= lang('history.about_description_p1'); ?></p><br>
    <p class="text-container"><?= lang('history.about_description_p2'); ?></p>
</section>
<div class="background-gray">
    <section class="mt-5 py-5 text-center container" id="mision">
        <h2 class="section-title"><?= lang('history.mision_title'); ?></h2>
        <p class="text-container"><?= lang('history.mision_description_p1'); ?></p>
        <br>
        <p class="text-container"><?= lang('history.mision_description_p2'); ?></p>
    </section>
</div>
<section class="mb-5 py-5 timeline" id="historia">
    <div class="container">
        <h2 class="section-title text-center mb-5"><?= lang('history.history_title'); ?></h2>
        <div class="years-box">
            <div class="year">
                <h2>2012</h2>
                <ul>
                    <?= lang('history.history_2012'); ?>
                </ul>
            </div>
            <div class="year">
                <h2>2013</h2>
                <ul>
                    <?= lang('history.history_2013'); ?>
                </ul>
            </div>
            <div class="year">
                <h2>2014</h2>
                <ul>
                    <?= lang('history.history_2014'); ?>
                </ul>
            </div>
            <div class="year">
                <h2>2015</h2>
                <ul>
                    <?= lang('history.history_2015'); ?>
                </ul>
            </div>
            <div class="year">
                <h2>2016</h2>
                <ul>
                    <?= lang('history.history_2016'); ?>
                </ul>
            </div>
            <div class="year">
                <h2>2017</h2>
                <ul>
                    <?= lang('history.history_2017'); ?>
                </ul>
            </div>
            <div class="year">
                <h2>2018</h2>
                <ul>
                    <?= lang('history.history_2018'); ?>
                </ul>
            </div>
            <div class="year">
                <h2>2019</h2>
                <ul>
                    <?= lang('history.history_2019'); ?>
                </ul>
            </div>
            <div class="year">
                <h2>2020</h2>
                <ul>
                    <?= lang('history.history_2020'); ?>
                </ul>
            </div>
            <div class="year">
                <h2>2021</h2>
                <ul>
                    <?= lang('history.history_2021'); ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="estrategia">
    <section class="my-5 pt-5 text-center container">
        <h2 class="section-title"><?= lang('history.strategy_title'); ?></h2>
        <p class="estrategiaText"><?= lang('history.strategy_description_p1'); ?></p>
        <p class="text-justify"><?= lang('history.strategy_description_p2'); ?></p>
        <div class="row mb-md-5">
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5">
                <div class="strategy-box">
                    <h4 class="number">1</h4>
                    <p><?= lang('history.strategy_item_1'); ?></p>
                    <div class="box-spaces white down"></div>
                    <div class="box-spaces white up"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5">
                <div class="strategy-box">
                    <h4 class="number">2</h4>
                    <p><?= lang('history.strategy_item_2'); ?></p>
                    <div class="box-spaces white down"></div>
                    <div class="box-spaces white up"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5">
                <div class="strategy-box">
                    <h4 class="number">3</h4>
                    <p><?= lang('history.strategy_item_3'); ?></p>
                    <div class="box-spaces white down"></div>
                    <div class="box-spaces white up"></div>
                </div>
            </div>
        </div>
        <div class="row mb-md-5">
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5">
                <div class="strategy-box">
                    <h4 class="number">4</h4>
                    <p><?= lang('history.strategy_item_4'); ?></p>
                    <div class="box-spaces white down"></div>
                    <div class="box-spaces white up"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5">
                <div class="strategy-box">
                    <h4 class="number">5</h4>
                    <p><?= lang('history.strategy_item_5'); ?></p>
                    <div class="box-spaces white down"></div>
                    <div class="box-spaces white up"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5">
                <div class="strategy-box">
                    <h4 class="number">6</h4>
                    <p><?= lang('history.strategy_item_6'); ?></p>
                    <div class="box-spaces white down"></div>
                    <div class="box-spaces white up"></div>
                </div>
            </div>
        </div>
    </section>
</section>
<section id="socios-estrategicos">
    <section class="container py-5 mb-5">
        <h2 class="section-title text-center mb-5"><?= lang('history.partners_title'); ?></h2>
        <p class="mt-5 text-justify"><?= lang('history.partners_description_p1'); ?></p>
        <p class="mb-5 text-justify"><?= lang('history.partners_description_p2'); ?></p>
            <a href="https://www.gfa.com.mx/" target="_blank" title="https://www.gfa.com.mx/" rel="noreferrer noopener">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <img class="img-fluid mb-3" src="<?= base_url("public/images/gfa.jpg")?>" alt="Thor Urbana - GFA Grupo Inmobiliario" title="GFA Grupo Inmobiliario" loading="lazy">
                    </div>
                    <div class="col-sm-12 col-md-6 text-justify">
                        <p><?= lang('history.partners_item_2'); ?></p>
                    </div>
                </div>  
            </a>  
            <br><br>
            <a href="https://www.thorequities.com/" target="_blank" title="https://www.thorequities.com/" rel="noreferrer noopener">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <img class="img-fluid mb-3" src="<?= base_url("public/images/thor_equities.jpg")?>" alt="Thor Urbana - Thor Equities" title="Thor Equities" loading="lazy">
                    </div>
                    <div class="col-sm-12 col-md-6 text-justify">
                        <p><?= lang('history.partners_item_1'); ?></p>
                    </div>
                </div>
            </a>
    </section>
</section>
<section class="my-5 py-5">
    <div class="container">
        <h2 class="section-title text-center"><?= lang('history.principals_title'); ?></h2>
        <div class="row mb-md-5">
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5 principle-deco">
                <h4 class="number">1</h4>
                <p><?= lang('history.principals_item_1'); ?></p>
            </div>
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5 principle-deco">
                <h4 class="number">2</h4>
                <p><?= lang('history.principals_item_2'); ?></p>
            </div>
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5">
                <h4 class="number">3</h4>
                <p><?= lang('history.principals_item_3'); ?></p>
            </div>
        </div>
        <div class="row mb-md-5">
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5 principle-deco">
                <h4 class="number">4</h4>
                <p><?= lang('history.principals_item_4'); ?></p>
            </div>
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5 principle-deco">
                <h4 class="number">5</h4>
                <p><?= lang('history.principals_item_5'); ?></p>
            </div>
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5">
                <h4 class="number">6</h4>
                <p><?= lang('history.principals_item_6'); ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5 principle-deco">
                <h4 class="number">7</h4>
                <p><?= lang('history.principals_item_7'); ?></p>
            </div>
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5 principle-deco">
                <h4 class="number">8</h4>
                <p><?= lang('history.principals_item_8'); ?></p>
            </div>
            <div class="col-sm-12 col-md-4 text-center py-md-3 px-5">
                <h4 class="number">9</h4>
                <p><?= lang('history.principals_item_9'); ?></p>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>