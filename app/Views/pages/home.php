<?= $this->extend('layouts/app') ?>

<?= $this->section('seo') ?>
<?= $metatags ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<!-- Plantilla popups dinámicos -->
<?= $this->include('partials/popup') ?>
<section class="info my-5 pb-2 container text-justify">
  <h2 class="mb-5 section-title text-center"><?= lang('home.welcome_title'); ?></h2>
  <p class="text-container"><?= lang('home.welcome_description_p1'); ?></p><br>
  <p class="text-container"><?= lang('home.welcome_description_p2'); ?></p>
</section>
<section class="our-team py-5">
  <h2 class="section-title text-center"><?= lang('home.locations_title'); ?></h2>
  <div class="text-center d-none d-lg-block">
    <?php if (session()->get('site_lang') == 'english'): ?>
      <img class="img-fluid map-thor mx-auto" src="<?= base_url("public/images/mapa_en.jpg")?>" alt="Thor Urbaba - Nuestras Ubicaciones" title="Nuestras ubicaciones" loading="lazy" usemap="#image-map">
    <?php else: ?>
      <img class="img-fluid map-thor mx-auto" src="<?= base_url("public/images/mapa.jpg")?>" alt="Thor Urbaba - Nuestras Ubicaciones" title="Nuestras Ubicaciones" loading="lazy" usemap="#image-map">
    <?php endif; ?>
    <map name="image-map" id="map-areas">
      <area target="" alt="Landmark Tijuana" title="Landmark Tijuana" coords="142,105,28" shape="circle"
        data-toggle="modal" data-target="#LandmarkTijuanaModal">
      <area target="" alt="The Park" title="The Park" coords="142,171,29" shape="circle" data-toggle="modal"
        data-target="#TheParkModal">
      <area target="" alt="Montage" title="Montage" coords="143,239,28" shape="circle" data-toggle="modal"
        data-target="#MontageModal">
      <area target="" alt="Costa Canuva" title="Costa Canuva" coords="142,302,28" shape="circle" data-toggle="modal"
        data-target="#canuvaModal">
      <area target="" alt="Landmark Reserve" title="Landmark Reserve" coords="143,364,26" shape="circle"
        data-toggle="modal" data-target="#LandmarkReserveModal">
      <area target="" alt="The Landmark GDL" title="The Landmark GDL" coords="143,431,26" shape="circle"
        data-toggle="modal" data-target="#TheLandmarkGdl">
      <area target="" alt="Townsquare" title="Townsquare" coords="142,496,28" shape="circle" data-toggle="modal"
        data-target="#townSquareModal">
      <area target="" alt="Altavista 147" title="Altavista 147" coords="143,562,27" shape="circle" data-toggle="modal"
        data-target="#altavista147Modal">
      <area target="" alt="The harbor" title="The harbor" coords="1055,105,28" shape="circle" data-toggle="modal"
        data-target="#theHarborModal">
      <area target="" alt="Marina" title="Marina" coords="1055,171,26" shape="circle" data-toggle="modal"
        data-target="#marinaModal">
      <area target="" alt="Calle Corazon" title="Calle Corazon" coords="1055,236,27" shape="circle" data-toggle="modal"
        data-target="#calleCorazonModal">
      <area target="" alt="Thompson Main House" title="Thompson Main House" coords="1056,302,27" shape="circle"
        data-toggle="modal" data-target="#thompsonMainHouseModal">
      <area target="" alt="Thompson" title="Thompson" coords="1055,368,29" shape="circle" data-toggle="modal"
        data-target="#thompsonModal">
      <area target="" alt="Tulum" title="Tulum" coords="1053,435,28" shape="circle" data-toggle="modal"
        data-target="#tulumModal">
      <area target="" alt="Four Seasons" title="Four Seasons" coords="1055,501,26" shape="circle" data-toggle="modal"
        data-target="#cayeChapelModal">
      <area target="" alt="The Ritz" title="The Ritz" coords="1055,563,27" shape="circle" data-toggle="modal"
        data-target="#theRitzModal">
    </map>
  </div>
  <div class="d-block d-lg-none container">
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/landmark.jpg'); ?>" alt="Thor Urbana - The Landmark Tijuana" class="img-fluid" title="The Landmark Tijuana"
          data-toggle="modal" data-target="#LandmarkTijuanaModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/the_park.jpg'); ?>" alt="Thor Urbana - The Park San Luis Potosí" class="img-fluid" title="The Park San Luis Potosí"
          data-toggle="modal" data-target="#TheParkModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/montage.jpg'); ?>" alt="Thor Urbana - Montage Los Cabos" class="img-fluid" data-toggle="modal" title="Montage Los Cabos"
          data-target="#MontageModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/riviera-nayarit.jpg'); ?>" alt="Thor Urbana - Costa Canuva Nayarit" class="img-fluid" title="Costa Canuva Nayarit"
          data-toggle="modal" data-target="#canuvaModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/landmark_reserve.jpg'); ?>" alt="Thor Urbana - The Landmark Reserve" class="img-fluid" title="The Landmark Reserve"
          data-toggle="modal" data-target="#LandmarkReserveModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/landmark_guadalajara.jpg'); ?>" alt="Thor Urbana - The Landmark Guadalajara" class="img-fluid" title="The Landmark Guadalajara"
          data-toggle="modal" data-target="#TheLandmarkGdl" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/town_square.jpg'); ?>" alt="Thor Urbana - Town Square Metepec" class="img-fluid" title="own Square Metepec"
          data-toggle="modal" data-target="#townSquareModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/altavista.jpg'); ?>" alt="Thor Urbana - Altavista 147" class="img-fluid" title="Altavista 147"
          data-toggle="modal" data-target="#altavista147Modal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/the_harbor.jpg'); ?>" alt="Thor Urbana - The Harbor Mérida" class="img-fluid" title="The Harbor Mérida"
          data-toggle="modal" data-target="#theHarborModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/marina_puerto_cancun.jpg'); ?>" alt="Thor Urbana - Marina Puerto Cancún" class="img-fluid" title="Marina Puerto Cancún"
          data-toggle="modal" data-target="#marinaModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/calle_corazon.jpg'); ?>" alt="Thor Urbana - Calle Corazón" class="img-fluid" title="Calle Corazón"
          data-toggle="modal" data-target="#calleCorazonModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/thompson-main-house.jpg'); ?>" alt="Thor Urbana - Thomson Beach House" class="img-fluid" title="Thomson Beach House"
          data-toggle="modal" data-target="#thompsonMainHouseModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/thompson.jpg'); ?>" alt="Thor Urbana - Thompson Main House" class="img-fluid" title="Thompson Main House"
          data-toggle="modal" data-target="#thompsonModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/four_season.jpg'); ?>" alt="Thor Urbana - Caye Chapel, Belice" class="img-fluid" title="Caye Chapel, Belice"
          data-toggle="modal" data-target="#cayeChapelModal" loading="lazy">
      </div>
      <div class="col-sm-12 col-md-6">
        <img src="<?= base_url('public/images/map-mobile/the_ritz.jpg'); ?>" alt="Thor Urbana - The Ritz Carlton, México" class="img-fluid" data-toggle="modal" title="The Ritz Carlton, México"
          data-target="#theRitzModal" loading="lazy">
      </div>
    </div>
  </div>
</section>
<?= $this->include('partials/locations-modal') ?>
<?= $this->endSection() ?>

<?= $this->section('css') ?>
<link rel="stylesheet" href="<?= base_url("public/css/popup.min.css");?>">
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?= base_url("public/js/jquery.maphilight.min.js");?>"></script>
<!-- Librerías de terceros - Popups Dinámicos -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js" integrity="sha512-5nBm8I4981vJDHhtHCaImHU+wcnmbheiFpmE03QLuYshaimnRk15jbW6sNK4/URdyqohIUQzNLsqdZ7bB3l5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js" integrity="sha512-l7jogMOI6ZWZJEY7lREjFdQum46y2+kpp/mnbJx7O+izymO9eGjL6Y4o7cEJNBdouhVHpti2Wd79Q6aIjPwxtQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment-with-locales.min.js" integrity="sha512-EATaemfsDRVs6gs1pHbvhc6+rKFGv8+w4Wnxk4LmkC0fzdVoyWb+Xtexfrszd1YuUMBEhucNuorkf8LpFBhj6w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.33/moment-timezone.min.js" integrity="sha512-jkvef+BAlqJubZdUhcyvaE84uD9XOoLR3e5GGX7YW7y8ywt0rwcGmTQHoxSMRzrJA3+Jh2T8Uy6f8TLU3WQhpQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.33/moment-timezone-with-data.min.js" integrity="sha512-rjmacQUGnwQ4OAAt3MoAmWDQIuswESNZwYcKC8nmdCIxAVkRC/Lk2ta2CWGgCZyS+FfBWPgaO01LvgwU/BX50Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- Lógica Popups Dinámicos -->
<?php if (isset($popup) && $popup != null && is_object($popup)): ?>
  <script>
    const current_time = moment().tz("America/Mexico_City").format("YYYY-MM-DD HH:mm:ss");

    const date_open = '<?php echo $popup->start_date ?>';
    const date_close = '<?php echo $popup->end_date ?>';

    let popupvisible = false;
    let isMobile = false;

    // Verificar si el popup aun debe estar visible en el sitio
    if (current_time >= date_open && current_time <= date_close){
        popupvisible = true;
    }

    // Verificar si el sitio esta siendo consumido por algún dispositivo movil
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
        isMobile = true;
    }

    // Mostrar el popup correspondiente [Desktop || Mobile]
    if(popupvisible){
        if(isMobile === false){
            setTimeout(function(){
                $.fancybox.open('#popup_desktop');
            }, 1000);
        } else {
            setTimeout(function(){
                $.fancybox.open('#popup_mobile');
            }, 1000);
        }
    }
  </script>
<?php endif; ?>
<?= $this->endSection() ?>