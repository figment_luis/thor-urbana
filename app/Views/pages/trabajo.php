<?= $this->extend('layouts/app') ?>

<?= $this->section('seo') ?>
<?= $metatags ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="container py-5">
    <h2 class="section-title text-center my-5"><?= lang('carrers.carrers_subtitle'); ?></h2>
    <article class="container job-info mb-5">
        <p class="text-container"><?= lang('carrers.carrers_description'); ?></p>
    </article>
    <div class="row mb-5">
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <?= lang('carrers.carrers_item_1'); ?>
            </div>
        </div>
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <?= lang('carrers.carrers_item_2'); ?>
            </div>
        </div>
        <div class="col-md-4 contact-info mx-auto p-3">
            <div class="background-gray p-3">
                <?= lang('carrers.carrers_item_3'); ?>
            </div>
        </div>
    </div>
    <section class="form-container">
        <div class="row">
            <div class="col-sm-12 col-md-6 form-col">
                <form action="<?= base_url('cvEmail'); ?>" method="post" id="job-form"
                    enctype="multipart/form-data">
                    <div class="form-info form-group">
                        <label for="name"><?= lang('carrers.carrers_form_name'); ?></label>
                        <input id="name" name="name" type="text" required>
                        <label for="email"><?= lang('carrers.carrers_form_email'); ?></label>
                        <input id="email" name="email" type="email" required>
                        <label for="vacant"><?= lang('carrers.carrers_form_jobs'); ?></label>
                        <select name="vacant" id="vacant" required>
                            <?php if (session()->get('site_lang') == 'english'): ?>
                            <option value="" selected="true" disabled="disabled">--Select--</option>
                            <option value="Legal Assistant">Legal Assistant</option>
                            <option value="Recruitment Analyst">Recruitment Analyst</option>
                            <option value="Associate Jr. Capital Markets">Associate Jr. Capital Markets</option>
                            <?php else: ?>
                            <option value="" selected="true" disabled="disabled">--Seleccione--</option>
                            <option value="Asistente Legal">Asistente Legal</option>
                            <option value="Analista de Selección">Analista de Selección</option>
                            <option value="Asociado Jr Capital Markets">Asociado Jr Capital Markets</option>
                            <?php endif; ?>
                        </select>
                        <label for="cv"><?= lang('carrers.carrers_form_resume'); ?> (.pdf o .docx)</label>
                        <!-- FILE INPUT PERSONALIZADO -->
                        <?php if (session()->get('site_lang') == 'english'): ?>
                        <div class="upload-btn-wrapper">
                            <button class="btnr" title="click to upload file">Select file</button>
                            <span id="upload-message">No attachments</span>
                            <input type="file"
                                onchange="document.getElementById('upload-message').innerHTML='File loaded successfully'"
                                class="form-control-file" name="cv" id="cv" title="click to upload file" />
                        </div>
                        <?php else: ?>
                        <div class="upload-btn-wrapper">
                            <button class="btnr" title="haga clic para cargar el archivo">Seleccionar archivo</button>
                            <span id="upload-message">No se eligió archivo</span>
                            <input type="file"
                                onchange="document.getElementById('upload-message').innerHTML='Archivo cargado correctamente'"
                                class="form-control-file" name="cv" id="cv" title="haga clic para cargar el archivo" />
                        </div>
                        <?php endif; ?>
                        <input type="hidden" name="form-type" value="job">
                    </div>
                    <button class="form-btn mt-5"><?= lang('carrers.carrers_form_button'); ?></button>
                </form>
            </div>
            <figure class="col-sm-12 col-md-6 form-image ">
                <img class="img-fluid" src="<?= base_url("public/images/trabajo-img.jpg");?>" alt="Thorurbana - Imagen Bolsa de Trabajo" title="Imagen Bolsa de Trabajo" loading="lazy">
            </figure>
        </div>
    </section>
</section>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?= base_url("public/js/sweetalert2.min.js");?>"></script>
<?= $this->endSection() ?>