<?= $this->extend('layouts/app') ?>

<?= $this->section('seo') ?>
<?= $metatags ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="my-5 pt-5 container" id="social-responsibility">
    <h2 class="section-title text-center"><?= lang('social.social_title'); ?></h2>
    <div class="row">
        <figure class="col-sm-12 col-md-6">
            <img class="img-fluid" src="<?= base_url("public/images/rs-2.jpg")?>" alt="Thor Urbana - Programa de Fundación Techo" title="Programa de Fundación Techo" loading="lazy">
        </figure>
        <article class="col-sm-12 col-md-6">
            <h4 class="secondary-title"><?= lang('social.social_title_1'); ?></h4>
            <p class="text-justify"><?= lang('social.social_description_1'); ?></p>
        </article>
    </div>
    <div class="row mt-5">
        <figure class="col-sm-12 col-md-6">
            <img class="img-fluid" src="<?= base_url("public/images/rs-1.jpg")?>" alt="Thor Urbana - Programa Construyendo y Creciendo" title="Programa Construyendo y Creciendo" loading="lazy">
        </figure>
        <article class="col-sm-12 col-md-6">
            <h4 class="secondary-title"><?= lang('social.social_title_2'); ?></h4>
            <p class="text-justify"><?= lang('social.social_description_2'); ?></p>
        </article>
    </div>
</section>
<?= $this->endSection() ?>