<?= $this->extend('layouts/app') ?>

<?= $this->section('seo') ?>
<?= $metatags ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="container py-5" id="leasing">
    <h2 class="section-title text-center mb-5">LEASING</h2>
    <p class="text-containerr text-justify" style="margin-bottom: 2rem;"><?= lang('leasing.leasing_description'); ?></p>
    <div class="row mb-5">
        <div class="col-md-4 offset-md-4 contact-info mx-auto background-gray p-3">
            <div class="contact-list">
                <p>Paseo de la Reforma 2620, Piso 16</p>
                <p>11950, México D.F.</p>
                <p><b><?= lang('leasing.leasing_address_phone'); ?>: </b><a href="tel:+52 (55) 4170 1400" title="tel:+525541701400">+52 (55) 4170 1400</a></p>
                <p><b>Email: </b><a href="mailto:info@thorurbana.com" title="mailto:info@thorurbana.com">info@thorurbana.com</a></p>
                <p><a href="tel:5581060290" title="tel:5581060290"><i class="fa fa-whatsapp" aria-hidden="true"></i> 55 8106 0290</a></p>
            </div>
        </div>
    </div>
    <div class="form-container">
        <div class="row">
            <div class="col-sm-12 col-md-6 form-col">
                <form action="<?= base_url('sendEmail'); ?>" method="post" id="text-form">
                    <div class="form-info form-group">
                        <label for="name"><?= lang('leasing.leasing_form_name'); ?></label>
                        <input id="name" name="name" type="text" required>
                        <label for="email"><?= lang('leasing.leasing_form_email'); ?></label>
                        <input id="email" name="email" type="email" required>
                        <label for="interest"><?= lang('leasing.leasing_form_project'); ?></label>
                        <select name="interest" id="interest" required>
                            <?php if (session()->get('site_lang') == 'english'): ?>
                            <option value="" selected="true" disabled="disabled">--Select--</option>
                            <?php else: ?>
                            <option value="" selected="true" disabled="disabled">--Seleccione--</option>
                            <?php endif; ?>
                            <optgroup label="<?= lang('leasing.leasing_centros'); ?>">
                                <option value="townsquare">Town Square, Metepec</option>
                                <option value="landmark-guadalajara">The Landmark, Guadalajara</option>
                                <option value="the-harbor">The Harbor, Mérida</option>
                                <option value="marina">Marina Puerto Cancún, Cancún </option>
                                <option value="calle-corazon">Calle Corazón, Playa del Carmen</option>
                                <option value="altavista">Altavista 147, Ciudad de México</option>
                            </optgroup>
                            <optgroup label="<?= lang('leasing.leasing_oficinas'); ?>">
                                <option value="landmark-guadalajara">The Landmark, Guadalajara</option>
                            </optgroup>
                            <optgroup label="<?= lang('leasing.leasing_proyectos'); ?>">
                                <option value="the-park">The Park, San Luis Potosí</option>
                                <option value="landmark-tijuana">The Landmark, Tijuana</option>
                            </optgroup>
                        </select>
                        <label for="message"><?= lang('leasing.leasing_form_message'); ?></label>
                        <textarea name="message" id="message" cols="30" rows="7" required></textarea>
                        <input type="hidden" name="form-type" value="leasing">
                    </div>
                    <button class="form-btn mt-5"><?= lang('leasing.leasing_form_button'); ?></button>
                </form>
            </div>
            <div class="col-sm-12 col-md-6 form-image ">
                <img class="img-fluid" src="<?= base_url("public/images/leasing-img.jpg");?>" alt="Thor Urbana - Imagen de Contacto" title="Imagen de Contacto" loading="lazy">
            </div>
        </div>
    </div>
</section>
<section class="py-5 text-center container partners" id="comerciales">
    <h2 class="section-title mb-5"><?= lang('leasing.partner_title'); ?></h2>
    <p class="mt-5 mb-3 text-justify"><?= lang('leasing.partner_description_p1'); ?></p>
    <p class="mb-5 text-justify"><?= lang('leasing.partner_description_p2'); ?></p>
    <div class="filters mx-auto text-center my-3">
        <div id="partner-filter-list">
            <div class="row mt-3">
                <?php foreach($partner_categories as $category): ?>
                    <div category="<?= $category->slug; ?>" class="partner-filter-item col-sm-6 col-md-3 my-3 section-title section-title-leasing">
                        <h4 style="text-transform: uppercase; cursor: pointer;"><?= lang('leasing.partner_category_' . $category->slug); ?></h4>
                    </div>
                <?php endforeach; ?>
            </div>
            <span id="partner-clean-filter" class="text-center"><?= lang('leasing.partner_show_all'); ?></span>
        </div>
    </div>
    <div class="row">
        <?php foreach($partners as $partner): ?>
            <?php if (is_file('public/images/socios-comerciales/'.$partner->image.'.jpg')): ?>
                <div class="col-sm-6 col-md-2 p-2 partner-item <?= $partner->slug; ?>">
                    <img class="img-fluid" src="<?= base_url('public/images/socios-comerciales/'.$partner->image.'.jpg') ?>" alt="Thor Urbana - <?= ucwords(implode(' ', explode('-', $partner->image))) ; ?>" title="Logotipo <?= ucwords(implode(' ', explode('-', $partner->image))) ; ?>" loading="lazy">
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <div class="col-sm-6 col-md-2 p-2 partner-item d-flex justify-content-center align-items-center ">
            <p><?= lang('leasing.partner_message'); ?></p>
        </div>
    </div>
</section>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?= base_url("public/js/sweetalert2.min.js");?>"></script>
<?= $this->endSection() ?>