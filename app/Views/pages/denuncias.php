<?= $this->extend('layouts/app') ?>

<?= $this->section('seo') ?>
<?= $metatags ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="container py-5">
    <h2 class="section-title text-center mb-5">Denuncias</h2>
    <form action="<?= base_url('sendDenouncement'); ?>" id="denouncementForm" enctype='multipart/form-data'>
        <div class="form-info form-group row">
            <div class="type-denouncement-checkbox col-sm-12 col-md-6">
                <label>¿Cómo desea enviar su denuncia?</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="typeDenouncement" id="typeDenouncement1"
                        value="0">
                    <label class="form-check-label" for="typeDenouncement1">Anónimo</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="typeDenouncement" id="typeDenouncement2"
                        value="1" checked>
                    <label class="form-check-label" for="typeDenouncement2">Con datos de identificación</label>
                </div>
                <div class="identification-denouncement-info input-hidden animate__animated animate__fadeInDown active">
                    <label for="name">Nombre Completo</label>
                    <input id="name" name="name" type="text">
                    <label>Sexo</label>
                    <div class="form-check ">
                        <input class="form-check-input" type="radio" name="gender" id="gender1" value="Masculino"
                            checked>
                        <label class="form-check-label" for="gender1">Masculino</label>
                    </div>
                    <div class="form-check ">
                        <input class="form-check-input" type="radio" name="gender" id="gender2" value="Femenino">
                        <label class="form-check-label" for="gender2">Femenino</label>
                    </div>
                    <div class="form-check ">
                        <input class="form-check-input" type="radio" name="gender" id="gender3" value="No Definido">
                        <label class="form-check-label" for="gender3">No definido</label>
                    </div>
                    <label for="email">Correo electrónico</label>
                    <input id="email" name="email" type="email">
                    <label for="phone">Teléfono Móvil (Formato: 55-4170-1400)</label>
                    <input id="phone" name="phone" type="tel" pattern="[0-9]{2}-[0-9]{4}-[0-9]{4}">
                </div>
                <label>¿Cuál es su relación con Thor Urbana?</label>
                <select name="relationDenouncement" id="relationDenouncement" required>
                    <option value="" selected="true" disabled="disabled">--Seleccione--</option>
                    <option value="Empleado">Empleado</option>
                    <option value="Cliente">Cliente</option>
                    <option value="Proveedor">Proveedor</option>
                    <option value="Ex empleado">Ex empleado</option>
                    <option value="Familiar de un empleado">Familiar de un empleado</option>
                    <option value="other">Otro - Especifique</option>
                </select>
                <input class="mt-3 input-hidden animate__animated animate__fadeInDown" id="relationDenouncementOther"
                    name="relationDenouncementOther" type="text" placeholder="Indique su relacion con Thor Urbana">
            </div>
            <div class="col-sm-12 col-md-6 form-image ">
                <img class="img-fluid" src="<?= base_url("public/images/help.jpg");?>" alt="Thor Urbana - Imagen de Denuncias" title="Imagen de Denuncias" loading="lazy">
            </div>
        </div>
        <div class="form-info form-group row">
            <label>LUGAR DE LA DENUNCIA</label>
            <div class="col-sm-12 col-md-6">
                <label for="place">Lugar de la denuncia</label>
                <select name="place" id="place" required>
                    <option value="" selected="true" disabled="disabled">--Seleccione--</option>
                    <optgroup label="Centros comerciales">
                        <option value="townsquare">Town Square, Metepec</option>
                        <option value="landmark-guadalajara">The Landmark, Guadalajara</option>
                        <option value="the-harbor">The Harbor, Mérida</option>
                        <option value="marina">Marina Puerto Cancún, Cancún </option>
                        <option value="calle-corazon">Calle Corazón, Playa del Carmen</option>
                        <option value="altavista">Altavista 147, Ciudad de México</option>
                    </optgroup>
                    <optgroup label="Oficinas">
                        <option value="landmark-guadalajara">The Landmark, Guadalajara</option>
                    </optgroup>
                    <optgroup label="Residenciales">
                        <option value="landmark-guadalajara">The Landmark, Guadalajara</option>
                    </optgroup>
                    <optgroup label="Hoteles">
                        <option value="the-ritz">The Ritz Carlton, Ciudad de México</option>
                        <option value="thompson">Thompson, Playa del Carmen</option>
                        <option value="caye-chapel">Caye Chapel, Belice</option>
                    </optgroup>
                    <optgroup label="Proyectos en Desarrollo">
                        <option value="the-park">The Park, San Luis Potosí</option>
                        <option value="landmark-tijuana">The Landmark, Tijuana</option>
                    </optgroup>
                    <optgroup label="Corporativo">
                        <option value="Corporativo">Corporativo</option>
                    </optgroup>
                </select>
            </div>
            <div class="col-sm-12 col-md-6">
                <label for="specificPlace">¿Dónde sucedieron los hechos?</label>
                <select name="specificPlace" id="specificPlace" required>
                    <option value="" selected="true" disabled="disabled">--Seleccione--</option>
                    <option value="Corporativo Thor Urbana">Corporativo Thor Urbana</option>
                    <option value="En el interior de un desarrollo en construcción">En el interior de un desarrollo en
                        construcción</option>
                    <option value="En una plaza comercial">En una plaza comercial</option>
                    <option value="En el estacionamiento">En el estacionamiento</option>
                    <option value="En una fiesta o convivio corporativo">En una fiesta o convivio corporativo</option>
                    <option value="En un domicilio particular">En un domicilio particular</option>
                    <option value="other">Otro</option>
                </select>
                <input class="mt-3 input-hidden animate__animated animate__fadeInDown" id="specificPlaceOther"
                    name="specificPlaceOther" type="text" placeholder="Indique el lugar de los hechos">
            </div>
            <div class="col-sm-12 col-md-6">
                <label for="eventDate">¿Hace cuánto sucedieron los hechos?</label>
                <input id="eventDate" name="eventDate" type="date" required>
            </div>
            <div class="col-sm-12 col-md-6">
                <label for="eventTime">Hora aproximada (Formato 12:00 a.m.)</label>
                <input id="eventTime" name="eventTime" type="time" required>
            </div>
            <label>HECHOS DE LA DENUNCIA</label>
            <div class="col-sm-12 col-md-6">
                <label for="short_description">Asunto de la denuncia, describa en pocas palabras en qué consiste su
                    reporte.</label>
                <textarea name="short_description" id="short_description" cols="30" rows="7" required></textarea>
            </div>
            <div class="col-sm-12 col-md-6">
                <label for="specificAmbient">Seleccione el ambiente que considera se trate su reporte: </label>
                <select name="specificAmbient" id="specificAmbient" required>
                    <option value="" selected="true" disabled="disabled">--Seleccione--</option>
                    <option value="Ambiente laboral">Ambiente laboral</option>
                    <option value="Fraude">Fraude</option>
                    <option value="Corrupción y soborno">Corrupción y soborno</option>
                    <option value="Trato a clientes o proveedores">Trato a clientes o proveedores</option>
                    <option value="Violencia">Violencia</option>
                    <option value="Robo">Robo</option>
                    <option value="Discriminación">Discriminación</option>
                    <option value="other">Otro</option>
                </select>
                <input class="mt-3 input-hidden animate__animated animate__fadeInDown" id="specificAmbientOther"
                    name="specificAmbientOther" type="text" placeholder="Indique el tipo de embiente de su reporte">
            </div>
            <div class="col-sm-12">
                <label for="description">Describa detalladamente lo que sucedió. Señalé cronológicamente lo sucedido,
                    considerando los detalles más mínimos y procurando contestar las siguientes preguntas: ¿Qué? ¿Cómo?
                    ¿Cuándo? ¿Quién? ¿Por qué? ¿Para qué? ¿Dónde?</label>
                <textarea name="description" id="description" cols="30" rows="7" required></textarea>
            </div>
            <div class="col-sm-12">
                <label for="aditionalPersons">Aparte de usted ¿alguien más se encuentra enterado de los hechos
                    denunciados?</label>
                <textarea name="aditionalPersons" id="aditionalPersons" cols="30" rows="7"></textarea>
            </div>
            <div class="col-sm-12">
                <label for="evidenceSources">¿Usted conoce dónde podemos recabar y conseguir pruebas de los hechos
                    sucedidos? </label>
                <textarea name="evidenceSources" id="evidenceSources" cols="30" rows="7"></textarea>
            </div>
            <div class="col-sm-12">
                <label for="involucratedPersons">¿Puede identificar a más colaboradores suyo o personal involucrado en
                    los hechos narrados? </label>
                <textarea name="involucratedPersons" id="involucratedPersons" cols="30" rows="7"></textarea>
            </div>
            <div class="col-sm-12 col-md-6">
                <label>¿Quiere proporcionar alguna evidencia de fotos, videos o documentos?</label>
                <label for="evidenceFiles">Evidencias ( .zip | .doc | .pdf )</label>
                <input type="file" multiple class="form-control-file" name="evidenceFiles[]" id="evidenceFiles">
            </div>
            <div class="col-sm-12 col-md-6">
                <label>SEGUIMIENTO A DENUNCIA</label>
                <label>¿Desea mantenerse al tanto de los resultados de la investigación?</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="tracing" id="tracing1" value="1" checked>
                    <label class="form-check-label" for="tracing1">Sí</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="tracing" id="tracing2" value="0">
                    <label class="form-check-label" for="tracing2">No</label>
                </div>
                <input class="mt-3 input-hidden animate__animated animate__fadeInDown" id="anonimeEmail"
                    name="anonimeEmail" type="text" placeholder="Email">
            </div>
            <button type="submit" class="form-btn mt-5">Enviar</button>
    </form>
</section>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?= base_url("public/js/sweetalert2.min.js");?>"></script>
<?= $this->endSection() ?>