<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Denuncia</title>
</head>
<body>
    <div>
        <h4>Relación con Thor Urbana: <?= $relation ?></h4>
		<h4>Lugar de la Denuncia</h4>
		<p>Lugar de la denuncia: <?= $place ?></p>
		<p>¿Dónde sucedieron los hechos?: <?= $specific ?></p>
		<p>Fecha del incidente: <?= $date ?></p>
		<p>Hora aproximada del incidente: <?= $time ?></p>
		<h4>Hechos de la denuncia</h4>
		<p><b>Descripcion Corta:</b></p>
		<p><?= $short ?></p>
		<p>Ambiente del reporte: <?= $ambient ?></p>
		<p><b>Describa detalladamente lo que sucedió. Señalé cronológicamente lo sucedido, considerando los detalles más mínimos y procurando contestar las siguientes preguntas: ¿Qué? ¿Cómo? ¿Cuándo? ¿Quién? ¿Por qué? ¿Para qué? ¿Dónde?
		</b></p>
		<p><?= $description ?></p>
		<p><b>Aparte de usted ¿alguien más se encuentra enterado de los hechos denunciados?</b></p>
		<p><?= $persons ?></p>
		<p><b>¿Usted conoce dónde podemos recabar y conseguir pruebas de los hechos sucedidos?</b></p>
		<p><?= $evidence ?></p>
		<p><b>¿Puede identificar a más colaboradores suyo o personal involucrado en los hechos narrados?</b></p>
		<p><?= $involucrated ?></p>
		<p><b>¿Desea mantenerse al tanto de los resultados de la investigación?</b></p>
		<p><?= $tracing ?></p>
        <?php if ($tracing == 'Si'): ?>
            <p>Email de Seguimiento: <?= $emailtracing ?></p>
        <?php endif; ?>
    </div>
</body>
</html>