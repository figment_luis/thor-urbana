﻿/* Sitio Web Thor Urbana - Thor Urbana es una de las empresas de desarrollo e inversión inmobiliaria líderes en México. */

/* TEAM */
IT Leader: Luis Antonio Rojas
Contact: luis[at]figment.com.mx
From: Mexico City

Design Leader: Jorge Nolasco
Contact: jorge[at]figment.com.mx
From: Mexico City

Web Developer: Mauricio Alejandro Hernández
Contact: mauricio.hernandez[at]figment.com.mx
From: Mexico City

Web Developer: Alejandro González
Contact: alejandro.gonzalez[at]figment.com.mx
From: Toluca City


/* THANKS */
Project Leader: Ana Aceves
Contact: ana.aceves[at]figment.com.mx
From: Mexico City


/* SITE */
Last update: 2021/07/09
Language: Español / Inglés
Standars: HTML5, CSS3, JavaScript, Responsive Web Design
Components: JQuery, Bootstrap, FontAwesome, Slick, MomentJS, SweetAlert2, LunarJS, MapHilightJS, CodeIgniter
Software: Visual Studio Code, FileZilla
