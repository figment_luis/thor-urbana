// Lazy-load backgound images
document.addEventListener("DOMContentLoaded", function() {
    let lazyloadImages;    
  
    if ("IntersectionObserver" in window) {
      lazyloadImages = document.querySelectorAll(".lazy");
      let imageObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            let image = entry.target;
            image.classList.remove("lazy");
            imageObserver.unobserve(image);
          }
        });
      });
  
      lazyloadImages.forEach(function(image) {
        imageObserver.observe(image);
      });
    } else {  
      let lazyloadThrottleTimeout;
      lazyloadImages = document.querySelectorAll(".lazy");
      
      function lazyload () {
        if(lazyloadThrottleTimeout) {
          clearTimeout(lazyloadThrottleTimeout);
        }    
  
        lazyloadThrottleTimeout = setTimeout(function() {
          let scrollTop = window.pageYOffset;
          lazyloadImages.forEach(function(img) {
              if(img.offsetTop < (window.innerHeight + scrollTop)) {
                img.src = img.dataset.src;
                img.classList.remove('lazy');
              }
          });
          if(lazyloadImages.length == 0) { 
            document.removeEventListener("scroll", lazyload);
            window.removeEventListener("resize", lazyload);
            window.removeEventListener("orientationChange", lazyload);
          }
        }, 20);
      }
  
      document.addEventListener("scroll", lazyload);
      window.addEventListener("resize", lazyload);
      window.addEventListener("orientationChange", lazyload);
    }
})

$(document).ready(function () {

    let navbarHeight = $('.navbar').innerHeight();
    let windowHeight = $(window).height();
    var windowWidth = $(window).width();
    let heroHeight = (windowHeight - navbarHeight)*0.7;
    $('.hero').css("height", heroHeight + "px");

    if (window.location.hash) {

        setTimeout(function() { 
            $('html, body').animate({ scrollTop: $(window.location.hash).offset().top - navbarHeight }, 'slow');
            // Add class active to internal links
            // $('#sidebar a').removeClass('active')
            $('.menu-option').each(function() {
                if("#" + $(this).attr("data-scroll") === window.location.hash ) {
                    $(this).addClass('active')
                }
            })
        }, 100);
    }

    // Selección de idioma
    $('.btn-language').click(function(e) {
        e.preventDefault()
        e.stopPropagation()
        window.location.href = URL + '/switchLang/' + $(this).data('language');
    })

    //SideBar Plugin

    $('#sidebar').slideReveal({
        trigger: $("#trigger,#sidebar-close"),
        overlay: true
    });

    //Header Plugin Slick
    $('.main-slider').slick({
        autoplay: true,
        autoplaySpeed: 2500,
        arrows: false,
        pauseOnHover: false,
        lazyLoad: 'ondemand',
    }); 

    //News Slider Slick

    $('.news-slider').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });




    //Function to scroll to the page sections
    $(function () {
        $('.menu-option').click(function (event) {
            event.preventDefault();
            var scroll = $(this).attr("data-scroll");

            // Remove class active from all menu items
            $(".menu-option").removeClass('active')

            if ($("#" + scroll).length > 0) {
                $('html, body').animate({ scrollTop: $('#' + scroll).offset().top - navbarHeight }, 'slow');
            } else {
                // var link = $(this).attr('href') + '#' + scroll;
                let link = $(this).attr('href');
                localStorage.setItem('back-url', link); 
                $(location).attr('href', link);
            }
            // set class active to this menu item
            $(this).addClass('active');
            localStorage.setItem('back-url', $(this).attr('href'));
            return false;
        });
    });


    //Function to change active class with scroll on Portafolio View
    //Save in Local Storage the URL of the section scroll position
    $(window).scroll(function() {
		let scrollDistance = $(window).scrollTop();

		$('.briefcase-section').each(function(i) {
			if ($(this).position().top <= scrollDistance+500) {
                    let sectionId = $(this).attr('id');
                    $('.menu-option').removeClass('active');
                    $("[data-scroll|="+sectionId+"]").addClass('active');
                    localStorage.setItem('back-url', $("[data-scroll|="+sectionId+"]").attr('href'));
				}
		    });
        }).scroll();

    $(".backbutton").click(function (event) {
        event.preventDefault();
        if(localStorage.getItem('back-url')){
            $(location).attr('href', localStorage.getItem('back-url'));
        } else {
            $(location).attr('href', $(this).find('a').attr('href'));
        }   
    });

        //Function to display dropdown Menus

    $('.dropdown-sidebar').click(function (event) {
        event.preventDefault();
        $('.dropdown-sidebar').removeClass('active');
        $('#sidebar a').removeClass('active');
        $(this).addClass('active');
        $(location).attr('href', $(this).attr('href'));
    });

    $( ".dropdown-sidebar" ).hover(function() {
        $('.dropdown-sidebar').removeClass('active');
        $(this).addClass('active');
    }
    // , function() {
    //     $( this ).removeClass( "active" );
    // }
    );
    
    

    
 



    // hide sidebar when clicking on hashed links
    $("#sidebar .menu-option").click(function() {
        if(window.location.hash || window.location.pathname === '/historia' || window.location.pathname === '/portafolio') {
            $("#sidebar").slideReveal("hide")
        }
    })

    $('#projectVideo').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })

    $("#footerVideoModal").on('hidden.bs.modal', function (e) {
        $("#footerVideoModal iframe").attr("src", $("#footerVideoModal iframe").attr("src"));
    });


    $('#jobs').on('shown.bs.modal', function () {
        $('#jobs-content').trigger('focus')
    });

    $("#mainVideo").on("ended", function () {
        $('.intro-video').fadeOut(300, function () {
            $(this).remove();
            // window.location.replace($("#mainVideo").attr('redirectTo'));
        });
    });

    /******************************************************* */
    //Formularios
    /******************************************************* */


    //Formulario Leasing 

    $("#text-form").on('submit', function (e) {
        e.preventDefault();
        var datos = $(this).serializeArray();
        
        $.ajax({
            type: $(this).attr('method'),
            data: datos,
            url: $(this).attr('action'),
            dataType: 'json',
            success: function (data) {
                
                let result = data;
                if (result.status == "send") {
                    Swal.fire({
                        title: 'Muchas gracias por ponerse en contacto con nosotros, pronto recibirá noticias por correo electrónico',
                        showClass: {
                            popup: 'animate__animated animate__fadeInDown'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp'
                        }
                    });
                    $('#text-form')[0].reset();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Opps, algo ha salido mal, por favor inténtalo más tarde.',
                        showClass: {
                            popup: 'animate__animated animate__fadeInDown'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp'
                        }
                    });

                }
            }
        })
    });


    // Formulario de Trabajo

    $("#job-form").on('submit', function(e){
        e.preventDefault();

        var datos = new FormData(this);

        $.ajax({
            type: $(this).attr('method'),
            data: datos,
            url: $(this).attr('action'),
            dataType: 'json',
            contentType: false,
            processData: false,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            async: true,
            cache: false,
            success: function(data){
                
                let result = data;
                if (result.status == "send") {
                    Swal.fire({
                        title: 'Muchas gracias por ponerse en contacto con nosotros, pronto recibirá noticias por correo electrónico',
                        showClass: {
                            popup: 'animate__animated animate__fadeInDown'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp'
                        }
                    });
                    $('#job-form')[0].reset();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Opps, algo ha salido mal, por favor inténtalo más tarde.',
                        showClass: {
                            popup: 'animate__animated animate__fadeInDown'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp'
                        }
                    });

                }
            },
            error: function (request, status, error) {
                
                Swal.fire({
                    icon: 'error',
                    title: 'Opps, algo ha salido mal, por favor inténtalo más tarde.',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                });
            }
        })
    });


    /******************************************************* */
    //Modulos de busqueda
    /******************************************************* */

    function hideDuplicates(){      //hide duplicates when a project is in two or more categories
        let seen = {};
        $(".search-item").each(function() {
            var title = $('.search-link h2', this).text();
            if (seen[title]){
                $(this).hide('1000');
            } 
            else{
                seen[title] = true;
            } 
        });
    }


    //AJAX for search module

    load_data(); //Se ejecuta una vez al iniciar para mostrar por defecto todos los proyectos
    hideDuplicates(); //Hide duplicate elements

    function load_data(query){
        var url = window.location;        

        $.ajax({
            // url: searchUrl,
            url: `${URL}/search`,
            method: "POST",
            data:{query:query},
            success:function(data){
                
                let result = (JSON.parse(data));
                let content= "";
                
                for (let i = 0 ; i < result["projects"].length ; i++){
                    if(result["projects"][i]["location"]=="new"){
                        content = content.concat(
                            "<div class='search-item mb-3 "+result["projects"][i]["category"]+"'><a target='_blank' class='search-link' href='"+result["projects"][i]["slug"]+"' title='"+result["projects"][i]["slug"]+"'><h2>"+result["projects"][i]["name"]+"</h2><p>"+result["projects"][i]["short_info"]+"</p></a></div>");
                    } else {
                        content = content.concat(
                            "<div class='search-item mb-3 "+result["projects"][i]["category"]+"'><a class='search-link' href='"+URL+"/proyecto/"+result["projects"][i]["slug"]+"' title='"+URL+"/proyecto/"+result["projects"][i]["slug"]+"'><h2>"+result["projects"][i]["name"]+" "+result["projects"][i]["location"]+"</h2><p>"+result["projects"][i]["short_info"]+"</p></a></div>");
                    }  
                }
                $('#search_result').html(content);

                $(".filter-item").each(function (){
                    if ($(this).hasClass('active')){
                        
                        filter = $(this).attr('category');
                        filterCategory(filter);
                    }
                });
                hideDuplicates(); //Hide duplicate elements
            }
        })
    }

    $('#search-input').keyup(function(){
        var search = $(this).val();
        if(search != ''){
            load_data(search);
        }
        else{
            load_data();
        }
    });

    



    /****************************** */
    /**Filters */
    /****************************** */

    //Functions for filter actions

    function filterCategory(filter){
        $(".search-item").hide();

        $(".search-item").each(function (){
            if ($(this).hasClass(filter)){
                $(this).show('1000');
            }
        });
    }

    function cleanfilter(){
        $(".filter-item").removeClass('active');
        $(".search-item").show();
        hideDuplicates(); //Hide duplicate elements
    }

    function slideFilter(){
        $("#filter-list").slideToggle("slow");
        $('#filter-button i').toggleClass("fa-arrow-up");
        $('#filter-button i').toggleClass("fa-arrow-down");
    }


    //Triggers for filter actions


    $("#filter-button").click(function(){   //Function to show/hide the filters in the search modal
        slideFilter();
    });

    $(".filter-item").click(function(){     //Funciton to filter the search results by category
        $(".filter-item").removeClass('active');
        $(this).addClass('active');
        filter = $(this).attr('category');
        filterCategory(filter);
    });

    $("#clean-filter").click(function(){    //Function to clean the filters applied
        cleanfilter();
    });

    $(".close").click(function(){
        cleanfilter();
        if($("#filter-list:visible")){
            $("#filter-list").hide();
            $('#filter-button i').toggleClass("fa-arrow-up");
            $('#filter-button i').toggleClass("fa-arrow-down");
        }
    });

    //Partners Filter

    $("#partner-filter-button").click(function(){   //Function to show/hide the filters in partners view
        $("#partner-filter-list").slideToggle("slow");
        $('#partner-filter-button i').toggleClass("fa-arrow-up");
        $('#partner-filter-button i').toggleClass("fa-arrow-down");
    });

    $(".partner-filter-item").click(function(){     //Funciton to filter the search results by category
        $(".partner-filter-item").removeClass('active');
        $(this).addClass('active');
        filter = $(this).attr('category');
        $(".partner-item").hide();

        $(".partner-item").each(function (){
            if ($(this).hasClass(filter)){
                $(this).show('1000');
            }
        });
    });

    $("#partner-clean-filter").click(function(){    //Function to clean the filters applied
        $(".partner-filter-item").removeClass('active');
        $(".partner-item").show();
    });

    //Briefcase Filters


    function cleanBriefcaseFilter(){
        $(".briefcase-filter-item").removeClass('active');
        $(".briefcase-section").show();
        $(".project-card").show();
        $("#location-filter").val('');
        notFoundResults();
    }

    function hideEmptySection(){
        $(".briefcase-section").each(function (){
            $(this).show('1000');
            if ( $(this).find(".project-card").is(":visible") ){
                $(this).show('1000');
            } else {
                $(this).hide();
            }
        });
    }

    function notFoundResults(){
        if (! $(".project-card").is(":visible")){
            
            $("#not-found-text").show('1000');
        } else {
            $("#not-found-text").hide();
        }
    }

    $("#location-filter").change(function(){ // Filter the item cards by the location selected
        filter = this.value;
        $(".project-card").hide();

        $(".project-card").each(function (){
            if ( $(this).find("span").text() == filter ){
                $(this).show('1000');
            }
        });

        if(! $(".briefcase-filter-item").hasClass('active')){
            hideEmptySection();
        }

        notFoundResults();
    });

    $(".briefcase-filter-item").click(function(){     //Filter the search results by category

        $(".briefcase-filter-item").removeClass('active');
        $(this).addClass('active');
        filter = $(this).attr('category');
        $(".briefcase-section").hide();

        $(".briefcase-section").each(function (){
            if ($(this).is("#"+filter)){
                $(this).show('1000');
            }
        });

        notFoundResults()
    });

    $("#briefcase-clean-filter").click(function(){    //Function to clean the filters applied
        cleanBriefcaseFilter();
    });


    //Filter News by Year

    $('#year-filter').change(function() {
        filterNewsByYear(this.value);
    });

    function filterNewsByYear(filter){
        $('.news-list-item').hide('1000');
        if(filter == 'all'){
            $('.news-list-item').show('1000');
        } else {
            $('.news-list-item').each(function() {
                if($(this).attr("year")=== filter) {
                    $(this).show('1000');
                }
            })
        }
    }


    //Denouncements 

    //Init Hide Items

    $('input:radio[name=typeDenouncement]').click(function (){
        if($('input:radio[name=typeDenouncement]:checked').val() == 1) { //0 = Anonimo, 1 = Con Datos personales
            $('.identification-denouncement-info').addClass('active');
            
            $('#anonimeEmail').removeClass('active');
        
        } else {
            $('.identification-denouncement-info').removeClass('active');

            if($('input:radio[name=tracing]:checked').val() == 1) {  //contotion to show the email traicing input
                $('#anonimeEmail').addClass('active');
            } else {
                $('#anonimeEmail').removeClass('active');
            }
        }
    });


    $('#relationDenouncement').change(function(){
        if( this.value != 'other') {
            $('#relationDenouncementOther').removeClass('active');
        } else {
            $('#relationDenouncementOther').addClass('active');
        }
    });

    $('#specificPlace').change(function(){
        if( this.value != 'other') {
            $('#specificPlaceOther').removeClass('active');
        } else {
            $('#specificPlaceOther').addClass('active');
        }
    });

    $('#specificAmbient').change(function(){
        if( this.value != 'other') {
            $('#specificAmbientOther').removeClass('active');
        } else {
            $('#specificAmbientOther').addClass('active');
        }
    });

    $('input:radio[name=tracing]').click(function (){
        if($('input:radio[name=tracing]:checked').val() == 1 && $('input:radio[name=typeDenouncement]:checked').val() == 0) {
            $('#anonimeEmail').addClass('active');
        } else {
            $('#anonimeEmail').removeClass('active');
        }
    });

    $("#denouncementForm").submit( function( event ) {
        event.preventDefault();
        
        let datos = new FormData(this);

        $.ajax({
            type: 'POST',
            data: datos,
            url: $(this).attr('action'),
            dataType: 'json',
            contentType: false,
            processData: false,
            async: true,
            cache: false,
            success: function(data){
                let result = data;
                if (result.status == "success") {
                    Swal.fire({
                        title: 'Muchas gracias por ponerse en contacto con nosotros, su denuncia ha sido recibida',
                        showClass: {
                            popup: 'animate__animated animate__fadeInDown'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp'
                        }
                    });
                    $('#denouncementForm')[0].reset();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Opps, algo ha salido mal, por favor inténtalo más tarde.',
                        showClass: {
                            popup: 'animate__animated animate__fadeInDown'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp'
                        }
                    });
                }
            },
            error: function (request, status, error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Opps, algo ha salido mal, por favor inténtalo más tarde.',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                });
            }
        })
        
    });
    

    // Hi Mau this maphilight i need add 2 class in img: .map-thor & .text-center
    // You can add this attr in area html: 
    // data-maphilight='{"stroke":false,"fillColor":"ff0000","fillOpacity":0.6}'
    //
    // example:
    //  
    // <area target="" alt="Landmark Tijuana" title="Landmark Tijuana" coords="142,171,29" shape="circle" data-toggle="modal" data-target="#LandmarkTijuanaModal" data-maphilight='{"stroke":false,"fillColor":"ff0000","fillOpacity":0.6}' ">
    //
    // OR this:
    try {
        $('.map-thor').maphilight({ stroke: false, fillColor: 'FF8217', fillOpacity: 0.6 });
        // Contol total areas:
        $("map area").mouseover(function(){
                
        }).mouseout(function(){
            
        }).click(function(){
                
        });
    }catch(e) {
        
    }

});